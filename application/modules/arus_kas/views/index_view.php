<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class="col-md-12">
      <div class="input-group">
       <input type="text" class="form-control" onkeyup="ArusKas.search(this, event)" id="keyword" placeholder="Pencarian Berdasarkan Tanggal ex : 2019-09-28">
       <span class="input-group-addon"><i class="fa fa-search"></i></span>
      </div>
     </div>
    </div>
    <br/>
    <br/>
    <div class="row">
     <div class="col-md-12">
      <div class='table-responsive' id="data_detail">
       <table class="table table-bordered table-list-draft">
        <thead>
         <tr class="bg-primary-light text-white">
          <th>Tanggal</th>
          <th>No Jurnal</th>
          <th>Ref</th>
          <th>Status</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($data_jurnal)) { ?>
          <?php foreach ($data_jurnal as $value) { ?>
           <tr>
            <td><?php echo $value['createddate'] ?></td>
            <td><?php echo $value['no_jurnal'] ?></td>
            <td><?php echo $value['nama_feature'] ?></td>
            <?php $text_color = $value['jenis'] == 'KREDIT' ? 'text-danger' : 'text-success' ?>
            <td class="<?php echo $text_color ?>"><?php echo $value['jenis'] ?></td>
           </tr>
          <?php } ?>
         <?php } ?>         
        </tbody>
       </table>
      </div>
     </div>          
    </div>        
   </div>

   <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-right">
     <?php echo $pagination['links'] ?>
    </ul>
   </div>
  </div>
 </div>
</div>
