<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">  
    <div class="row">
     <div class='col-md-3 text-bold'>
      Nama Bank
     </div>
     <div class='col-md-3'>
      <?php echo $nama_bank ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      No Rekening
     </div>
     <div class='col-md-3'>
      <?php echo $no_rekening ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Akun
     </div>
     <div class='col-md-3'>
      <?php echo $akun ?>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger-baru" onclick="BankAkun.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
