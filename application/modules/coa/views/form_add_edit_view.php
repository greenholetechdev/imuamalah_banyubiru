<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Parent
     </div>
     <div class='col-md-3'>
      <select class="form-control" id="parent" error="Parent">
       <option value="">Pilih Parent</option>
       <?php if (!empty($list_parent)) { ?>
        <?php foreach ($list_parent as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($parent)) { ?>
          <?php $selected = $parent == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['code'].' - '.$value['keterangan'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Kode Akun
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='code' class='form-control required' 
             value='<?php echo isset($code) ? $code : '' ?>' error="Kode Akun"/>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Akun
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='akun' class='form-control required' 
             value='<?php echo isset($keterangan) ? $keterangan : '' ?>' error="Akun"/>
     </div>     
    </div>
    <br/>       
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="Coa.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Coa.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
