<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'FORM' ?></h3>
   </div>
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Kode Penjualan
     </div>
     <div class='col-md-3'>
      <?php if (isset($content)) { ?>
       <?php echo $content[0]['no_invoice'] ?>
      <?php } else { ?>
       <input type='text' name='' id='no_invoice' class='form-control' value='' onkeyup="Faktur.getInvocePembelian(this, event)"/>
      <?php } ?>
     </div>     
    </div>
    <br/>
    <hr/>

    <div class='content_data_pembelian'>
     <div class='row'>
      <div class='col-md-4'>
       <u>Data Pembelian</u>
      </div>
     </div>
     <br/>

     <div class='row'>
      <div class='col-md-12'>
       <div class='table-responsive'>
        <table class="table table-striped table-bordered table-list-draft">
         <thead>
          <tr class="bg-primary-light text-white">
           <th>Nama</th>
           <th>Rumah</th>
           <th>Harga Pokok</th>
           <th>Harga Jual Tunai</th>
           <th>Status Pembelian</th>
           <th>No HP</th>
           <th>Alamat</th>
           <th>&nbsp;</th>
          </tr>
         </thead>
         <tbody>
          <?php if (!empty($content)) { ?>
           <?php foreach ($content as $value) { ?>
            <tr id="<?php echo $value['id'] ?>">
             <td><?php echo $value['nama_pembeli'] ?></td>
             <td><?php echo $value['nama_rumah'] ?></td>
             <td><?php echo 'Rp. ' . number_format($value['harga_cash'], 2, ',', '.') ?></td>
             <td><?php echo 'Rp. ' . number_format($value['harga_kredit'], 2, ',', '.') ?></td>
             <td><?php echo $value['status'] ?></td>
             <td><?php echo $value['no_hp'] ?></td>
             <td><?php echo $value['alamat'] ?></td>
             <td class="text-center">
              <button id="" class="btn btn-warning-baru font12" 
                      onclick="Faktur.bayar('<?php echo $value['id'] ?>')">Bayar</button>
              &nbsp;
             </td>
            </tr>

            <!--Content Pembayaran-->
            <tr pembayaran_rumah="<?php echo $value['pembayaran_rumah_id'] ?>" status="<?php echo $value['status'] ?>" id="<?php echo $value['id'] ?>" class="content_<?php echo $value['id'] ?> hide">
             <td colspan="8">
              <div class='row'>
               <div class='col-md-4'>
                <u>Input Data Pembayaran</u>
               </div>
              </div>
              <br/>
              <hr/>

              <?php if (trim(strtolower($value['status'])) == 'kredit') { ?>
               <div class='row'>
                <div class='col-md-3'>
                 * Angsuran Per Bulan -> Rp .  <b><?php echo number_format($value['harga_angsuran'], 2, ',', '.') ?></b>
                 <br/>
                 * Period -> <b><?php echo $value['periode_tahun'] ?></b>
                 <br/>
                 * Total Cicilan -> <b><?php echo $value['angsuran_time'] ?>x</b>
                 <br/>           
                 * Harga Total -> <b><?php echo 'Rp. '. number_format($value['harga_total'], 2, ',', '.') ?></b>                 
                 <br/>           
                 <hr/>         
                 * Belum Dicicil -> <b id="belum_dicicil"><?php echo $value['angsuran_time'] - $value['sudah_angsur'] ?>x</b>
                 <br/>
                 * Sudah Tercicil -> <b id="sudah_dicicil"><?php echo $value['sudah_angsur'] ?>x</b>
                 <br/>
                 <hr/>
                </div>
               </div>
               <br/>
              <?php } ?>

              <div class='row'>
               <div class='col-md-4'>
                <u>Detail Pembayaran</u>
               </div>
              </div>
              <br/>

              <div class='row'>
               <div class='col-md-3'>
                Jumlah Bayar (Rp)
               </div>
               <div class='col-md-3'>
                <input type='text' name='' id='jumlah_bayar' class='form-control required text-right' value='0' error="Jumlah Bayar"/>
               </div>
              </div>
              <br/>

              <div class='row'>
               <div class='col-md-3'>
                Tanggal Pembayaran
               </div>
               <div class='col-md-3'>
                <input type='text' name='' id='tgl_bayar' class='form-control required' value='' error="Tanggal Pembayaran"/>
               </div>
              </div>
              <br/>

              <div class='row'>
               <div class='col-md-3'>
                &nbsp;
               </div>
               <div class='col-md-3 text-right'>
                <button id="" class="btn btn-info-baru" onclick="Faktur.prosesBayar(this)">Proses</button>
                &nbsp;
                <button id="" class="btn btn-baru" onclick="Faktur.cancelBayar(this)">Tutup</button>
               </div>
              </div>
              <br/>

              <div class='row'>
               <div class='col-md-12'>
                <table class="table table-striped table-bordered table-list-draft">
                 <thead>
                  <tr>
                   <th>Total Bayar</th>
                   <th>Tanggal Bayar</th>
                   <th>Kembali</th>
                  </tr>
                 </thead>
                 <tbody>
                  <?php foreach ($value['detail'] as $v_detail) { ?>
                   <tr>
                    <td><?php echo $v_detail['no_invoice'] ?></td>
                    <td><?php echo 'Rp. ' . number_format($v_detail['total_bayar'], 2, ',', '.') ?></td>
                    <td><?php echo date('d F Y', strtotime($v_detail['tgl_bayar'])) ?></td>
                    <td><?php echo 'Rp. ' . number_format($v_detail['total_bayar'] - $value['harga_angsuran'], 2, ',', '.') ?></td>
                   </tr>
                  <?php } ?>
                 </tbody>
                </table>
               </div>
              </div>
              <br/>            
              <hr/>

             </td>
            </tr>
           <?php } ?>
          <?php } else { ?>
           <tr>
            <td colspan="9" class="text-center">Tidak ada data ditemukan</td>
           </tr>
          <?php } ?>

         </tbody>
        </table>
       </div>
      </div>
     </div>
    </div>    
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-baru" onclick="Faktur.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
