<!DOCTYPE html>
<html lang="en" dir="ltr">
 <head>
  <meta charset="utf-8">
  <title><?php echo 'Faktur Penjualan'; ?></title>

  <style>
   body {
    font-family: "Helvetica", sans-serif;
    font-size: 11px;
   }
   table {
    width: 100%;
   }
   .text-center {
    text-align: center;
   }
   .text-right {
    text-align: right;
   }
   .text-left {
    text-align: left;
   }
   .font-bold {
    font-weight: bold;
   }
   .mb-32px {
    margin-bottom: 32px;
   }
   .mr-8px {
    margin-right: 8px;
   }
   .ml-8px {
    margin-left: 8px;
   }
   .table-logo {
    width: 100%;
   }
   table.table-logo > tbody > tr > td {
    padding: 16px;
   }
   table.table-logo td {
    vertical-align: top;
   }
   .table-item {
    width: 100%;
    margin-top: 16px;
    border-collapse: collapse;
    font-size: 10px;
   }
   table.table-item th,  table.table-item td {
    border: 1px solid #333;
    padding: 4px 8px;
    border-collapse: collapse;
    vertical-align: top;
   }
   .media {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-align: start;
    align-items: flex-start;
   }
   .media-body {
    -ms-flex: 1;
    flex: 1;
   }
   
   @page{
       size: auto;
       margin-top: 2cm;
       margin-left: 2cm;
       margin-right: 2cm;
   }
  </style>
 </head>
 <body>
  <table class="table-logo">
   <tbody>
    <tr>
     <td style="width: 60%">
      <table style="margin-bottom: 8px">
       <tr>
        <td width="40"><img src="<?php echo base_url() ?>files/berkas/general/<?php echo $self['logo'] ?>" alt="" width="40"></td> 
        <td>
         <div class="font-bold">FAKTUR PENJUALAN</div>
         <div class="font-bold"><?php echo $self['title'] ?></div>
         <div><?php echo $self['alamat'] ?></div>
        </td>
      </tr>              
      </table>
      <table>
       <tr>
        <td class="" style="padding-top: 30px;">
         <div class="">Kepada, <br/><?php echo $invoice['nama_pembeli'] ?><br/><?php echo $invoice['alamat'] ?></div>
        </td>
       </tr>
      </table>
     </td>
     <td style="width: 40%" style="padding-top: 20px;">
      <table>
       <tr>
        <td class="">
         <div class="font-bold">No Faktur</div>
        </td>
        <td class="text-right" width="10">:</td>
        <td>
         <div><?php echo $invoice['no_faktur'] ?></div>
        </td>
       </tr>
       <tr>
        <td class="">
         <div class="font-bold">Tgl. Faktur</div>
        </td>
        <td class="text-right" width="10">:</td>
        <td>
         <div><?php echo date("d F Y", strtotime($invoice['tanggal_faktur'])) ?></div>
        </td>
       </tr>
       <tr>
        <td class="">
         <div class="font-bold">Tgl. Jatuh Tempo</div>
        </td>
        <td class="text-right" width="10">:</td>
        <td>
         <div><?php echo date("d F Y", strtotime($invoice['tanggal_bayar'])) ?></div>
        </td>
       </tr>       
       <tr>
        <td class="">
         <div class="font-bold">Jenis Pembayaran</div>
        </td>
        <td class="text-right" width="10">:</td>
        <td>
         <div><?php echo $invoice['jenis_bayar'] ?></div>
        </td>
       </tr>   
       <tr>
        <td class="">
         <div class="font-bold">Potongan</div>
        </td>
        <td class="text-right" width="10">:</td>
        <td>
         <div><?php echo $invoice['jenis_potongan'] ?></div>
        </td>
       </tr>   
       <tr>
        <td class="">
         <div class="font-bold">Nilai Potongan</div>
        </td>
        <td class="text-right" width="10">:</td>
        <td>
         <div><?php echo $invoice['pot_faktur'] ?></div>
        </td>
       </tr>   
      </table>
     </td>
    </tr>
   </tbody>
  </table>
  <table class="table-item">
   <thead>
    <tr>
     <th style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: 1px solid #ccc;">No</th>
     <th style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: 1px solid #ccc;">Produk</th>
     <th style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: 1px solid #ccc;">Jumlah</th>
     <th style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: 1px solid #ccc;">Satuan</th>
     <th style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: 1px solid #ccc;">Potongan</th>
     <!--<th>Jumlah Potongan</th>-->
     <!--<th>Pajak</th>-->
     <th style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: 1px solid #ccc;">Harga</th>
     <th style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: 1px solid #ccc;" width="125">Total Bayar</th>
    </tr>
   </thead>
   <tbody>
    <?php if (!empty($invoice_item)) { ?>
     <?php $no = 1; ?>
     <?php foreach ($invoice_item as $value) { ?>
      <tr>
       <td style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: none;"><?php echo $no++ ?></td>
       <td style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: none;"><?php echo $value['nama_product'] ?></td>
       <td style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: none;" class="text-center"><?php echo $value['qty'] ?></td>
       <td style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: none;" class="text-center"><?php echo $value['nama_satuan'] ?></td>
       <td style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: none;" class="text-center">
        <?php if (!empty($value['pot_item'])) { ?>
         <?php foreach ($value['pot_item'] as $v_i) { ?>
          <?php if ($v_i['jenis_potongan'] == 'Nominal') { ?>
           <?php echo number_format($v_i['nilai']) . '/' ?>
          <?php } else { ?>
           <?php echo number_format($v_i['nilai']) . ' %/' ?>
          <?php } ?>
         <?php } ?>        
        <?php } ?>        
       </td>
       <!--<td class="text-center"><?php echo $value['jenis'] ?></td>-->
       <td style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: none;" class="text-center"><?php echo number_format($value['harga'], 2) ?></td>
       <td style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: none;" class="text-right"><?php echo 'Rp. ' . number_format($value['sub_total'], 2, ',', '.') ?></td>
      </tr>

      <?php if ($value['bank'] != '' && $value['bank'] != '0') { ?>
       <tr>
        <td class="text-left font-bold" colspan="6"><?php echo $value['nama_bank'] . ' - ' . $value['no_rekening'] . ' - ' . $value['akun'] ?></td>
       </tr>
      <?php } ?>
     <?php } ?>
    <?php } ?>    
    <?php if (!empty($biaya_item)) { ?>
     <?php foreach ($biaya_item as $value) { ?>
      <tr>
       <td style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: none;" colspan="6" class="text-right"><?php echo $value['ket_biaya'] ?></td>
       <td style="border-bottom: 1px solid #ccc;border-left: none;border-right: none;border-top: none;" class="text-right"><?php echo 'Rp, ' . number_format($value['jumlah']) ?></td>
      </tr>
     <?php } ?>
    <?php } ?>    
    <tr>
     <td style="border: 0" class="text-right" colspan="6">Total</td>
     <td style="border-bottom: none;border-left: none;border-right: none;border-top: none;" class="text-right font-bold"><?php echo 'Rp. ' . number_format($invoice['total'], 2, ',', '.') ?></td>
    </tr>
   </tbody>
  </table>
  
  <table style="width: 100%; margin-top: 32px">
   <tbody>
    <tr>
     <td class="text-center">Hormat Kami,</td>
     <td class="text-center">Penerima,</td>
     <td class="text-center">&nbsp;</td>
    </tr>
    <tr>
     <td style="height: 50px"></td>
     <td style="height: 50px"></td>
     <td style="height: 50px"></td>
    </tr>
    <tr>
     <td class="text-center">(<?php echo $this->session->userdata('username') ?>)</td>
     <td class="text-center">(------------------------------)</td>
     <!--<td class="text-center">Hal:---------------------------------</td>-->
    </tr>
   </tbody>
  </table>
 </body>
</html>
