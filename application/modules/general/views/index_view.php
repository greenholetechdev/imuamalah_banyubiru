<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class="col-md-3">
      <button class="btn btn-success" id="" onclick="General.ubah(1)">Ubah</button>
     </div>
    </div>    
    <br/>
    <div class="row">
     <div class="col-md-12">
      <div class='table-responsive'>
       <table class="table table-striped table-bordered table-list-draft">
        <thead>
         <tr class="bg-primary-light text-white">
          <th>No</th>
          <th>Nama Perusahaan</th>
          <th>Alamat</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($content)) { ?>
          <?php $no = 1; ?>
          <?php foreach ($content as $value) { ?>
           <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $value['title'] ?></td>
            <td><?php echo $value['alamat'] ?></td>
           </tr>
          <?php } ?>
         <?php } else { ?>
          <tr>
           <td colspan="6" class="text-center">Tidak ada data ditemukan</td>
          </tr>
         <?php } ?>

        </tbody>
       </table>
      </div>
     </div>          
    </div> 
   </div>
   
   <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-right">
     <?php echo $pagination['links'] ?>
    </ul>
   </div>
  </div>
 </div>
</div>
