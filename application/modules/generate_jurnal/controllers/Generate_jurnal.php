<?php

class Generate_jurnal extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'generate_jurnal';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
  );

  return $data;
 }

 public function getTableName() {
  return 'coa';
 }

 public function index() {
  echo 'oke';
 }
 
 public function getJurnalStruktur($transaction) {
  $data = Modules::run('database/get', array(
              'table' => 'jurnal_struktur js',
              'field' => array('js.*', 'f.nama as transaksi'),
              'join' => array(
                  array('feature f', 'f.id = js.feature')
              ),
              'where' => "js.deleted = 0 and f.nama = '".$transaction."'"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function insertJurnal($ref = "") {  
  $data['no_jurnal'] = Modules::run('no_generator/generateNoJurnal');
  $data['tanggal'] = date('Y-m-d');
  $data['ref'] = $ref;
  $jurnal = Modules::run('database/_insert', 'jurnal', $data);
  return $jurnal;
 }
 
 public function insertJurnalDetail($value) {  
  $data['jurnal'] = $value['jurnal'];
  $data['jurnal_struktur'] = $value['jurnal_struktur'];
  $data['jumlah'] = $value['jumlah'];
  Modules::run('database/_insert', 'jurnal_detail', $data);
 }
 

}
