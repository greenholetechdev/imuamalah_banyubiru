<head>
 <script src="<?php echo base_url() ?>assets/js/excellentexport.js"></script>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <title><?php echo isset($title) ? $title : '' ?></title>
 <meta name="description" content="Sufee Admin - HTML5 Admin Template">
 <meta name="viewport" content="width=device-width, initial-scale=1">

 <link rel="icon" type="image/png" href="<?php echo base_url().'assets/images/architecture.png' ?>" />

 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/normalize.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
 <!--<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap-datepicker.min.css">-->
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/font-awesome.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/themify-icons.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/flag-icon.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/cs-skin-elastic.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/lib/mdi/css/materialdesignicons.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/amaran.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/animate.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/select2.min.css">
 <!-- <link rel="stylesheet" href="assets/css/bootstrap-select.less"> -->
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/lib/datatable/dataTables.bootstrap.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/stickyTable.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/scss/style.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/wjc.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/autocomplete.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/toastr.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/css-loader.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugin/dropzone/dropzone.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugin/dropzone/basic.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/js/jquery-ui/themes/base/jquery-ui.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/jquery-ui.min.css">

 <script src="<?php echo base_url() ?>assets/js/jquery.js"></script> 
 <script src="<?php echo base_url() ?>assets/js/select2.full.min.js"></script>
 <!--<script src="<?php echo base_url() ?>assets/js/bootstrap-datepicker.js"></script>-->
 <script src="<?php echo base_url() ?>assets/js/number-divider.min.js"></script>
 <script src="<?php echo base_url() ?>assets/js/jquery-ui.min.js"></script>
 <script src="<?php echo base_url() ?>assets/js/jquery-1.9.1.min.js"></script> 
 <!--<script src="<?php echo base_url() ?>assets/js/bootstrap-datepicker.min.js"></script>-->
 <script src="<?php echo base_url() ?>assets/js/bootstrap.min.js"></script>
 <script src="<?php echo base_url() ?>assets/js/popper.min.js"></script>
 <script src="<?php echo base_url() ?>assets/js/plugins.js"></script>
 <script src="<?php echo base_url() ?>assets/js/Chart.bundle.min.js"></script>
 <script src="<?php echo base_url() ?>assets/js/utils.js"></script>
 <script src="<?php echo base_url() ?>assets/js/stickyTable.min.js"></script>
 <script src="<?php echo base_url() ?>assets/js/main.js"></script>
 <script src="<?php echo base_url() ?>assets/js/url.js"></script>
 <script src="<?php echo base_url() ?>assets/js/autocomplete.js"></script>
 <script src="<?php echo base_url() ?>assets/js/message.js"></script>
 <script src="<?php echo base_url() ?>assets/js/toastr.min.js"></script>
 <script src="<?php echo base_url() ?>assets/js/validation.js"></script>
 <script src="<?php echo base_url() ?>assets/js/propper.js"></script>
 <script src="<?php echo base_url() ?>assets/js/bootbox.js"></script>
 <script src="<?php echo base_url() ?>assets/js/controllers/template.js"></script> 
</head>
