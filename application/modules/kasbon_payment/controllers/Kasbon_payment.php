<?php

class Kasbon_payment extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'kasbon_payment';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/kasbon_payment.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'kasbon_payment';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Faktur";
  $data['title_content'] = 'Data Faktur';
  $content = $this->getDataFaktur();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataFaktur($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_faktur', $keyword),
       array('p.tanggal_faktur', $keyword),
       array('p.tanggal_bayar', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*'),
              'like' => $like,
              'is_or_like' => true,
              'where' => "p.deleted is null or p.deleted = 0"
  ));

  return $total;
 }

 public function getDataFaktur($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_faktur', $keyword),
       array('p.tanggal_faktur', $keyword),
       array('p.tanggal_bayar', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*'),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "p.deleted is null or p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataFaktur($keyword)
  );
 }

 public function getDetailDataFaktur($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kp',
              'field' => array('kp.*', 'pl.nama as nama_pegawai', 'pl.id as pegawai'),
              'join' => array(
                  array('(select max(id) id, kasbon_payment from kasbon_payment_item group by kasbon_payment) ppi', 'ppi.kasbon_payment = kp.id'),
                  array('kasbon_payment_item pi', 'ppi.id = pi.id'),
                  array('kasbon k', 'k.id = pi.kasbon'),
                  array('pegawai pl', 'k.pegawai = pl.id'),
              ),
              'where' => "kp.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getDetailDataInvoice($id) {
  $data = Modules::run('database/get', array(
              'table' => 'invoice i',
              'field' => array('i.*',
                  'p.nama as nama_pembeli', 'ist.status'),
              'join' => array(
                  array('pembeli p', 'i.pembeli = p.id'),
                  array('(select max(id) id, invoice from invoice_status group by invoice) iss', 'iss.invoice = i.id'),
                  array('invoice_status ist', 'ist.id = iss.id'),
              ),
              'where' => "i.id = '" . $id . "'"
  ));

  $result = $data->row_array();
  $result['total_ori'] = $result['total'];
  $result['total'] = number_format($result['total']);
  return $result;
 }

 public function getListProduct() {
  $data = Modules::run('database/get', array(
              'table' => 'product_satuan ps',
              'field' => array('ps.*', 'p.product as nama_product'),
              'join' => array(
                  array('product p', 'ps.product = p.id')
              ),
              'where' => "ps.deleted = 0 or ps.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPegawai() {
  $data = Modules::run('database/get', array(
              'table' => 'pegawai p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 or p.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPajak() {
  $data = Modules::run('database/get', array(
              'table' => 'pajak p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 or p.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListMetodeBayar() {
  $data = Modules::run('database/get', array(
              'table' => 'metode_bayar',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListInvoice() {
  $data = Modules::run('database/get', array(
              'table' => 'invoice i',
              'field' => array('i.*'),
              'join' => array(
                  array('(select max(id) id, invoice from invoice_status group by invoice) iss', 'iss.invoice = i.id'),
                  array('invoice_status ist', 'ist.id = iss.id')
              ),
              'where' => "i.deleted = 0 and ist.status = 'DRAFT'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Faktur";
  $data['title_content'] = 'Tambah Faktur';
  $data['list_product'] = $this->getListProduct();
  $data['list_pegawai'] = $this->getListPegawai();
  $data['list_metode'] = $this->getListMetodeBayar();
  $data['list_pajak'] = $this->getListPajak();
  $data['list_invoice'] = $this->getListInvoice();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataFaktur($id);
//  echo $data['total'];die;
//  echo '<pre>';
//  print_r($data);die;
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Faktur";
  $data['title_content'] = 'Ubah Faktur';
  $data['list_product'] = $this->getListProduct();
  $data['list_pegawai'] = $this->getListPegawai();
  $data['list_metode'] = $this->getListMetodeBayar();
  $data['list_pajak'] = $this->getListPajak();
  $data['invoice_item'] = $this->getListInvoiceItem($id);

  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataFaktur($id);
//  echo '<pre>';
//  print_r($data);
//  die;
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Faktur";
  $data['title_content'] = 'Detail Faktur';
  $data['list_product'] = $this->getListProduct();
  $data['list_pegawai'] = $this->getListPegawai();
  $data['list_metode'] = $this->getListMetodeBayar();
  $data['list_pajak'] = $this->getListPajak();
  $data['list_invoice'] = $this->getListInvoice();

  $data['invoice_item'] = $this->getDataInvoiceItem($id);
//  echo '<pre>';
//  print_r($data['invoice_item']);die;
  $data['view_item'] = $this->load->view('detail_invoice_item', $data, true);
  echo Modules::run('template', $data);
 }

 public function getListInvoiceItem($invoice) {
  $data = Modules::run('database/get', array(
              'table' => 'invoice_product ip',
              'field' => array('ip.*', 'ps.satuan', 'ps.harga',
                  'p.product as nama_product',
                  'b.nama_bank', 'b.akun',
                  'b.no_rekening', 'pj.jenis',
                  'pj.persentase', 'm.metode'),
              'join' => array(
                  array('product_satuan ps', 'ps.id = ip.product_satuan'),
                  array('product p', 'p.id = ps.product'),
                  array('bank b', 'b.id = ip.bank', 'left'),
                  array('pajak pj', 'pj.id = ip.pajak'),
                  array('metode_bayar m', 'm.id = ip.metode_bayar'),
              ),
              'where' => "ip.invoice = '" . $invoice . "' and ip.deleted = 0",
              'orderby' => 'ip.id'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getPostDataHeader($value) {
  $data['no_faktur'] = Modules::run('no_generator/generateNoFakturBayarKasbon');
  $data['tanggal_faktur'] = date('Y-m-d', strtotime($value->tanggal_faktur));
  $data['tanggal_bayar'] = date('Y-m-d', strtotime($value->tanggal_bayar));
  $data['jumlah'] = str_replace('.', '', $value->jumlah);
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $jumlah_bayar = str_replace('.', '', $data->jumlah);
   $post_data = $this->getPostDataHeader($data);
   
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_data);

    //payment_item
    if (!empty($data->kasbon_item)) {
     $sisa = $jumlah_bayar;
     foreach ($data->kasbon_item as $value) {
      $total_bayar = 0;
      $sisa_hutang = str_replace(',', '', $value->sisa_hutang);
      if ($sisa < $sisa_hutang) {
       $total_sisa = $sisa_hutang - $sisa;
      } else {
       $total_sisa = $sisa_hutang;
      }

      $margin = $sisa - $total_sisa;

      $post_item['kasbon_payment'] = $id;
      $post_item['kasbon'] = $value->kasbon;
      $post_item['jumlah_bayar'] = $sisa;
      Modules::run('database/_insert', 'kasbon_payment_item', $post_item);

      if ($margin >= 0) {
       //paid
       $post_status['kasbon'] = $value->kasbon;
       $post_status['status'] = 'PAID';
       Modules::run('database/_insert', 'kasbon_status', $post_status);

       $post_sisa['jumlah'] = 0;
      } else {
       $post_sisa['jumlah'] = $total_sisa;
      }

      //invoice sisa     
      $post_sisa['kasbon'] = $value->kasbon;
      Modules::run('database/_insert', 'kasbon_sisa', $post_sisa);
      $sisa -= $sisa_hutang;
     }
    }


    //create jurnal akuntan
    $jurnal = Modules::run('generate_jurnal/insertJurnal', $post_data['no_faktur']);
    $jurnal_struktur = Modules::run('generate_jurnal/getJurnalStruktur', 'Kasbon Bayar');
//    echo '<pre>';
//    print_r($jurnal_struktur);die;
    if (!empty($jurnal_struktur)) {
     foreach ($jurnal_struktur as $value) {
      $post_detail['jurnal'] = $jurnal;
      $post_detail['jurnal_struktur'] = $value['id'];
      $post_detail['jumlah'] = $jumlah_bayar;
      Modules::run('generate_jurnal/insertJurnalDetail', $post_detail);
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Faktur";
  $data['title_content'] = 'Data Faktur';
  $content = $this->getDataFaktur($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function addItem() {
  $data['list_product'] = $this->getListProduct();
  $data['list_pegawai'] = $this->getListPegawai();
  $data['list_metode'] = $this->getListMetodeBayar();
  $data['list_pajak'] = $this->getListPajak();
  $data['index'] = $_POST['index'];
  echo $this->load->view('product_item', $data, true);
 }

 public function getListBank() {
  $data = Modules::run('database/get', array(
              'table' => 'bank',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getMetodeBayar() {
  $data['list_bank'] = $this->getListBank();
  $data['index'] = $_POST['index'];
  echo $this->load->view('bank_akun', $data, true);
 }

 public function printFaktur($id) {

  $data = $this->getDetailDataFaktur($id);
  $data['invoice_item'] = $this->getDataInvoiceItem($id);
  $data['self'] = Modules::run('general/getDetailDataGeneral', 1);
  $mpdf = Modules::run('mpdf/getInitPdf');

//  $pdf = new mPDF('A4');
  $view = $this->load->view('cetak', $data, true);
  $mpdf->WriteHTML($view);
  $mpdf->Output('Nota Bayar Customer - ' . date('Y-m-d') . '.pdf', 'I');
 }

 public function getDataInvoice($pegawai) {
  $data = Modules::run('database/get', array(
              'table' => 'kasbon k',
              'field' => array('k.*', 'ist.status', 'isa.jumlah as sisa_hutang'),
              'join' => array(
                  array('(select max(id) id, kasbon from kasbon_status group by kasbon) iss', 'iss.kasbon = k.id'),
                  array('kasbon_status ist', 'ist.id = iss.id'),
                  array('(select max(id) id, kasbon from kasbon_sisa group by kasbon) ss', 'ss.kasbon = k.id', 'left'),
                  array('kasbon_sisa isa', 'isa.id = ss.id', 'left'),
              ),
              'where' => "k.deleted = 0 and k.pegawai = '" . $pegawai . "' and ist.status = 'DRAFT'",
              'orderby' => 'k.tanggal_faktur, k.tanggal_bayar'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    if ($value['sisa_hutang'] == '') {
     $value['sisa_hutang'] = $value['total'];
    }
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;
  return $result;
 }

 public function getDataInvoiceItem($payment) {
  $data = Modules::run('database/get', array(
              'table' => 'kasbon_payment_item pi',
              'field' => array('pi.*', 'ist.status',
                  'isa.jumlah as sisa_hutang',
                  'i.no_kasbon',
                  'i.total as invoice_total', 'ist.status'),
              'join' => array(
                  array('kasbon i', 'pi.kasbon = i.id'),
                  array('(select max(id) id, kasbon from kasbon_status group by kasbon) iss', 'iss.kasbon = i.id'),
                  array('kasbon_status ist', 'ist.id = iss.id'),
                  array('(select max(id) id, kasbon from kasbon_sisa group by kasbon) ss', 'ss.kasbon = i.id', 'left'),
                  array('kasbon_sisa isa', 'isa.id = ss.id', 'left'),
              ),
              'where' => "pi.deleted = 0 and pi.kasbon_payment = '" . $payment . "'",
              'orderby' => 'pi.id'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    if ($value['sisa_hutang'] == '') {
     $value['sisa_hutang'] = $value['total'];
    }
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;
  return $result;
 }

 public function getDetailInvoice() {
  $pegawai = $_POST['pegawai'];
  $data['invoice_item'] = $this->getDataInvoice($pegawai);
//  echo '<pre>';
//  print_r($data);die;
  $data['view_item'] = $this->load->view('invoice_item', $data, true);

  echo json_encode($data);
 }

}
