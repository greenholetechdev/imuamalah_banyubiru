<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Pelanggan</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Pelanggan
     </div>
     <div class='col-md-3'>
      <select disabled="" class="form-control required" id="customer" 
              error="Invoice"
              onchange="KasbonPayment.getDetailInvoice(this)">
       <option value="">Pilih Pegawai</option>
       <?php if (!empty($list_pegawai)) { ?>
        <?php foreach ($list_pegawai as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($pegawai)) { ?>
          <?php $selected = $pegawai == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>
    <br/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Kasbon</u>
     </div>
    </div>
    <br/>
    <hr/>

    <div class="row">
     <div class="col-md-10">
      <div class="table-responsive" id="content-invoice">
       <?php echo $view_item ?>
      </div>
     </div>
    </div>
    <br/>

    <?php echo $this->load->view('detail_faktur_bayar'); ?>

    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger" onclick="KasbonPayment.cetak('<?php echo isset($id) ? $id : '' ?>')">Cetak</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="KasbonPayment.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
