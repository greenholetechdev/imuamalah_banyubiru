<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-box-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="box-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="box-body box-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Pelanggan</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Pelanggan
     </div>
     <div class='col-md-3'>
      <select class="form-control required" id="pegawai" 
              error="Invoice"
              onchange="KasbonPayment.getDetailInvoice(this)">
       <option value="">Pilih Pegawai</option>
       <?php if (!empty($list_pegawai)) { ?>
        <?php foreach ($list_pegawai as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($pembeli)) { ?>
          <?php $selected = $pembeli == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>
    <br/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Invoice</u>
     </div>
    </div>
    <br/>
    <hr/>

    <div class="row">
     <div class="col-md-10">
      <div class="table-responsive" id="content-invoice">
       <table class="table table-striped table-bordered table-list-draft" id="tb_item">
        <thead>
         <tr class="bg-primary-light text-white">
          <th>No</th>
          <th>Faktur</th>
          <th>Total</th>
          <th class="text-center">
           <input type="checkbox" value="" id="check_all" class="" />
          </th>
         </tr>
        </thead>
        <tbody>
         <tr data_id="">
          <td colspan="7"class="text-center">
           Tidak ada data ditemukan
          </td>
         </tr>
        </tbody>
       </table>
      </div>
     </div>
    </div>

    <div class="row">
     <div class="col-md-10 text-right">
      <h4>Total : Rp, <label id="total" total="0">0</label></h4>
     </div>
    </div>
    <br/>

    <?php echo $this->load->view('form_faktur_bayar'); ?>

    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="KasbonPayment.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="KasbonPayment.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
