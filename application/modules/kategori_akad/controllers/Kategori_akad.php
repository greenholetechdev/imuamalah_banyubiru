<?php

class Kategori_akad extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'kategori_akad';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/kategori_akad.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'kategori_akad';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Kategori Akad";
  $data['title_content'] = 'Data Kategori Akad';
  $content = $this->getDataKategoriAkad();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataKategoriAkad($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('ka.kategori', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
  'table' => $this->getTableName() . ' ka',
  'field' => array('ka.*'),
  'like' => $like,
  'is_or_like' => true,
  'where'=> "ka.deleted is null or ka.deleted = 0"
  ));

  return $total;
 }

 public function getDataKategoriAkad($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('ka.kategori', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' ka',
  'field' => array('ka.*'),
  'like' => $like,
  'is_or_like' => true,
  'limit' => $this->limit,
  'offset' => $this->last_no,
  'where'=> "ka.deleted is null or ka.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
  'data' => $result,
  'total_rows' => $this->getTotalDataKategoriAkad($keyword)
  );
 }

 public function getDetailDataKategoriAkad($id) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' kr',
  'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah KategoriAkad";
  $data['title_content'] = 'Tambah KategoriAkad';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataKategoriAkad($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah KategoriAkad";
  $data['title_content'] = 'Ubah KategoriAkad';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataKategoriAkad($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail KategoriAkad";
  $data['title_content'] = 'Detail KategoriAkad';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['kategori'] = $value->kategori_akad;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $kategori_akad = $id;
  $this->db->trans_begin();
  try {
   $post_kategori = $this->getPostDataHeader($data);
   if ($id == '') {
    $kategori_akad = Modules::run('database/_insert', $this->getTableName(), $post_kategori);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_kategori, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'kategori_akad'=> $kategori_akad));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Kategori Akad";
  $data['title_content'] = 'Data Kategori Akad';
  $content = $this->getDataKategoriAkad($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }
}
