<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">
    <div class="row">
     <div class='col-md-3 text-bold'>
      Kategori
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='kategori_akad' class='form-control required' 
             value='<?php echo isset($kategori) ? $kategori : '' ?>' error="Kategori"/>
     </div>     
    </div>
    <br/>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="KategoriAkad.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="KategoriAkad.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
