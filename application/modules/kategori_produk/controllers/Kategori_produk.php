<?php

class Kategori_produk extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'kategori_produk';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/kategori_produk.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'kategori_product';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Kategori product";
  $data['title_content'] = 'Data Kategori product';
  $content = $this->getDataKategoriproduct();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataKategoriproduct($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('kr.kategori', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
  'table' => $this->getTableName() . ' kr',
  'field' => array('kr.*'),
  'like' => $like,
  'is_or_like' => true,
  'where' => "kr.deleted is null or kr.deleted = 0"
  ));

  return $total;
 }

 public function getDataKategoriproduct($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('kr.kategori', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' kr',
  'field' => array('kr.*'),
  'like' => $like,
  'is_or_like' => true,
  'limit' => $this->limit,
  'offset' => $this->last_no,
  'where' => "kr.deleted is null or kr.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
  'data' => $result,
  'total_rows' => $this->getTotalDataKategoriproduct($keyword)
  );
 }

 public function getDetailDataKategoriproduct($id) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' kr',
  'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Kategori product";
  $data['title_content'] = 'Tambah Kategori product';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataKategoriproduct($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Kategoriproduct";
  $data['title_content'] = 'Ubah Kategoriproduct';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataKategoriproduct($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Kategoriproduct";
  $data['title_content'] = 'Detail Kategoriproduct';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['kategori'] = $value->kategori;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $kategori_product = $id;
  $this->db->trans_begin();
  try {
   $post_kategori_product = $this->getPostDataHeader($data);
   if ($id == '') {
    $kategori_product = Modules::run('database/_insert', $this->getTableName(), $post_kategori_product);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_kategori_product, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'kategori_product'=> $kategori_product));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Kategoriproduct";
  $data['title_content'] = 'Data Kategoriproduct';
  $content = $this->getDataKategoriproduct($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function ubahHarga($id, $produk_has_satuan) {
  $harga = $this->input->post('harga');
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', 'produk_satuan_has_harga', array('period_end' => date('Y-m-d')), array('id' => $id));
   $post['produk_has_satuan'] = $produk_has_satuan;
   $post['harga'] = $harga;
   $post['period_start'] = date('Y-m-d');
   Modules::run('database/_insert', 'produk_satuan_has_harga', $post);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getDataKategoriproductSatuanHarga($produk_satuan) {
  $data = Modules::run('database/get', array(
  'table' => 'produk_satuan_has_harga',
  'where' => array('produk_has_satuan' => $produk_satuan)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['active'] = "0";
    if ($value['period_end'] == '') {
     $value['active'] = "1";
     $value['period_end'] = '-';
    } else {
     $value['period_end'] = date('d F Y', strtotime($value['period_end']));
    }
    $value['period_start'] = date('d F Y', strtotime($value['period_start']));
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }
}
