<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">  

    <div class="row">
     <div class='col-md-12'>
      <table class="table table-striped table-bordered table-list-draft">
       <thead>
        <tr class="bg-primary-light text-white">
         <th>Keterangan</th>
         <th>Presentase (%)</th>
         <th>Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php foreach ($content as $value) { ?>
          <tr id="<?php echo $value['id'] ?>">
           <td>
            <input type='text' name='' id='keterangan' class='form-control' value='<?php echo $value['keterangan'] ?>'/>
          </td>
          <td>
           <input type='text' name='' id='presentase' class='form-control' value='<?php echo $value['presentase'] ?>'/>
          </td>
           <td class="text-center">
            &nbsp;
           </td>
          </tr>
         <?php } ?>
         <tr id="">
          <td>
           <input type='text' name='' id='keterangan' class='form-control' value=''/>
          </td>
          <td>
           <input type='text' name='' id='presentase' class='form-control' value=''/>
          </td>
          <td class="text-center">
           <i class="mdi mdi-plus mdi-24px hover" onclick="Internal.addRow(this)"></i>
          </td>
         </tr>
        <?php } else { ?>
         <tr>
          <td colspan="6" class="text-center">Tidak ada data ditemukan</td>
         </tr>
        <?php } ?>

       </tbody>
      </table>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="Internal.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Internal.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
