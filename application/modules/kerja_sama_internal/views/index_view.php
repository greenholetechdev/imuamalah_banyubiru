<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class="col-md-3">
      <button class="btn btn-success" id="" onclick="Internal.edit()">Edit</button>
     </div>
     <div class="col-md-9">
      <!--      <div class="form-inside-icon icon-pos-right">
             <input type="text" id="keyword" class="form-control" placeholder="Pencarian" onkeyup="Internal.ubah(this, event)">
             <div class="form-icon">
              <i class="fa fa-search"></i>
             </div>
            </div>-->
     </div>
    </div>
    <!--    <div class='row'>
         <div class='col-md-12'>
    <?php if (isset($keyword)) { ?>
     <?php if ($keyword != '') { ?>
                        Cari Data : "<b><?php echo $keyword; ?></b>"
     <?php } ?>
    <?php } ?>
         </div>
        </div>-->
    <br/>
    <?php $total_presentase = 0; ?>
    <div class="row">
     <div class="col-md-12">
      <div class='table-responsive'>
       <table class="table table-striped table-bordered table-list-draft">
        <thead>
         <tr class="bg-primary-light text-white">
          <th>No</th>
          <th>Keterangan</th>
          <th>Presentase (%)</th>
          <th>Action</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($content)) { ?>
          <?php $no = 1; ?>
          <?php foreach ($content as $value) { ?>
           <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $value['keterangan'] ?></td>
            <td><?php echo $value['presentase'] ?></td>
            <td class="text-center">
             <?php if ($value['keterangan'] != 'Dividen') { ?>
              <button class="btn btn-danger-baru" onclick="Internal.delete('<?php echo $value['id'] ?>')">Hapus</button>
             <?php } ?>
            </td>
           </tr>
           <?php $total_presentase += $value['presentase'] ?>
          <?php } ?>
          <tr>
           <td class="text-right" colspan="3"><?php echo 'Total :' ?></td>
           <td class="text-left"><?php echo $total_presentase . ' %' ?></td>          
          </tr>
         <?php } else { ?>
          <tr>
           <td colspan="6" class="text-center">Tidak ada data ditemukan</td>
          </tr>
         <?php } ?>

        </tbody>
       </table>
      </div>
     </div>          
    </div> 
    <div class="row">
     <div class="col-md-12">
      <div class="pagination">
       <?php echo $pagination['links'] ?>
      </div>
     </div>
    </div>       
   </div>
  </div>
 </div>
</div>
