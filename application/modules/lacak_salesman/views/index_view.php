<input type="hidden" value="<?php echo $sales_id ?>" id="sales_id" class="form-control" />
<div class="content">
 <div class="animated fadeIn">
   <div class="box padding-16">   
    <div class="box-body">   
      <div class="row">
        <div class="col-md-12">
          <div class="form-group row">
            <label for="filterUser" class="col-sm-2 col-form-label">Pilih Salesman</label>
            <div class="col-sm-10">
              <select class="form-control required" id="salesman" 
                      error="Lacak_saleman" onchange="LacakSalesman.goPositionSales(this)">
                <option disabled selected></option>
                <!-- <option value="000">Semua salesman</option> -->
                <?php if (!empty($list_user)) { ?>
                  <?php foreach ($list_user as $value) { ?>
                  <?php $selected = '' ; ?>
                  <?php if (isset($sales_id)) { ?>
                    <?php $selected = $sales_id == $value['id'] ? 'selected' : '' ?>
                  <?php } ?>
                  <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['username'] ?></option>
                  <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>   
  <div id="map"></div>
 </div>
</div>
