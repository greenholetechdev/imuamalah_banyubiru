<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class="col-md-4">
      <input type="text" id="tanggal" class="form-control" readonly>
					</div>
					<div class="col-md-8">
							<button id="tampil" class="btn btn-primary" onclick="Lapnonpiutang.tampilkan(this)">Tampilkan</button>
							&nbsp;
      <a class="btn btn-success" download="<?php echo 'Laba Penjualan' ?>.xls" href="#" id="" onclick="return ExcellentExport.excel(this, 'tb_laporan', 'Laporan Penjualan');">Export</a>
					</div>
    </div>
    <br/>
    <br/>
    <div class="row">
     <div class="col-md-12">
      <div class='table-responsive' id="data_detail">
       <table class="table table-bordered table-list-draft" id="tb_laporan">
        <thead>
         <tr class="bg-primary-light text-white">
          <th>No</th>
          <th>Nomor</th>
          <th>Tanggal</th>
          <th>Jenis Pembayaran</th>
          <th>Keterangan</th>
          <th class="text-center">Jumlah</th>         
         </tr>
        </thead>
        <tbody>
									<?php if (!empty($data_piutang)) { ?>
										<?php $no = 1 ?>
          <?php foreach ($data_piutang as $value) { ?>
           <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $value['no_invoice'] ?></td>
            <td><?php echo $value['createddate'] ?></td>
            <td><?php echo $value['jenis'] ?></td>
            <td><?php echo $value['keterangan'] ?></td>
            <td class="text-right"><?php echo number_format($value['total'], 0, ',', '.') ?></td>
           </tr>
          <?php } ?>
         <?php } ?>         
        </tbody>
       </table>
      </div>
     </div>          
    </div>        
   </div>

  </div>
 </div>
</div>
