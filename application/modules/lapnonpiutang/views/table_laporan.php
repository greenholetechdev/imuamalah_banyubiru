<?php if (!empty($data_piutang)) { ?>
 <?php $no = 1 ?>
 <?php foreach ($data_piutang as $value) { ?>
  <tr>
   <td><?php echo $no++ ?></td>
   <td><?php echo $value['no_invoice'] ?></td>
   <td><?php echo $value['createddate'] ?></td>
   <td><?php echo $value['jenis'] ?></td>
   <td><?php echo $value['keterangan'] ?></td>
   <td class="text-right"><?php echo number_format($value['total'], 0, ',', '.') ?></td>
  </tr>
 <?php } ?>
<?php } ?>    