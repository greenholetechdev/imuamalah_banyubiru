<?php

class Lappenjualan extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'lappenjualan';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/printjs.js"></script>',
      '<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
      '<link rel="stylesheet" href="' . base_url() . 'assets/admin_lte/bower_components/bootstrap-daterangepicker/daterangepicker.css">',
      '<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/min/moment.min.js"></script>',
      '<script src="' . base_url() . 'assets/admin_lte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/lappenjualan.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'invoice';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Penjualan";
  $data['title_content'] = 'Data Penjualan';

//  echo 'asdasd';die;
  $content = $this->getDataPenjualanDetail();
  $data['data_penjualan'] = $content['data'];
  $data['jenis_bayar'] = $this->getListJenisBayar();
  echo Modules::run('template', $data);
 }

 public function getListJenisBayar() {
  $data = Modules::run('database/get', array(
              'table' => 'jenis_pembayaran',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataPenjualanDetail($keyword = '') {
  $sql = "select i.no_faktur
	, i.total
	, p.nama as customer
	, i.tanggal_faktur
	, jp.jenis
	 from invoice i
	join pembeli p ON
		p.id = i.pembeli
	join jenis_pembayaran jp
		on jp.id = i.jenis_pembayaran
	where i.deleted = 0";

  $data = Modules::run('database/get_custom', $sql);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;

  return array(
      'data' => $result,
  );
 }

 public function getDataPenjualanDetailSearch($tgl_awal, $tgl_akhir) {

  $where = "where i.createddate >= '" . $tgl_awal . "' and i.createddate <= '" . $tgl_akhir . "'";

  $jenis_bayar = $_POST['jenis_bayar'];
  if ($jenis_bayar != '') {
   $where .= ' and jp.id = '.$jenis_bayar;
  }
  $sql = "select i.no_faktur
	, i.total
	, p.nama as customer
	, i.tanggal_faktur
 , jp.jenis
	 from invoice i
	join pembeli p ON
		p.id = i.pembeli 
  join jenis_pembayaran jp
		on jp.id = i.jenis_pembayaran " . $where;

  $data = Modules::run('database/get_custom', $sql);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;

  return array(
      'data' => $result,
  );
 }

 public function tampilkan() {
  list($tgl_awal, $tgl_akhir) = explode('-', $_POST['tanggal']);
  $tgl_awal = date('Y-m-d', strtotime(trim($tgl_awal)));
  $tgl_akhir = date('Y-m-d', strtotime(trim($tgl_akhir)));

  $data_laporan = $this->getDataPenjualanDetailSearch($tgl_awal, $tgl_akhir);
  // echo '<pre>';
  // print_r($data_laporan);die;
  $data['data_penjualan'] = $data_laporan['data'];
  echo $this->load->view('table_laporan', $data, true);
 }

}
