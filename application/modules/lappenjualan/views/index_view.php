<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class="col-md-4">
      <input type="text" id="tanggal" class="form-control" readonly>
					</div>
     <div class="col-md-4">
      <select class="form-control required" error="Propinsi" id="jenis_bayar">
       <option value="">Pilih Jenis Bayar</option>
       <?php if (!empty($jenis_bayar)) { ?>
        <?php foreach ($jenis_bayar as $value) { ?>
         <option value="<?php echo $value['id'] ?>"><?php echo $value['jenis'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>
					<div class="col-md-4">
      <button id="tampil" class="btn btn-primary" onclick="LapPenjualan.tampilkan(this)">Tampilkan</button>
      &nbsp;
      <a class="btn btn-success" download="<?php echo 'Laba Penjualan' ?>.xls" href="#" id="" onclick="return ExcellentExport.excel(this, 'tb_laporan', 'Laporan Penjualan');">Export</a>
      &nbsp;
      <button id="cetak" class="btn btn-danger" onclick="LapPenjualan.cetak(this)">Cetak</button>      
					</div>
    </div>
    <br/>
    <br/>
    <div class="row">
     <div class="col-md-12">
      <div class='table-responsive' id="data_detail">
       <table class="table table-bordered table-list-draft" id="tb_laporan">
        <thead>
         <tr class="bg-primary-light text-white">
          <th>No</th>
          <th>Nomor</th>
          <th>Tanggal</th>
          <th>Pelanggan</th>
          <th>Jenis Pembayaran</th>
          <th class="text-center">Jumlah</th>         
         </tr>
        </thead>
        <tbody>
         <?php $total = 0 ?>
         <?php if (!empty($data_penjualan)) { ?>
          <?php $no = 1 ?>
          <?php foreach ($data_penjualan as $value) { ?>
           <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $value['no_faktur'] ?></td>
            <td><?php echo $value['tanggal_faktur'] ?></td>
            <td><?php echo $value['customer'] ?></td>
            <td><?php echo $value['jenis'] ?></td>
            <td class="text-right"><?php echo 'Rp. '.number_format($value['total'], 0, ',', '.') ?></td>
           </tr>
           <?php $total += $value['total'] ?>
          <?php } ?>
         <?php } ?>
         <tr>
          <td colspan="5" class="text-right">Total</td>
          <td class="text-right"><?php echo 'Rp. '.number_format($total) ?></td>
         </tr>
        </tbody>
       </table>
      </div>
     </div>          
    </div>        
   </div>

  </div>
 </div>
</div>
