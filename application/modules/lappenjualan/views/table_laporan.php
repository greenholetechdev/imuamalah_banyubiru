<?php $total = 0 ?>
<?php if (!empty($data_penjualan)) { ?>
 <?php $no = 1 ?>
 <?php foreach ($data_penjualan as $value) { ?>
  <tr>
   <td><?php echo $no++ ?></td>
   <td><?php echo $value['no_faktur'] ?></td>
   <td><?php echo $value['tanggal_faktur'] ?></td>
   <td><?php echo $value['customer'] ?></td>
   <td><?php echo $value['jenis'] ?></td>
   <td class="text-right"><?php echo 'Rp. ' . number_format($value['total'], 0, ',', '.') ?></td>
  </tr>
  <?php $total += $value['total'] ?>
 <?php } ?>
<?php } ?>
<tr>
 <td colspan="5" class="text-right">Total</td>
 <td class="text-right"><?php echo 'Rp. ' . number_format($total) ?></td>
</tr>