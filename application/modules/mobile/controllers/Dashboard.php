<?php

class Dashboard extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'Dashboard';
 }

 public function getTableName() {
  return 'order';
 }

 public function index() {
  echo 'Dashboard';
 }

 public function getTotalPenjualan(){
	$user = isset($_POST['user']) ? $_POST['user'] : '';
    $data = Modules::run('database/get', array(
        'table' => $this->getTableName() . ' o',
		'field' => array('sum(o.total) as total'),
		'join' => array(
			array('(select max(id) id, `order` from order_status group by `order`) iss', 'iss.order = o.id'),
			array('order_status isa', 'isa.id = iss.id'),
		),
        'where' => "o.deleted is null or o.deleted = 0 and isa.status = 'DRAFT' and (o.createdby = '".$user."' or o.createdby is null)"
    ));

    $result = array();
    if(!empty($data)){
        foreach ($data->result_array() as $value) {
			if($value['total'] == ''){
				$value['total'] = "0";
			}else{
				$value['total'] = number_format($value['total']);
			}
            array_push($result, $value);
        }
    }

    echo json_encode(array(
        'data'=> $result
    ));
 }

 public function getTotalReturBarang(){
	$user = isset($_POST['user']) ? $_POST['user'] : '3';
    $data = Modules::run('database/get', array(
		'table' => 'retur_order rp',
		'field' => array('sum(rpi.qty) total'),
		'join' => array(
			array('`order` i', 'i.id = rp.order'),
			array('(select max(id) id, retur_order from retur_order_item group by retur_order) rpp', 'rpp.retur_order = rp.id'),
			array('retur_order_item rpi', 'rpi.id = rpp.id'),
			array('pembeli p', 'p.id = i.pembeli'),
			array('(select max(id) id, `order` from order_status group by `order`) iss', 'iss.order = i.id'),
			array('order_status isa', 'isa.id = iss.id'),
			array('(select max(id) id, `order` from order_product group by `order`) ispp', 'ispp.order = i.id'),
			array('order_product isp', 'isp.id = ispp.id'),
			array('product_satuan ps', 'ps.id = isp.product_satuan'),
			array('product pro', 'pro.id = ps.product'),
			array('satuan sa', 'sa.id = ps.satuan'),

		),
		'where' => "rp.deleted is null or rp.deleted = 0 and (rp.createdby = '".$user."' or rp.createdby is null)",
		'orderby' => "rp.id desc"
	));

    $result = array();
    if(!empty($data)){
        foreach ($data->result_array() as $value) {
			if($value['total'] == ''){
				$value['total'] = "0";
			}
            array_push($result, $value);
        }
    }

    echo json_encode(array(
        'data'=> $result
    ));
 }
}
