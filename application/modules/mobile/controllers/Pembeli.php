<?php

class Pembeli extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'pembeli';
 }

 public function getTableName() {
  return 'pembeli';
 }

 public function index() {
  echo 'pembeli';
 }

 public function getListPembeli(){
    $data = Modules::run('database/get', array(
        'table' => $this->getTableName() . ' p',
        'field' => array('p.*'),
        'where' => "p.deleted is null or p.deleted = 0"
    ));

    $result = array();
    if(!empty($data)){
        foreach ($data->result_array() as $value) {
            array_push($result, $value);
        }
    }

    echo json_encode(array(
        'data'=> $result
    ));
 }
}
