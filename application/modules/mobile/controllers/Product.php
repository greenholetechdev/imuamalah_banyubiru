<?php

class Product extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'product';
 }

 public function getTableName() {
  return 'product';
 }

 public function index() {
  echo 'product';
 }

 public function getListProduct(){
    $data = Modules::run('database/get', array(
        'table' => $this->getTableName() . ' p',
		'field' => array('p.*', 's.nama_satuan', 
		'ps.harga', 'ps.id as product_satuan'),
		'join' => array(
			array('product_satuan ps', 'p.id = ps.product'),
			array('satuan s', 's.id = ps.satuan'),
		),
        'where' => "p.deleted is null or p.deleted = 0 and ps.deleted = 0"
    ));

    $result = array();
    if(!empty($data)){
        foreach ($data->result_array() as $value) {
			$value['val'] = $value['product'].' - '.$value['nama_satuan'].' - '.number_format($value['harga']);
            array_push($result, $value);
        }
    }

    echo json_encode(array(
        'data'=> $result
    ));
 }
}
