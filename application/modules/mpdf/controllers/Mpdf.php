<?php

class Mpdf extends MX_Controller{
 public function getInitPdf() {
  require_once APPPATH . '/third_party/vendor/autoload.php'; //php 7
  $mpdf = new \Mpdf\Mpdf();
  return $mpdf;
 }
 
 public function getInitPdfPaperA5() {
  require_once APPPATH . '/third_party/vendor/autoload.php'; //php 7
  //190mm wide x 236mm height
  $mpdf = new \Mpdf\Mpdf(['format' => [190, 236]]);
//  echo '<pre>';
//  var_dump(get_class_methods($mpdf));die;
  return $mpdf;
 }
}