<?php

class Notif_jatuh_tempo extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'notif_jatuh_tempo';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/notif_jatuh_tempo.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'notif_jatuh_tempo';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Jatuh Tempo";
  $data['title_content'] = 'Data Jatuh Tempo';
  $content = $this->getDataJatuhTempo();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataJatuhTempo($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('njt.nama_notif', $keyword),
   array('njt.jadwal_notif', $keyword),
   array('njt.jenis_notif', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
  'table' => $this->getTableName() . ' njt',
  'field' => array('njt.*'),
  'like' => $like,
  'is_or_like' => true,
  'where' => "njt.deleted is null or njt.deleted = 0"
  ));

  return $total;
 }

 public function getDataJatuhTempo($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('njt.nama_notif', $keyword),
   array('njt.jadwal_notif', $keyword),
   array('njt.jenis_notif', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' njt',
  'field' => array('njt.*'),
  'like' => $like,
  'is_or_like' => true,
  'limit' => $this->limit,
  'offset' => $this->last_no,
  'where' => "njt.deleted is null or njt.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
  'data' => $result,
  'total_rows' => $this->getTotalDataJatuhTempo($keyword)
  );
 }

 public function getDetailDataJatuhTempo($id) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' kr',
  'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Jatuh Tempo";
  $data['title_content'] = 'Tambah Jatuh Tempo';
  $data['jenis_pengiriman'] = $this->getJenisPengiriman();
  echo Modules::run('template', $data);
 }

 public function getDetailDataNotifPengiriman($id) {
  $data = Modules::run('database/get', array(
  'table' => 'notif_has_jenis_pengiriman',
  'where' => array('notif_jatuh_tempo' => $id)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getJenisPengiriman() {
  $data = Modules::run('database/get', array(
  'table' => 'jenis_notif_pengiriman',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function ubah($id) {
  $data = $this->getDetailDataJatuhTempo($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Jatuh Tempo";
  $data['title_content'] = 'Ubah Jatuh Tempo';
  $data['detail'] = $this->getDetailDataNotifPengiriman($id);
  $data['jenis_pengiriman'] = $this->getJenisPengiriman();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataJatuhTempo($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Jatuh Tempo";
  $data['title_content'] = 'Detail Jatuh Tempo';
  $data['detail'] = $this->getDetailDataNotifPengiriman($id);
  $data['jenis_pengiriman'] = $this->getJenisPengiriman();
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['nama_notif'] = $value->nama_notif;
  $data['jadwal_notif'] = $value->jadwal_notif;
  $data['jenis_notif'] = $value->jenis_notif;
  return $data;
 }

 public function getPostDataDetail($value, $jatuh_tempo) {
  $data['notif_jatuh_tempo'] = $jatuh_tempo;
  $data['jenis_notif_pengiriman'] = $value->value;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

  $id = $data->id;
//  echo '<pre>';
//  print_r($data);
//  die;
  $is_valid = false;
  $jatuh_tempo = $id;
  $this->db->trans_begin();
  try {
   $post_jatuh = $this->getPostDataHeader($data);
   if ($id == '') {
    $jatuh_tempo = Modules::run('database/_insert', $this->getTableName(), $post_jatuh);
    foreach ($data->detail as $value) {
     if ($value->checked == 1) {
      $post_detail = $this->getPostDataDetail($value, $jatuh_tempo);
      Modules::run('database/_insert', 'notif_has_jenis_pengiriman', $post_detail);
     }
    }
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_jatuh, array('id' => $id));
    foreach ($data->detail as $value) {
     if ($value->checked == 1) {
      if ($value->id == '') {
       $post_detail = $this->getPostDataDetail($value, $jatuh_tempo);
       Modules::run('database/_insert', 'notif_has_jenis_pengiriman', $post_detail);
      }
     } else {
      if ($value->id != '') {
       Modules::run('database/_delete', 'notif_has_jenis_pengiriman', array('id' => $value->id));
      }
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'jatuh_tempo' => $jatuh_tempo));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Jatuh Tempo";
  $data['title_content'] = 'Data Jatuh Tempo';
  $content = $this->getDataJatuhTempo($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
