<?php

class Payroll extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'payroll';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/payroll.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'payroll';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Gaji";
  $data['title_content'] = 'Data Gaji';
  $content = $this->getDataGaji();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataGaji($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_faktur', $keyword),
       array('p.tanggal_faktur', $keyword),
       array('p.tanggal_bayar', $keyword),
       array('pg.nama', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*', 'pr.month', 'pr.year',
                  'pg.nama as nama_pegawai'),
              'join' => array(
                  array('periode pr', 'p.periode = pr.id'),
                  array('pegawai pg', 'p.pegawai = pg.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "p.deleted is null or p.deleted = 0"
  ));

  return $total;
 }

 public function getDataGaji($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_faktur', $keyword),
       array('p.tanggal_faktur', $keyword),
       array('p.tanggal_bayar', $keyword),
       array('pg.nama', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*', 'pr.month', 'pr.year',
                  'pg.nama as nama_pegawai'),
              'join' => array(
                  array('periode pr', 'p.periode = pr.id'),
                  array('pegawai pg', 'p.pegawai = pg.id'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "p.deleted is null or p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['month_str'] = Modules::run('helper/getIndoDateMonth', $value['month']);
    $value['month_str'] = strtoupper(substr($value['month_str'], 0, 3));
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataGaji($keyword)
  );
 }

 public function getDetailDataGaji($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*', 'pr.month', 'pr.year',
                  'pg.nama as nama_pegawai'),
              'join' => array(
                  array('periode pr', 'p.periode = pr.id'),
                  array('pegawai pg', 'p.pegawai = pg.id'),
              ),
              'where' => "p.id = '" . $id . "'"
  ));

  $data = $data->row_array();
  $data['month_str'] = Modules::run('helper/getIndoDateMonth', $data['month']);
  $data['month_str'] = strtoupper(substr($data['month_str'], 0, 3));
  return $data;
 }

 public function getDetailDataInvoice($id) {
  $data = Modules::run('database/get', array(
              'table' => 'invoice i',
              'field' => array('i.*',
                  'p.nama as nama_pembeli', 'ist.status'),
              'join' => array(
                  array('pembeli p', 'i.pembeli = p.id'),
                  array('(select max(id) id, invoice from invoice_status group by invoice) iss', 'iss.invoice = i.id'),
                  array('invoice_status ist', 'ist.id = iss.id'),
              ),
              'where' => "i.id = '" . $id . "'"
  ));

  $result = $data->row_array();
  $result['total_ori'] = $result['total'];
  $result['total'] = number_format($result['total']);
  return $result;
 }

 public function getListPeriode() {
  $data = Modules::run('database/get', array(
              'table' => 'periode p',
              'where' => "p.deleted = 0 or p.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['month_str'] = Modules::run('helper/getIndoDateMonth', $value['month']);
    $value['month_str'] = strtoupper(substr($value['month_str'], 0, 3));
    array_push($result, $value);
   }
  }
//  echo '<pre>';
//  print_r($result);die;

  return $result;
 }

 public function getListPegawai() {
  $data = Modules::run('database/get', array(
              'table' => 'pegawai p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 or p.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListPajak() {
  $data = Modules::run('database/get', array(
              'table' => 'pajak p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 or p.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListMetodeBayar() {
  $data = Modules::run('database/get', array(
              'table' => 'metode_bayar',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListInvoice() {
  $data = Modules::run('database/get', array(
              'table' => 'invoice i',
              'field' => array('i.*'),
              'join' => array(
                  array('(select max(id) id, invoice from invoice_status group by invoice) iss', 'iss.invoice = i.id'),
                  array('invoice_status ist', 'ist.id = iss.id')
              ),
              'where' => "i.deleted = 0 and ist.status = 'DRAFT'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Gaji";
  $data['title_content'] = 'Tambah Gaji';
  $data['list_periode'] = $this->getListPeriode();
  $data['list_pegawai'] = $this->getListPegawai();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataGaji($id);
//  echo $data['total'];die;
//  echo '<pre>';
//  print_r($data);die;
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Gaji";
  $data['title_content'] = 'Ubah Gaji';
  $data['list_periode'] = $this->getListPeriode();
  $data['list_pegawai'] = $this->getListPegawai();
  $data['payroll_item'] = $this->getListPayrollItem($id);
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataGaji($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Gaji";
  $data['title_content'] = 'Detail Gaji';
  $data['payroll_item'] = $this->getListPayrollItem($id);
  echo Modules::run('template', $data);
 }

 public function getListPayrollItem($payroll) {
  $data = Modules::run('database/get', array(
              'table' => 'payroll_item pi',
              'field' => array('pi.*', 'pc.jenis', 'pc.angkutan'),
              'join' => array(
                  array('payroll_category pc', 'pc.id = pi.payroll_category'),
              ),
              'where' => "pi.payroll = '" . $payroll . "' and pi.deleted = 0",
              'orderby' => 'pi.id'
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getPostDataHeader($value) {
  $data['no_faktur'] = Modules::run('no_generator/generateNoGajiBayar');
  $data['tanggal_faktur'] = date('Y-m-d', strtotime($value->tanggal_faktur));
  $data['tanggal_bayar'] = date('Y-m-d', strtotime($value->tanggal_bayar));
  $data['periode'] = $value->periode;
  $data['pegawai'] = $value->pegawai;
  $data['keterangan'] = $value->keterangan;
  $data['total'] = str_replace('.', '', $value->jumlah);
  $data['jumlah_hari_masuk'] = $value->jumlah_hari;
  $data['jumlah_gaji_perhari'] = $value->gaji_hari;
  $data['jumlah_pengangkutan'] = $value->jumlah_angkut;
//  $data['jumlah_gaji_pengangkutan'] = $value->gaji_angkut;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post_data = $this->getPostDataHeader($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_data);


    //payroll item
    if (!empty($data->data_item)) {
     foreach ($data->data_item as $value) {
      $post_item['payroll'] = $id;
      $post_item['payroll_category'] = $value->payroll_category;
      $post_item['jumlah'] = $value->jumlah;
      $post_item['keterangan'] = $value->keterangan;
      Modules::run('database/_insert', 'payroll_item', $post_item);
     }
    }


    //create jurnal akuntan
    $jurnal = Modules::run('generate_jurnal/insertJurnal', $post_data['no_faktur']);
    $jurnal_struktur = Modules::run('generate_jurnal/getJurnalStruktur', 'Gaji');
//    echo '<pre>';
//    print_r($jurnal_struktur);die;
    if (!empty($jurnal_struktur)) {
     foreach ($jurnal_struktur as $value) {
      $post_detail['jurnal'] = $jurnal;
      $post_detail['jurnal_struktur'] = $value['id'];
      $post_detail['jumlah'] = str_replace('.', '', $data->jumlah);
      Modules::run('generate_jurnal/insertJurnalDetail', $post_detail);
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Gaji";
  $data['title_content'] = 'Data Gaji';
  $content = $this->getDataGaji($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getListKategori() {
  $data = Modules::run('database/get', array(
              'table' => 'payroll_category pc',
              'field' => array('pc.*'),
              'where' => "pc.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function addItem() {
  $data['list_kategori'] = $this->getListKategori();
//  echo '<pre>';
//  print_r($data);die;
  $data['index'] = $_POST['index'];
  echo $this->load->view('kategori_item', $data, true);
 }

 public function getListBank() {
  $data = Modules::run('database/get', array(
              'table' => 'bank',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getMetodeBayar() {
  $data['list_bank'] = $this->getListBank();
  $data['index'] = $_POST['index'];
  echo $this->load->view('bank_akun', $data, true);
 }

 public function printGaji($id) {

  $data = $this->getDetailDataGaji($id);
//  echo '<pre>';
//  print_r($data);die;
  $data['payroll_item'] = $this->getListPayrollItem($id);
  $data['self'] = Modules::run('general/getDetailDataGeneral', 1);
//  echo '<pre>';
//  print_r($data);die;
  $mpdf = Modules::run('mpdf/getInitPdf');

//  $pdf = new mPDF('A4');
  $view = $this->load->view('cetak', $data, true);
  $mpdf->WriteHTML($view);
  $mpdf->Output('Gaji Pegawai - ' . date('Y-m-d') . '.pdf', 'I');
 }

 public function getDetailInvoice() {
  $invoice = $_POST['invoice'];
  $data['invoice'] = $this->getDetailDataInvoice($invoice);
  $data['invoice_item'] = $this->getListInvoiceItem($invoice);

  $data['view_item'] = $this->load->view('product_item', $data, true);

  echo json_encode($data);
 }

}
