<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 
    <div class="row">
     <div class='col-md-3 text-bold'>
      No Faktur
     </div>
     <div class='col-md-3'>
      <?php echo $no_faktur ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Pegawai
     </div>
     <div class='col-md-3'>
      <?php echo $nama_pegawai ?>
					</div>     
					
     <div class='col-md-3 text-bold'>
						Jumlah Hari Masuk
     </div>
     <div class='col-md-3'>
      <?php echo $jumlah_hari_masuk.' Hari' ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Faktur
     </div>
     <div class='col-md-3'>
      <?php echo $tanggal_faktur ?>
     </div>     
					
					<div class='col-md-3 text-bold'>
						Jumlah Gaji per Hari
     </div>
     <div class='col-md-3'>
      <?php echo 'Rp, '.number_format($jumlah_gaji_perhari) ?>
     </div>     
    </div>
    <br/>
    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Bayar
     </div>
     <div class='col-md-3'>
      <?php echo $tanggal_bayar ?>
     </div>     
					
					<div class='col-md-3 text-bold'>
						Jumlah Pengangkutan
     </div>
     <div class='col-md-3'>
      <?php echo $jumlah_pengangkutan ?>
     </div>     
    </div>
    <br/>        
    <div class="row">
     <div class='col-md-3 text-bold'>
      Periode
     </div>
     <div class='col-md-3'>
      <?php echo $month_str . $year ?>
     </div>     
					
<!--					<div class='col-md-3 text-bold'>
						Jumlah Gaji per Pengangkutan
     </div>
     <div class='col-md-3'>
      <?php echo 'Rp, '.number_format($jumlah_gaji_pengangkutan) ?>
     </div>     -->
    </div>
    <br/>        
    <div class="row">
     <div class='col-md-3 text-bold'>
      Keterangan
     </div>
     <div class='col-md-3'>
      <?php echo $keterangan ?>
     </div>     
    </div>
    <br/>        
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Gaji</u>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class="col-md-12">
      <div class="table-responsive">
       <table class="table table-striped table-bordered table-list-draft" id="tb_product">
        <thead>
         <tr class="bg-primary-light text-white">
          <th>Kategori</th>
          <th>Jumlah</th>
          <th>Keterangan</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($payroll_item)) { ?>
          <?php foreach ($payroll_item as $value) { ?>
           <tr> 
            <td><?php echo $value['jenis'] ?></td>
            <td><?php echo 'Rp, ' . number_format($value['jumlah']) ?></td>
            <td><?php echo $value['keterangan'] ?></td>
           </tr>
          <?php } ?>
         <?php } ?>       
        </tbody>
       </table>
      </div>
     </div>
    </div>

    <div class="row">
     <div class="col-md-12 text-right">
      <h4>Total : Rp, <label id="total"><?php echo number_format($total) ?></label></h4>
     </div>
    </div>
    <br/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger" onclick="Payroll.cetak('<?php echo isset($id) ? $id : '' ?>')">Cetak</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Payroll.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
