<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 
    <div class="row">
     <div class='col-md-3 text-bold'>
      Nama Pegawai
     </div>
     <div class='col-md-3'>
      <?php echo $nama_pegawai ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Awal
     </div>
     <div class='col-md-3'>
      <?php echo $tanggal_awal ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Akhir
     </div>
     <div class='col-md-3'>
      <?php echo $tanggal_akhir ?>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
<!--      <button id="" class="btn btn-danger" onclick="PegawaiKontrak.cetak('<?php echo isset($id) ? $id : '' ?>')">Cetak</button>
      &nbsp;-->
      <button id="" class="btn btn-danger-baru" onclick="PegawaiKontrak.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
