<?php

class Pelanggan extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'pelanggan';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/pelanggan.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'pembeli';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pelanggan";
  $data['title_content'] = 'Data Pelanggan';
  $content = $this->getDataPelanggan();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataPelanggan($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('p.nama', $keyword),
   array('p.no_hp', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
  'table' => $this->getTableName() . ' p',
  'field' => array('p.*'),
  'like' => $like,
  'is_or_like' => true,
  'where' => "p.deleted = 0 or p.deleted is null"
  ));

  return $total;
 }

 public function getDataPelanggan($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('p.nama', $keyword),
   array('p.no_hp', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' p',
  'field' => array('p.*'),
  'like' => $like,
  'is_or_like' => true,
  'limit' => $this->limit,
  'offset' => $this->last_no,
  'where' => "p.deleted = 0 or p.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
  'data' => $result,
  'total_rows' => $this->getTotalDataPelanggan($keyword)
  );
 }

 public function getDetailDataPelanggan($id) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' p',
  'where' => "p.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Pelanggan";
  $data['title_content'] = 'Tambah Pelanggan';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataPelanggan($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Pelanggan";
  $data['title_content'] = 'Ubah Pelanggan';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataPelanggan($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Pelanggan";
  $data['title_content'] = 'Detail Pelanggan';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['nama'] = $value->nama;
  $data['no_hp'] = $value->no_hp;
  $data['email'] = $value->email;
  $data['alamat'] = $value->alamat;
  $data['pembeli_kategori'] = 2;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $palanggan = $id;
  $this->db->trans_begin();
  try {
   $post_pelanggan = $this->getPostDataHeader($data);
   if ($id == '') {
    $palanggan = Modules::run('database/_insert', $this->getTableName(), $post_pelanggan);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_pelanggan, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'pelanggan'=> $palanggan));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pelanggan";
  $data['title_content'] = 'Data Pelanggan';
  $content = $this->getDataPelanggan($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }
}
