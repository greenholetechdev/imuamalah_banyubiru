<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'FORM' ?></h3>
   </div>
   <div class="box-body">   
    <div class="row">
     <div class='col-md-2 text-bold'>
      Nama
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='nama' class='form-control required' 
             value='<?php echo isset($nama) ? $nama : '' ?>' error="Nama"/>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-2 text-bold'>
      Alamat
     </div>
     <div class='col-md-3'>
      <textarea id="alamat" class="form-control required" error="Alamat"><?php echo isset($alamat) ? $alamat : '' ?></textarea>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-2 text-bold'>
      No HP
     </div>
     <div class='col-md-3 text-left'>
      <input type='text' name='' id='no_hp' class='form-control required' 
             value='<?php echo isset($no_hp) ? $no_hp : '' ?>' error="No HP"/>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-2 text-bold'>
      Email
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='email' class='form-control required' 
             value='<?php echo isset($email) ? $email : '' ?>' error="Email"/>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button type="submit" class="btn btn-success" onclick="Pelanggan.simpan('<?php echo isset($id) ? $id : '' ?>', event)"><i class="fa fa-check"></i>&nbsp;Proses</button>
      <!--<button id="" class="btn btn-succes-baru" onclick="Pelanggan.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>-->
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Pelanggan.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
