<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class='card'>
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   
   <div class='card-body card-block'>
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Tagihan</u>
     </div>
    </div> 
    <hr/>

    <div class="row">
     <div class='col-md-3'>
      Tagihan
     </div>
     <div class='col-md-3'>
      <?php echo $jenis_tagihan ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Keterangan
     </div>
     <div class='col-md-3'>
      <?php echo $keterangan ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3'>
      Total Pembayaran
     </div>
     <div class='col-md-3'>
      <?php echo 'Rp. ' . number_format($total, 2, ',', '.') ?>
     </div>     
    </div>
    <br/>   
    
    <div class="row">
     <div class='col-md-3'>
      Jenis Pembayaran
     </div>
     <div class='col-md-3'>
      <?php echo $jenis ?>
     </div>     
    </div>
    <br/>   
    
    <div class="row">
     <div class='col-md-3'>
      Tanggal Pembayaran
     </div>
     <div class='col-md-3'>
      <?php echo date('d F Y', strtotime($tgl_bayar)) ?>
     </div>     
    </div>
    <br/>   
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-baru" onclick="Pembayaran.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
