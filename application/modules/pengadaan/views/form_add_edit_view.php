<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Vendor
     </div>
     <div class='col-md-3'>
      <select class="form-control required" id="vendor" error="Vendor">
       <option value="">Pilih Vendor</option>
       <?php if (!empty($list_vendor)) { ?>
        <?php foreach ($list_vendor as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($vendor)) { ?>
          <?php $selected = $vendor == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama_vendor'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Faktur
     </div>
     <div class='col-md-3'>
      <input type='text' readonly="" name='' id='tanggal' class='form-control required' 
             value='<?php echo isset($tanggal) ? $tanggal : '' ?>' error="Tanggal Faktur"/>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Keterangan
     </div>
     <div class='col-md-3'>
      <textarea id='keterangan' class='form-control required' error="Keterangan"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
     </div>     
    </div>
    <br/>
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Produk</u>
     </div>
    </div>
    <br/>

    <?php echo $this->load->view('form_product') ?>

    <div class="row">
     <div class="col-md-10 text-right">
      <h4>Total : Rp, <label id="total"><?php echo isset($total) ? number_format($total) : '0' ?></label></h4>
     </div>
    </div>

    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="Pengadaan.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Pengadaan.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
