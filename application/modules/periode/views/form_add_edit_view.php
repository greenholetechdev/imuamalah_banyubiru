<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">  
    <div class="row">
     <div class='col-md-3 text-bold'>
      Bulan
     </div>
     <div class='col-md-3'>
      <input value="<?php echo date('m') ?>" type='number' name='' min="1" max="12" id='month' class='form-control required' 
             value='<?php echo isset($month) ? $month : '' ?>' error="Bulan"/>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3 text-bold'>
      Tahun
     </div>
     <div class='col-md-3'>
      <input type='number' value="<?php echo date('Y') ?>" min="2017" max="<?php echo date('Y') ?>" name='' id='year' class='form-control required' 
             value='<?php echo isset($year) ? $year : '' ?>' error="Tahun"/>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="Periode.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Periode.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
