<?php

class Persyaratan extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'persyaratan';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/persyaratan.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'persyaratan';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Persyaratan";
  $data['title_content'] = 'Data Persyaratan';
  $content = $this->getDataPersyaratan();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataPersyaratan($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('p.syarat', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
  'table' => $this->getTableName() . ' p',
  'field' => array('p.*'),
  'like' => $like,
  'is_or_like' => true,
  'where' => "p.deleted is null or p.deleted = 0"
  ));

  return $total;
 }

 public function getDataPersyaratan($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('p.syarat', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' p',
  'field' => array('p.*'),
  'like' => $like,
  'is_or_like' => true,
  'limit' => $this->limit,
  'offset' => $this->last_no,
  'where' => "p.deleted is null or p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
  'data' => $result,
  'total_rows' => $this->getTotalDataPersyaratan($keyword)
  );
 }

 public function getDetailDataPersyaratan($id) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' p',
  'field' => array('p.*', 'ja.jenis', 'ka.kategori'),
  'join' => array(
  array('jenis_akad ja', 'p.jenis_akad = ja.id'),
  array('kategori_akad ka', 'p.kategori_akad = ka.id'),
  ),
  'where' => "p.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListKategoriAkad() {
  $data = Modules::run('database/get', array(
  'table' => 'kategori_akad',
  'where'=> "deleted = 0 or deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function getListJenisAkad() {
  $data = Modules::run('database/get', array(
  'table' => 'jenis_akad',
  'where'=> "deleted = 0 or deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Persyaratan";
  $data['title_content'] = 'Tambah Persyaratan';
  $data['list_kategori'] = $this->getListKategoriAkad();
  $data['list_jenis'] = $this->getListJenisAkad();
		$data['kategori_akad'] = '';
		$data['jenis_akad'] = '';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataPersyaratan($id);
  $data['detail'] = $this->getDetailBerkasPersyaratan($id);

  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Persyaratan";
  $data['title_content'] = 'Ubah Persyaratan';
  $data['list_kategori'] = $this->getListKategoriAkad();
  $data['list_jenis'] = $this->getListJenisAkad();
  echo Modules::run('template', $data);
 }

 public function getDetailBerkasPersyaratan($persyaratan) {
  $data = Modules::run('database/get', array(
  'table' => 'persyaratan_has_berkas phb',
  'where' => array('phb.persyaratan' => $persyaratan)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataPersyaratan($id);
  $data['detail'] = $this->getDetailBerkasPersyaratan($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Persyaratan";
  $data['title_content'] = 'Detail Persyaratan';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['syarat'] = $value->syarat;
  $data['keterangan'] = $value->keterangan;
  $data['jenis_akad'] = $value->jenis_akad;
  $data['kategori_akad'] = $value->kategori_akad;

  return $data;
 }

 public function getPostDataDetail($value, $syarat) {
  $data['persyaratan'] = $syarat;
  $data['nama_berkas'] = $value->nama_berkas;
  $data['status'] = $value->status;

  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);
//  die;
  $id = $this->input->post('id');
  $is_valid = false;
  $syarat = $id;
  $this->db->trans_begin();
  try {
   $post_syarat = $this->getPostDataHeader($data);
   if ($id == '') {
    $syarat = Modules::run('database/_insert', $this->getTableName(), $post_syarat);

    foreach ($data->detail as $value) {
     $post_syarat_detail = $this->getPostDataDetail($value, $syarat);
     Modules::run('database/_insert', 'persyaratan_has_berkas', $post_syarat_detail);
    }
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_syarat, array('id' => $id));
    foreach ($data->detail as $value) {
     $post_syarat_detail = $this->getPostDataDetail($value, $syarat);
     if ($value->id == '') {      
      if ($value->nama_berkas != '') {
       Modules::run('database/_insert', 'persyaratan_has_berkas', $post_syarat_detail);
      }
     } else {
      if ($value->nama_berkas != '') {
       //update       
       Modules::run('database/_update', 'persyaratan_has_berkas', $post_syarat_detail, array('id' => $value->id));
      }
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'syarat' => $syarat));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Persyaratan";
  $data['title_content'] = 'Data Persyaratan';
  $content = $this->getDataPersyaratan($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function uploadData($name_of_field) {
  $config['upload_path'] = 'files/berkas/persyaratan/';
  $config['allowed_types'] = 'gif|jpg|png|pdf';
  $config['max_size'] = '1000';
  $config['max_width'] = '2000';
  $config['max_height'] = '2000';

  $this->load->library('upload', $config);
  $this->upload->do_upload($name_of_field);
 }
 
 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
