<?php

class Potongan_angsuran extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'potongan_angsuran';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/printjs.js"></script>',
      '<script src="' . base_url() . 'assets/js/excellentexport.min.js"></script>',
      '<link rel="stylesheet" href="' . base_url() . 'assets/admin_lte/bower_components/bootstrap-daterangepicker/daterangepicker.css">',
      '<script src="' . base_url() . 'assets/admin_lte/bower_components/moment/min/moment.min.js"></script>',
      '<script src="' . base_url() . 'assets/admin_lte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/potongan_angsuran.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'payroll';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Angsuran";
  $data['title_content'] = 'Data Angsuran';

//  echo 'asdasd';die;
  $content = $this->getDataAngsuranDetail();
//  echo '<pre>';
//  print_r($content);die;
  $data['data_penjualan'] = $content['data'];
  $data['jenis_bayar'] = $this->getListJenisBayar();
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getListJenisBayar() {
  $data = Modules::run('database/get', array(
              'table' => 'jenis_pembayaran',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getTotalDataAngsuranDetail($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('pg.nama', $keyword),
       array('p.no_faktur', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*'),
              'join' => array(
                  array('payroll_item pi', 'pi.payroll = p.id'),
                  array('payroll_category pc', 'pc.id = pi.payroll_category'),
                  array('pegawai pg', 'pg.id = p.pegawai'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "p.deleted = 0 and pc.jenis = 'Potongan Angsuran'"
  ));

  return $total;
 }

 public function getDataAngsuranDetail($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('pg.nama', $keyword),
       array('p.no_faktur', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*', 'pi.jumlah', 'pg.nama as nama_pegawai'),
              'join' => array(
                  array('payroll_item pi', 'pi.payroll = p.id'),
                  array('payroll_category pc', 'pc.id = pi.payroll_category'),
                  array('pegawai pg', 'pg.id = p.pegawai'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "p.deleted = 0 and pc.jenis = 'Potongan Angsuran'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataAngsuranDetail($keyword)
  );
 }

 public function getDataAngsuranDetailSearch($tgl_awal, $tgl_akhir) {

  $where = "where i.createddate >= '" . $tgl_awal . "' and i.createddate <= '" . $tgl_akhir . "'";

  $jenis_bayar = $_POST['jenis_bayar'];
  if ($jenis_bayar != '') {
   $where .= ' and jp.id = ' . $jenis_bayar;
  }
  $sql = "select i.no_faktur
	, i.total
	, p.nama as customer
	, i.tanggal_faktur
 , jp.jenis
	 from invoice i
	join pembeli p ON
		p.id = i.pembeli 
  join jenis_pembayaran jp
		on jp.id = i.jenis_pembayaran " . $where;

  $data = Modules::run('database/get_custom', $sql);

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

//  echo '<pre>';
//  print_r($result);die;

  return array(
      'data' => $result,
  );
 }

 public function tampilkan() {
  list($tgl_awal, $tgl_akhir) = explode('-', $_POST['tanggal']);
  $tgl_awal = date('Y-m-d', strtotime(trim($tgl_awal)));
  $tgl_akhir = date('Y-m-d', strtotime(trim($tgl_akhir)));

  $data_laporan = $this->getDataAngsuranDetailSearch($tgl_awal, $tgl_akhir);
  // echo '<pre>';
  // print_r($data_laporan);die;
  $data['data_penjualan'] = $data_laporan['data'];
  echo $this->load->view('table_laporan', $data, true);
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Pengguna";
  $data['title_content'] = 'Data Pengguna';
  $content = $this->getDataAngsuranDetail();
//  echo '<pre>';
//  print_r($content);die;
  $data['data_penjualan'] = $content['data'];
  $data['jenis_bayar'] = $this->getListJenisBayar();
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/search/'.$keyword, $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

}
