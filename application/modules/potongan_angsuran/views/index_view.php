<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <!--     <div class="col-md-4">
           <input type="text" id="tanggal" class="form-control" readonly>
          </div>-->
     <!--     <div class="col-md-4">
           <select class="form-control required" error="Jenis Bayar" id="jenis_bayar">
            <option value="">Pilih Jenis Bayar</option>
     <?php if (!empty($jenis_bayar)) { ?>
      <?php foreach ($jenis_bayar as $value) { ?>
                  <option value="<?php echo $value['id'] ?>"><?php echo $value['jenis'] ?></option>
      <?php } ?>
     <?php } ?>
           </select>
          </div>-->
     <!--					<div class="col-md-4">
           <button id="tampil" class="btn btn-primary" onclick="PotonganAngsuran.tampilkan(this)">Tampilkan</button>
           &nbsp;
           <a class="btn btn-success" download="<?php echo 'Laba Penjualan' ?>.xls" href="#" id="" onclick="return ExcellentExport.excel(this, 'tb_laporan', 'Laporan Penjualan');">Export</a>
           &nbsp;
           <button id="cetak" class="btn btn-danger" onclick="PotonganAngsuran.cetak(this)">Cetak</button>      
          </div>-->

     <div class="col-md-12">
      <div class="input-group">
       <input type="text" class="form-control" onkeyup="PotonganAngsuran.search(this, event)" id="keyword" placeholder="Pencarian">
       <span class="input-group-addon"><i class="fa fa-search"></i></span>
      </div>
     </div>
    </div>
    <br/>
    
    <div class='row'>
     <div class='col-md-12'>
      <?php if (isset($keyword)) { ?>
       <?php if ($keyword != '') { ?>
        Cari Data : "<b><?php echo $keyword; ?></b>"
       <?php } ?>
      <?php } ?>
     </div>
    </div>
    <br/>
    <div class="row">
     <div class="col-md-12">
      <div class='table-responsive' id="data_detail">
       <table class="table table-bordered table-list-draft" id="tb_laporan">
        <thead>
         <tr class="bg-primary-light text-white">
          <th>No</th>
          <th>Nomor Faktur</th>
          <th>Tanggal</th>
          <th>Pegawai</th>
          <th class="text-center">Jumlah</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($data_penjualan)) { ?>
          <?php $no = $pagination['last_no'] +1; ?>
          <?php foreach ($data_penjualan as $value) { ?>
           <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $value['no_faktur'] ?></td>
            <td><?php echo $value['tanggal_faktur'] ?></td>
            <td><?php echo $value['nama_pegawai'] ?></td>
            <td class="text-right"><?php echo 'Rp. ' . number_format($value['jumlah'], 0, ',', '.') ?></td>
           </tr>
          <?php } ?>
         <?php } ?>
        </tbody>
       </table>
      </div>
     </div>          
    </div>        
   </div>


   <div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-right">
     <?php echo $pagination['links'] ?>
    </ul>
   </div>
  </div>
 </div>
</div>
