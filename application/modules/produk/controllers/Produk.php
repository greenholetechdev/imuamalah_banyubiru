<?php

class Produk extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'produk';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/produk.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'product';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Produk";
  $data['title_content'] = 'Data Produk';
  $content = $this->getDataProduk();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataProduk($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('r.product', $keyword),
       array('tr.tipe', $keyword),
       array('kr.kategori', $keyword)
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' r',
              'field' => array('r.*', 'tr.tipe', 'kr.kategori'),
              'join' => array(
                  array('kategori_product kr', 'r.kategori_product = kr.id'),
                  array('tipe_product tr', 'r.tipe_product = tr.id')
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "r.deleted = 0 or r.deleted is null"
  ));

  return $total;
 }

 public function getDataProduk($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('r.product', $keyword),
       array('tr.tipe', $keyword),
       array('kr.kategori', $keyword)
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' r',
              'field' => array('r.*', 'tr.tipe', 'kr.kategori'),
              'join' => array(
                  array('kategori_product kr', 'r.kategori_product = kr.id'),
                  array('tipe_product tr', 'r.tipe_product = tr.id')
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "r.deleted = 0 or r.deleted is null"
  ));

//  echo '<pre>';
//  print_r($data->result_array());die;
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataProduk($keyword)
  );
 }

 public function getDetailDataProduk($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' r',
              'field' => array('r.*',
//                  'rhhjc.harga as harga_cash',
//                  'rhhjk.harga harga_kredit',
                  'kr.kategori as product_kategori',
                  'tr.tipe'),
              'join' => array(
//                  array('product_has_harga_jual_pokok rhhjc', 'r.id = rhhjc.product and rhhjc.period_end is null'),
//                  array('product_has_harga_jual_tunai rhhjk', 'r.id = rhhjk.product and rhhjk.period_end is null'),
                  array('kategori_product kr', 'r.kategori_product = kr.id'),
                  array('tipe_product tr', 'r.tipe_product = tr.id')
              ),
              'where' => "r.id = '" . $id . "'"
  ));

//  echo "<pre>";
//  echo $this->db->last_query();
//  die;
//  echo '<pre>';
//  print_r($data->row_array());
//  die;
  return $data->row_array();
 }

 public function getListTipeProduk() {
  $data = Modules::run('database/get', array(
              'table' => 'tipe_product',
              'where' => "deleted is null or deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListProdukKategori() {
  $data = Modules::run('database/get', array(
              'table' => 'kategori_product',
              'where' => "deleted is null or deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListTenor() {
  $data = Modules::run('database/get', array(
              'table' => 'ansuran',
              'where' => "deleted is null or deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListStatusProduk() {
  $data = Modules::run('database/get', array(
              'table' => 'status_obat',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_new';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Produk";
  $data['title_content'] = 'Tambah Produk';
  $data['list_tipe'] = $this->getListTipeProduk();
  $data['list_rk'] = $this->getListProdukKategori();
//  $data['list_tenor'] = $this->getListTenor();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataProduk($id);
//  echo '<pre>';
//  print_r($data);
//  die;
//  $data['detail'] = $this->getDetailHargaAnsuran($id);
  $data['view_file'] = 'form_add_edit_new';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Produk";
  $data['title_content'] = 'Ubah Produk';
  $data['list_tipe'] = $this->getListTipeProduk();
  $data['list_rk'] = $this->getListProdukKategori();
//  $data['list_tenor'] = $this->getListTenor();
  $data['data_image'] = $this->getDataImageProduk($id);
  echo Modules::run('template', $data);
 }

 public function getDetailHargaAnsuran($product) {
  $data = Modules::run('database/get', array(
              'table' => 'product_has_harga_angsuran rhha',
              'field' => array('rhha.*', 'a.periode_tahun'),
              'join' => array(
                  array('ansuran a', 'rhha.ansuran = a.id')
              ),
              'where' => "rhha.product = '" . $product . "' and rhha.period_end is null and (a.deleted is null or a.deleted = 0)"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDataImageProduk($product) {
  $data = Modules::run('database/get', array(
              'table' => 'product_has_foto',
              'where' => array('product' => $product, 'status' => null)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataProduk($id);
//  $data['detail'] = $this->getDetailHargaAnsuran($id);
  $data['view_file'] = 'detail_view_new';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Produk";
  $data['title_content'] = 'Detail Produk';
  $data['list_tipe'] = $this->getListTipeProduk();
  $data['list_rk'] = $this->getListProdukKategori();
  $data['list_tenor'] = $this->getListTenor();
  $data['data_image'] = $this->getDataImageProduk($id);
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['kode_product'] = $value->kode_product;
  $data['tipe_product'] = 1;
  $data['kategori_product'] = 1;
  $data['product'] = $value->product;
  $data['keterangan'] = $value->keterangan;
  return $data;
 }

 public function getPostDataHargaCash($value, $product) {
  $data['product'] = $product;
  $data['harga'] = str_replace('.', '', $value->harga_cash);
  $data['period_start'] = date('Y-m-d');
  $data['diskon'] = 0;
  return $data;
 }

 public function getPostDataHargaKredit($value, $product) {
  $data['product'] = $product;
  $data['harga'] = str_replace('.', '', $value->harga_kredit);
  $data['period_start'] = date('Y-m-d');
  $data['diskon'] = 0;
  return $data;
 }

 public function getPostHargaAnsuran($value, $product) {
  $data['product'] = $product;
  $data['harga'] = str_replace('.', '', $value->harga);
  $data['ansuran'] = $value->ansuran;
  $data['harga_total'] = str_replace('.', '', $value->harga_total);
  $data['period_start'] = date('Y-m-d');
  return $data;
 }

 public function getPostProdukHasFoto($value, $product) {
  $data['product'] = $product;
  $data['foto'] = $value->image;
  return $data;
 }

 public function isExistHargaCash($product, $harga) {
  $data = Modules::run('database/get', array(
              'table' => 'product_has_harga_jual_pokok',
              'where' => array('product' => $product, 'harga' => $harga, 'period_end' => null)
  ));

  $is_exist = false;
  if (!empty($data)) {
   $data = $data->row_array();
   $is_exist = true;
  }

  return $is_exist;
 }

 public function isExistHargaKredit($product, $harga) {
  $data = Modules::run('database/get', array(
              'table' => 'product_has_harga_jual_tunai',
              'where' => array('product' => $product, 'harga' => $harga, 'period_end' => null)
  ));

  $is_exist = false;
  if (!empty($data)) {
   $data = $data->row_array();
   $is_exist = true;
  }

  return $is_exist;
 }

 public function isExistHargaAngsuran($product, $harga) {
  $data = Modules::run('database/get', array(
              'table' => 'product_has_harga_angsuran',
              'where' => array('product' => $product, 'harga' => $harga, 'period_end' => null)
  ));

  $is_exist = false;
  if (!empty($data)) {
   $data = $data->row_array();
   $is_exist = true;
  }

  return $is_exist;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
//  echo '<pre>';
//  print_r($data);die;
  $id = $this->input->post('id');

  $product = $id;
  $is_valid = false;
  $this->db->trans_begin();
  try {
   $post = $this->getPostDataHeader($data);
   if ($id == '') {
    $product = Modules::run('database/_insert', $this->getTableName(), $post);
   } else {
    //update
    $product = $id;
    Modules::run('database/_update', $this->getTableName(), $post, array('id' => $id));
   }
   $this->db->trans_commit();
   $product = $product;
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'product' => $product));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Produk";
  $data['title_content'] = 'Data Produk';
  $content = $this->getDataProduk($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function ubahHarga($id, $product_has_satuan) {
  $harga = $this->input->post('harga');
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', 'produk_satuan_has_harga', array('period_end' => date('Y-m-d')), array('id' => $id));
   $post['produk_has_satuan'] = $product_has_satuan;
   $post['harga'] = $harga;
   $post['period_start'] = date('Y-m-d');
   Modules::run('database/_insert', 'produk_satuan_has_harga', $post);
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function listFotoProduk($product) {
  $data['data_image'] = $this->getDataImageProduk($product);
//  echo '<pre>';
//  print_r($data);die;
  echo $this->load->view('list_foto_product', $data, true);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
