<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Tipe Produk
     </div>
     <div class='col-md-3'>
      <?php echo $tipe ?>
     </div>     
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-3 text-bold'>
      Nama Produk
     </div>
     <div class='col-md-3'>
      <?php echo $product ?>
     </div>
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-3 text-bold'>
      Produk Kategori
     </div>
     <div class='col-md-3'>
      <?php echo $product_kategori ?>
     </div>
    </div>
    <br/>


    <div class='row'>
     <div class='col-md-3 text-bold'>
      Keterangan Produk
     </div>
     <div class='col-md-3'>
      <?php $keterangan ?>
     </div>
    </div>
    <br/>

    <hr/>

    <div class='row'>
     <div class='col-md-3 text-bold'>
      Harga Pokok Produk
     </div>
     <div class='col-md-3'>
      <?php echo 'Rp. ' . number_format($harga_cash, 2, ',', '.') ?>
     </div>
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-3 text-bold'>
      Harga Jual Produk (Tunai)
     </div>
     <div class='col-md-3'>
      <?php echo 'Rp. ' . number_format($harga_kredit, 2, ',', '.') ?>
     </div>
    </div>
    <hr/>

    <div class='row'>
     <div class='col-md-3 text-bold'>
      <u>Foto Produk</u>
     </div>
    </div>
    <br/>    

    <div class='row'>
     <?php foreach ($data_image as $v_image) { ?>
      <div class='col-md-4'>
       <div class=''>
        <img src="<?php echo $v_image['foto'] ?>" width="300" height="300"/>
       </div>       
      </div>
     <?php } ?>
    </div>
    <br/>
    <hr/>

    <div class='row'>
     <div class='col-md-3 text-bold'>
      <u>Detail Harga Angsuran</u>
     </div>
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-12'>
      <table class="table table-striped table-bordered table-list-draft" id="list_ansuran">
       <thead>
        <tr class="bg-primary-light text-white">
         <th>No</th>
         <th>Harga Ansguran</th>
         <th>Tenor</th>
         <th>Harga Total</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($detail)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($detail as $value) { ?>
          <tr class="edit"> 
           <td><?php echo $no++ ?></td>
           <td>
            <?php echo 'Rp. ' . number_format($value['harga'], 2, ',', '.') ?>
           </td>
           <td class="text-center">
            <?php echo $value['periode_tahun'] ?>
           </td>
           <td class="text-center">
            <?php echo 'Rp. '.number_format($value['harga_total'], 2, ',', '.') ?>
           </td>
          </tr>
         <?php } ?>
        <?php } ?>
       </tbody>
      </table>
     </div>
    </div>
    <hr/>

    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-baru" onclick="Produk.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
