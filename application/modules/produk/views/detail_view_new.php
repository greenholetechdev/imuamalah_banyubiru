<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   

    <div class='row'>
				<div class='col-md-3 text-bold'>
      Kode Product
     </div>
     <div class='col-md-3'>
      <?php echo $kode_product ?>
     </div>
    </div>
    <br/>
				
				<div class='row'>
     <div class='col-md-3 text-bold'>
      Nama Produk
     </div>
     <div class='col-md-3'>
      <?php echo $product ?>
     </div>     
    </div>
    <br/>
   
    <div class='row'>
     <div class='col-md-3 text-bold'>
      Keterangan Produk
     </div>
     <div class='col-md-3'>
      <?php $keterangan ?>
     </div>
    </div>
    <br/>
    <hr/>

    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-baru" onclick="Produk.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
