<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-header with-border" style="margin-top: 12px;">
    <h3 class="box-title"><i class="fa fa-file-text-o"></i>&nbsp;<?php echo 'FORM' ?></h3>
   </div>
   <br/>
   <div class="box-body box-block">

    <div class='row'>
     <div class='col-md-3 text-bold'>
      Kode Produk
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='kode_product' class='form-control required' 
             error="Kode Produk" value='<?php echo isset($kode_product) ? $kode_product : '' ?>' placeholder=""/>
     </div>     
    </div>
    <br/>
    <div class='row'>
    <div class='col-md-3 text-bold'>
      Nama Produk
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='product' class='form-control required' 
             error="Nama Produk" value='<?php echo isset($product) ? $product : '' ?>' placeholder=""/>
     </div>
    </div>
    <br/>

    <div class='row'>    
     <div class='col-md-3 text-bold'>
      Keterangan Produk
     </div>
     <div class='col-md-3'>
      <textarea class="form-control required" error="Detail Produk Produk" id="keterangan"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
     </div>  
    </div>
    <br/>
    <hr/>

    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="Produk.simpan('<?php echo isset($id) ? $id : '' ?>')"><i class="fa fa-check"></i>&nbsp;Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Produk.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
