<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Nama Rak
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='nama_rak' class='form-control required' 
             value='<?php echo isset($nama_rak) ? $nama_rak : '' ?>' error="Nama Rak"/>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="Rak.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Rak.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
