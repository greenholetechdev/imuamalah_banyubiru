<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      No Faktur Penjualan
     </div>
     <div class='col-md-3'>
      <select class="form-control required" id="faktur" 
              error="Faktur" onchange="ReturPelanggan.getFakturDetail(this)">
       <option value="">Pilih Faktur</option>
       <?php if (!empty($list_faktur)) { ?>
        <?php foreach ($list_faktur as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($invoice)) { ?>
          <?php $selected = $invoice == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['no_faktur'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Produk</u>
     </div>
    </div>
    <br/>

    <div class="form-item">
     <?php echo $this->load->view('form_product') ?>
    </div>

    <div class="row">
     <div class="col-md-10 text-right">
      <h4>Total : Rp, <label id="total"><?php echo isset($total) ? number_format($total) : '0' ?></label></h4>
     </div>
    </div>
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Retur</u>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal Retur
     </div>
     <div class='col-md-3'>
      <input readonly="" type='text' name='' id='tanggal_retur' class='form-control required' 
             value='<?php echo isset($tanggal) ? $tanggal : '' ?>' error="Tanggal Retur"/>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Keterangan
     </div>
     <div class='col-md-3'>
      <textarea  id='keterangan_retur' class='form-control required' error="Keterangan"><?php echo isset($keterangan) ? $keterangan : '' ?></textarea>
     </div>     
    </div>
    <br/>
    <hr/>
    
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="ReturPelanggan.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="ReturPelanggan.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
