<?php

class Retur_pengadaan extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'retur_pengadaan';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/retur_pengadaan.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'procurement_retur';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Retur";
  $data['title_content'] = 'Data Retur';
  $content = $this->getDataRetur();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataRetur($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_faktur', $keyword),
       array('p.tanggal', $keyword),
       array('p.keterangan', $keyword),
       array('pt.no_faktur', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*', 'pt.no_faktur as no_pengadaan'),
              'join' => array(
                  array('procurement pt', 'pt.id = p.procurement')
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "p.deleted is null or p.deleted = 0"
  ));

  return $total;
 }

 public function getDataRetur($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('p.no_faktur', $keyword),
       array('p.tanggal', $keyword),
       array('p.keterangan', $keyword),
       array('pt.no_faktur', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*', 'pt.no_faktur as no_pengadaan'),
              'join' => array(
                  array('procurement pt', 'pt.id = p.procurement')
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "p.deleted is null or p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataRetur($keyword)
  );
 }

 public function getDetailDataRetur($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' p',
              'field' => array('p.*', 'v.nama_vendor', 
                  'pt.no_faktur as no_pengadaan', 
                  'pt.tanggal as tanggal_pengadaan',
                  'pt.keterangan as ket_pengadaan'),
              'join' => array(
                  array('procurement pt', 'pt.id = p.procurement'),
                  array('vendor v', 'v.id = pt.vendor'),
              ),
              'where' => "p.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListNoPengadaan() {
  $data = Modules::run('database/get', array(
              'table' => 'procurement',
              'where' => "deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Retur";
  $data['title_content'] = 'Tambah Retur';
  $data['list_pengadaan'] = $this->getListNoPengadaan();
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataRetur($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Retur";
  $data['title_content'] = 'Ubah Retur';
  $data['list_vendor'] = $this->getListVendor();
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataRetur($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Retur";
  $data['title_content'] = 'Detail Retur';
  $data['list_retur_item'] = $this->getProcReturItem($id);
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function getProcReturItem($id) {
  $data = Modules::run('database/get', array(
              'table' => 'procurement_retur_item pri',
              'field' => array('pri.*',
                  'p.product as nama_product', 
                  's.nama_satuan',
                  'pi.harga',
                  'pi.qty as qty_total'),
              'join' => array(
                  array('procurement_item pi', 'pi.id = pri.procurement_item'),
                  array('product p', 'pi.product = p.id'),
                  array('satuan s', 'pi.satuan = s.id'),
              ),
              'where' => "pri.deleted = 0 and pri.procurement_retur = '" . $id . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getPostDataHeader($value) {
  $data['no_faktur'] = Modules::run('no_generator/generateNoFakturReturPengadaan');
  $data['procurement'] = $value->procurement;
  $data['tanggal'] = date('Y-m-d', strtotime($value->tanggal));
  $data['total'] = str_replace('.', '', $value->total);
  $data['keterangan'] = $value->keterangan;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));  
  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post_data = $this->getPostDataHeader($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_data);

    //procuremtn item
    if (!empty($data->retur_item)) {
     foreach ($data->retur_item as $value) {
      $proc_item['procurement_retur'] = $id;
      $proc_item['procurement_item'] = $value->proc_item;
      $proc_item['qty'] = $value->jumlah;
      $proc_item['sub_total'] = str_replace(',', '', $value->sub_total);

      Modules::run('database/_insert', 'procurement_retur_item', $proc_item);
     }
    }
//
//    //procurement status
//    $proc_status['procurement'] = $id;
//    $proc_status['status'] = 'DRAFT';
//    Modules::run('database/_insert', 'procurement_status', $proc_status);

    //create jurnal akuntan
    $jurnal = Modules::run('generate_jurnal/insertJurnal', $post_data['no_faktur']);
    $jurnal_struktur = Modules::run('generate_jurnal/getJurnalStruktur', 'Retur Pembelian');
    if (!empty($jurnal_struktur)) {
     foreach ($jurnal_struktur as $value) {
      $post_detail['jurnal'] = $jurnal;
      $post_detail['jurnal_struktur'] = $value['id'];
      $post_detail['jumlah'] = str_replace('.', '', $data->total);
      Modules::run('generate_jurnal/insertJurnalDetail', $post_detail);
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Retur";
  $data['title_content'] = 'Data Retur';
  $content = $this->getDataRetur($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

 public function getListProduct() {
  $data = Modules::run('database/get', array(
              'table' => 'product p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0 or p.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListSatuan() {
  $data = Modules::run('database/get', array(
              'table' => 'product_satuan ps',
              'field' => array('s.nama_satuan', 's.id as satuan'),
              'join' => array(
                  array('product p', 'ps.product = p.id'),
                  array('satuan s', 'ps.satuan = s.id', 'left'),
              ),
              'where' => "ps.deleted = 0 or ps.deleted is null"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function addItem() {
  $data['list_product'] = $this->getListProduct();
  $data['list_satuan'] = $this->getListSatuan();
  $data['index'] = $_POST['index'];
  echo $this->load->view('product_item', $data, true);
 }

 public function addSatuanContent() {
  $data['list_satuan'] = $this->getListSatuan();
  $data['tr_index'] = $_POST['tr_index'];
  $data['urutan'] = $_POST['urutan'];
  echo $this->load->view('list_item', $data, true);
 }

 public function printFaktur($id) {

  $data['proc'] = $this->getDetailDataRetur($id);
  $data['proc_item'] = $this->getProcReturItem($id);
  $data['self'] = Modules::run('general/getDetailDataGeneral', 1);
  $mpdf = Modules::run('mpdf/getInitPdf');

//  $pdf = new mPDF('A4');
  $view = $this->load->view('cetak', $data, true);
  $mpdf->WriteHTML($view);
  $mpdf->Output('Nota Retur - ' . date('Y-m-d') . '.pdf', 'I');
 }

 public function getDetailProcurement($id) {
  $data = Modules::run('database/get', array(
              'table' => 'procurement p',
              'field' => array('p.*', 'v.nama_vendor'),
              'join' => array(
                  array('vendor v', 'v.id = p.vendor')
              ),
              'where' => "p.deleted = 0 and p.id = '".$id."'"
  ));
  
  return $data->row_array();
 }
 
 public function getListProcItem($proc) {
  $data = Modules::run('database/get', array(
              'table' => 'procurement_item pi',
              'field' => array('pi.*', 'p.product as nama_product', 's.nama_satuan'),
              'join' => array(
                  array('product p', 'p.id = pi.product'),
                  array('satuan s', 's.id = pi.satuan'),
              ),
              'where' => "pi.deleted = 0 and pi.procurement = '".$proc."'"
  ));
  
  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }
 
 public function getPengadaanDetail() {
  $procurement = $_POST['procurement'];
  $data = $this->getDetailProcurement($procurement);
  
  $data_item = $this->getListProcItem($procurement);
  $content['invoice_item'] = $data_item;
  $view_item = $this->load->view('form_product', $content, true);
  echo json_encode(array('data' => $data, 'view_item' => $view_item));
 }
}
