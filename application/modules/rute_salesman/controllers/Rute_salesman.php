<?php

class Rute_salesman extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'rute_salesman';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/rute_salesman.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'sales_rute';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Rute";
  $data['title_content'] = 'Data Rute';
  $content = $this->getDataRute();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataRute($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('sr.no_rute', $keyword),
       array('m.nama', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' sr',
              'field' => array('sr.*', 'm.nama as nama_minggu'),
              'join' => array(
                  array('minggu m', 'sr.minggu = m.id')
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "sr.deleted is null or sr.deleted = 0"
  ));

  return $total;
 }

 public function getDataRute($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('sr.no_rute', $keyword),
       array('m.nama', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' sr',
              'field' => array('sr.*', 'm.nama as nama_minggu'),
              'join' => array(
                  array('minggu m', 'sr.minggu = m.id')
              ),
              'like' => $like,
              'is_or_like' => true,
              'limit' => $this->limit,
              'offset' => $this->last_no,
              'where' => "sr.deleted is null or sr.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataRute($keyword)
  );
 }

 public function getDetailDataRute($id) {
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' kr',
              'field' => array('kr.*', 'm.nama as nama_minggu'),
              'join' => array(
                  array('minggu m', 'kr.minggu = m.id')
              ),
              'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function getListMinggu() {
  $data = Modules::run('database/get', array(
              'table' => 'minggu',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListHari() {
  $data = Modules::run('database/get', array(
              'table' => 'hari',
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListSales() {
  $data = Modules::run('database/get', array(
              'table' => 'user u',
              'field' => array('u.*', 'p.nama'),
              'join' => array(
                  array('priveledge pv', 'pv.id = u.priveledge'),
                  array('pegawai p', 'p.id = u.pegawai')
              ),
              'where' => "u.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getListCustomer() {
  $data = Modules::run('database/get', array(
              'table' => 'pembeli p',
              'field' => array('p.*'),
              'where' => "p.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Rute";
  $data['title_content'] = 'Tambah Rute';
  $data['list_minggu'] = $this->getListMinggu();
  $data['list_hari'] = $this->getListHari();
  $data['list_sales'] = $this->getListSales();
  $data['list_customer'] = $this->getListCustomer();
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataRute($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Rute";
  $data['title_content'] = 'Ubah Rute';
  $data['list_minggu'] = $this->getListMinggu();
  $data['list_hari'] = $this->getListHari();
  $data['list_sales'] = $this->getListSales();
  $data['list_customer'] = $this->getListCustomer();

  $data['data_hari'] = $this->getDetailHari($id);
  $data['data_sales'] = $this->getDetailSales($id);
  $data['data_customer'] = $this->getDetailCustomer($id);
  echo Modules::run('template', $data);
 }

 public function getDetailHari($sales_rute) {
  $data = Modules::run('database/get', array(
              'table' => 'sales_rute_hari srh',
              'field' => array('srh.*', 'h.nama as nama_hari'),
              'join' => array(
                  array('hari h', 'srh.hari = h.id')
              ),
              'where' => "srh.deleted = 0 and srh.sales_rute = '" . $sales_rute . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDetailSales($sales_rute) {
  $data = Modules::run('database/get', array(
              'table' => 'sales_rute_user srh',
              'field' => array('srh.*', 'p.nama as nama_pegawai'),
              'join' => array(
                  array('user u', 'srh.user = u.id'),
                  array('pegawai p', 'u.pegawai = p.id'),
              ),
              'where' => "srh.deleted = 0 and srh.sales_rute = '" . $sales_rute . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function getDetailCustomer($sales_rute) {
  $data = Modules::run('database/get', array(
              'table' => 'sales_rute_customer srh',
              'field' => array('srh.*', 'p.nama as nama_customer'),
              'join' => array(
                  array('pembeli p', 'srh.pembeli = p.id'),
              ),
              'where' => "srh.deleted = 0 and srh.sales_rute = '" . $sales_rute . "'"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function detail($id) {
  $data = $this->getDetailDataRute($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Rute";
  $data['title_content'] = 'Detail Rute';
  $data['data_hari'] = $this->getDetailHari($id);
  $data['data_sales'] = $this->getDetailSales($id);
  $data['data_customer'] = $this->getDetailCustomer($id);
//  echo '<pre>';
//  print_r($data);die;
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['no_rute'] = Modules::run('no_generator/generateNoRute');
  $data['minggu'] = $value->minggu;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));

  $id = $this->input->post('id');
  $is_valid = false;

  $this->db->trans_begin();
  try {
   $post_data = $this->getPostDataHeader($data);
   if ($id == '') {
    $id = Modules::run('database/_insert', $this->getTableName(), $post_data);

    //sales hari
    if (!empty($data->hari)) {
     foreach ($data->hari as $value) {
      $post_hari['hari'] = $value;
      $post_hari['sales_rute'] = $id;
      Modules::run('database/_insert', 'sales_rute_hari', $post_hari);
     }
    }

    //sales user
    if (!empty($data->sales)) {
     foreach ($data->sales as $value) {
      $post_sales['user'] = $value;
      $post_sales['sales_rute'] = $id;
      Modules::run('database/_insert', 'sales_rute_user', $post_sales);
     }
    }

    //sales customer
    if (!empty($data->customer)) {
     foreach ($data->customer as $value) {
      $post_cus['pembeli'] = $value;
      $post_cus['sales_rute'] = $id;
      Modules::run('database/_insert', 'sales_rute_customer', $post_cus);
     }
    }
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_data, array('id' => $id));

    //sales hari
    Modules::run('database/_delete', 'sales_rute_hari', array('sales_rute'=> $id));
    if (!empty($data->hari)) {
     foreach ($data->hari as $value) {
      $post_hari['hari'] = $value;
      $post_hari['sales_rute'] = $id;
      Modules::run('database/_insert', 'sales_rute_hari', $post_hari);
     }
    }

    //sales user
    Modules::run('database/_delete', 'sales_rute_user', array('sales_rute'=> $id));
    if (!empty($data->sales)) {
     foreach ($data->sales as $value) {
      $post_sales['user'] = $value;
      $post_sales['sales_rute'] = $id;
      Modules::run('database/_insert', 'sales_rute_user', $post_sales);
     }
    }

    //sales customer
    Modules::run('database/_delete', 'sales_rute_customer', array('sales_rute'=> $id));
    if (!empty($data->customer)) {
     foreach ($data->customer as $value) {
      $post_cus['pembeli'] = $value;
      $post_cus['sales_rute'] = $id;
      Modules::run('database/_insert', 'sales_rute_customer', $post_cus);
     }
    }
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Rute";
  $data['title_content'] = 'Data Rute';
  $content = $this->getDataRute($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
