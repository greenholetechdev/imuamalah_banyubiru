<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block"> 
    <div class="row">
     <div class='col-md-3 text-bold'>
      Minggu
     </div>
     <div class='col-md-3'>
      <?php echo $nama_minggu ?>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Hari
     </div>
     <div class='col-md-3'>
      <?php if (!empty($data_hari)) { ?>
       <?php foreach ($data_hari as $value) { ?>
        <?php echo $value['nama_hari'] . ', ' ?>
       <?php } ?>
      <?php } ?>
     </div>     
    </div>
    <br/>
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Sales</u>
     </div>
    </div>
    <br/>
    <div class="row">
     <div class="col-md-5">
      <div class="table-responsive">
       <table class="table table-striped table-bordered table-list-draft">
       <thead>
        <tr class="bg-primary-light text-white">
         <th>No</th>
         <th>Sales</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($data_sales)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($data_sales as $value) { ?>
          <tr>
           <td><?php echo $no++ ?></td>
           <td><?php echo $value['nama_pegawai'] ?></td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td colspan="6" class="text-center">Tidak ada data ditemukan</td>
         </tr>
        <?php } ?>

       </tbody>
      </table>
      </div>
     </div>
    </div>
    <br/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Customer</u>
     </div>
    </div>
    <br/>
    <div class="row">
     <div class="col-md-5">
      <div class="table-responsive">
       <table class="table table-striped table-bordered table-list-draft">
       <thead>
        <tr class="bg-primary-light text-white">
         <th>No</th>
         <th>Customer</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($data_customer)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($data_customer as $value) { ?>
          <tr>
           <td><?php echo $no++ ?></td>
           <td><?php echo $value['nama_customer'] ?></td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td colspan="6" class="text-center">Tidak ada data ditemukan</td>
         </tr>
        <?php } ?>

       </tbody>
      </table>
      </div>
     </div>
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger-baru" onclick="RuteSales.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
