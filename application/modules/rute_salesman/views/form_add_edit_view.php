<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Minggu
     </div>
     <div class='col-md-3'>
      <select class="form-control "id="minggu">
       <option value="">Pilih Minggu</option>
       <?php if (!empty($list_minggu)) { ?>
        <?php foreach ($list_minggu as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($minggu)) { ?>
          <?php $selected = $minggu == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Hari
     </div>
     <div class='col-md-3'>
      <select class="form-control select2" multiple="multiple" 
              data-placeholder="Pilih Hari"
              style="width: 100%;" id="hari">
       <option value="">Pilih Hari</option>
       <?php if (!empty($list_hari)) { ?>
        <?php foreach ($list_hari as $value) { ?>         
         <?php $selected = ""; ?>
         <?php if (isset($data_hari)) { ?>       
          <?php foreach ($data_hari as $v_hari) { ?>
           <?php if ($v_hari['hari'] == $value['id']) { ?>
            <?php $selected = "selected"; ?>       
            <?php break; ?>       
           <?php } ?>           
          <?php } ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Sales
     </div>
     <div class='col-md-3'>
      <select class="form-control select2" multiple="multiple" 
              data-placeholder="Pilih Sales"
              style="width: 100%;" id="sales">
       <option value="">Pilih Sales</option>
       <?php if (!empty($list_sales)) { ?>
        <?php foreach ($list_sales as $value) { ?>   
         <?php $selected = ""; ?>
         <?php if (isset($data_sales)) { ?>       
          <?php foreach ($data_sales as $v_sales) { ?>
           <?php if ($v_sales['user'] == $value['id']) { ?>
            <?php $selected = "selected"; ?>
            <?php break; ?>
           <?php } ?>           
          <?php } ?>           
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Customer
     </div>
     <div class='col-md-3'>
      <select class="form-control select2" multiple="multiple" 
              data-placeholder="Pilih Customer"
              style="width: 100%;" id="customer">
       <option value="">Pilih Customer</option>
       <?php if (!empty($list_customer)) { ?>
        <?php foreach ($list_customer as $value) { ?>   
         <?php $selected = ""; ?>
         <?php if (isset($data_customer)) { ?>       
          <?php foreach ($data_customer as $v_cus) { ?>
           <?php if ($v_cus['pembeli'] == $value['id']) { ?>
            <?php $selected = "selected"; ?>
            <?php break; ?>
           <?php } ?>           
          <?php } ?>           
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="RuteSales.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="RuteSales.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
