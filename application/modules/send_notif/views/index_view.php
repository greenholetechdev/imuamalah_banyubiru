<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class="col-md-3">
      &nbsp;
     </div>
     <div class="col-md-9">
      <div class="input-group">
       <input type="text" class="form-control" onkeyup="SendNotif.search(this, event)" id="keyword" placeholder="Pencarian">
       <span class="input-group-addon"><i class="fa fa-search"></i></span>
      </div>
     </div>
    </div>
    <br/>
    <div class='row'>
     <div class='col-md-12'>
      <?php if (isset($keyword)) { ?>
       <?php if ($keyword != '') { ?>
        Cari Data : "<b><?php echo $keyword; ?></b>"
       <?php } ?>
      <?php } ?>
     </div>
    </div>
    <br/>
    <div class='row'>
     <div class='col-md-6'>
      <?php if (!empty($jatuh_tempo)) { ?>
       <?php $message = "* Data yang tampil "; ?>
       <?php $message .= '<i><b>' . $jatuh_tempo['jadwal_notif'] . '</b></i> Hari '; ?>
       <?php $message .= $jatuh_tempo['jenis_notif'] == '+' ? '<i><b>Setelah</b></i>' : '<i><b>Sebelum</b></i>' ?> 
       <?php $message .= " Periode Jatuh Tempo" ?>
       <?php echo $message ?>
      <?php } ?>      
     </div>
    </div>    
    <hr/>
    <br/>
    <div class="row">
     <div class="col-md-12">
      <div class='table-responsive'>
       <table class="table table-striped table-bordered table-list-draft">
        <thead>
         <tr class="bg-primary-light text-white">
          <th>No</th>
          <th>No Faktur</th>
          <th>Total</th>
          <th>Action</th>
         </tr>
        </thead>
        <tbody>
         <?php if (!empty($content)) { ?>
          <?php $no = 1; ?>
          <?php foreach ($content as $value) { ?>
           <tr>
            <td><?php echo $no++ ?></td>
            <td><?php echo $value['no_faktur'] ?></td>
            <td><?php echo number_format($value['total']) ?></td>
            <td class="text-center">
             <button no_hp="<?php echo $value['no_hp'] ?>" 
                     email="<?php echo $value['email'] ?>" 
                     no_faktur="<?php echo $value['no_faktur'] ?>" 
                     id="" class="btn btn-warning-baru font12" 
                     onclick="SendNotif.send(this, '<?php echo $value['id'] ?>')">Kirim Notif</button>
             &nbsp;
            </td>
           </tr>
          <?php } ?>
         <?php } else { ?>
          <tr>
           <td colspan="8" class="text-center">Tidak ada data ditemukan</td>
          </tr>
         <?php } ?>

        </tbody>
       </table>
      </div>
     </div>          
    </div> 
    <div class="row">
     <div class="col-md-12">
      <div class="pagination">
       <?php echo $pagination['links'] ?>
      </div>
     </div>
    </div>       
   </div>
  </div>
 </div>
</div>
