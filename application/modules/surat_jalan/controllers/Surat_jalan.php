<?php

class Surat_jalan extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'surat_jalan';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/surat_jalan.js"></script>'
		);

		return $data;
	}

	public function getTableName()
	{
		return 'surat_jalan';
	}

	public function index()
	{
		$this->segment = 3;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;

		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Surat";
		$data['title_content'] = 'Data Surat';
		$content = $this->getDataSurat();
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function getTotalDataSurat($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('sr.tanggal_faktur', $keyword),
				array('sr.keterangan', $keyword),
			);
		}
		$total = Modules::run('database/count_all', array(
			'table' => $this->getTableName() . ' sr',
			'field' => array('sr.*', 'i.no_faktur'),
			'join' => array(
				array('invoice i', 'sr.invoice = i.id')
			),
			'like' => $like,
			'is_or_like' => true,
			'where' => "sr.deleted is null or sr.deleted = 0"
		));

		return $total;
	}

	public function getDataSurat($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('sr.tanggal_faktur', $keyword),
				array('sr.keterangan', $keyword),
			);
		}
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' sr',
			'field' => array('sr.*', 'i.no_faktur'),
			'join' => array(
				array('invoice i', 'sr.invoice = i.id')
			),
			'like' => $like,
			'is_or_like' => true,
			'limit' => $this->limit,
			'offset' => $this->last_no,
			'where' => "sr.deleted is null or sr.deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		return array(
			'data' => $result,
			'total_rows' => $this->getTotalDataSurat($keyword)
		);
	}

	public function getDetailDataSurat($id)
	{
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' sr',
			'field' => array('sr.*', 'i.no_faktur', 'p.nama as nama_pembeli'),
			'join' => array(
				array('invoice i', 'sr.invoice = i.id'),
				array('pembeli p', 'i.pembeli = p.id'),
			),
			'where' => "sr.id = '" . $id . "'"
		));

		return $data->row_array();
	}

	public function getListInvoice()
	{
		$data = Modules::run('database/get', array(
			'table' => 'invoice i',
			'field' => array('i.*'),
			'join' => array(
				array('(select max(id) id, invoice from invoice_status group by invoice) iss', 'iss.invoice = i.id'),
				array('invoice_status ist', 'ist.id = iss.id')
			),
			'where' => "i.deleted = 0 and ist.status = 'DRAFT'"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function add()
	{
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tambah Surat";
		$data['title_content'] = 'Tambah Surat';
		$data['list_invoice'] = $this->getListInvoice();
		echo Modules::run('template', $data);
	}

	public function ubah($id)
	{
		$data = $this->getDetailDataSurat($id);
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Ubah Surat";
		$data['title_content'] = 'Ubah Surat';
		$data['list_invoice'] = $this->getListInvoice();
		echo Modules::run('template', $data);
	}

	public function detail($id)
	{
		$data = $this->getDetailDataSurat($id);
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Surat";
		$data['title_content'] = 'Detail Surat';
		$data_item = $this->getListInvoiceItem($data['invoice']);
		$data_biaya = $this->getListBiayaItem($data['id']);
		
		$data['invoice_item'] = $data_item['data'];
		$data['biaya_item'] = $data_biaya;
		$data['biaya_item_invoice'] = Modules::run('faktur_pelanggan/getListBiayaItem', $data['invoice']);
//  echo '<pre>';
// 		 print_r($data['biaya_item_invoice']);die;
		echo Modules::run('template', $data);
	}

	public function getListBiayaItem($sj)
	{
		$data = Modules::run('database/get', array(
			'table' => 'surat_jalan_biaya sb',
			'field' => array('sb.*'),
			'where' => "sb.surat_jalan = '" . $sj . "' and sb.deleted = 0",
			'orderby' => 'sb.id'
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getPostDataHeader($value)
	{
		$data['no_surat_jalan'] = Modules::run('no_generator/generateNoSuratJalan');
		$data['invoice'] = $value->invoice;
		$data['tanggal_faktur'] = date('Y-m-d', strtotime($value->tanggal));
		$data['total'] = str_replace('.', '', $value->total);
		return $data;
	}

	public function simpan()
	{
		$data = json_decode($this->input->post('data'));

		  // echo '<pre>';
		  // print_r($data);die;
		$id = $this->input->post('id');
		$is_valid = false;

		$this->db->trans_begin();
		try {
			$post_data = $this->getPostDataHeader($data);

			if ($id == '') {
				$id = Modules::run('database/_insert', $this->getTableName(), $post_data);

				//insert biaya
				if (!empty($data->biaya_item)) {
					foreach ($data->biaya_item as $v_biaya) {
						$post_biaya['surat_jalan'] = $id;
						$post_biaya['ket_biaya'] = $v_biaya->keterangan_biaya;
						$post_biaya['jumlah'] = $v_biaya->biaya;

						Modules::run('database/_insert', 'surat_jalan_biaya', $post_biaya);
					}
				}
			} else {
				//update
				Modules::run('database/_update', $this->getTableName(), $post_data, array('id' => $id));
			}
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
	}

	public function search($keyword)
	{
		$this->segment = 4;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;
		$keyword = urldecode($keyword);

		$data['keyword'] = $keyword;
		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Surat";
		$data['title_content'] = 'Data Surat';
		$content = $this->getDataSurat($keyword);
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function delete($id)
	{
		$is_valid = false;
		$this->db->trans_begin();
		try {
			Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function getListInvoiceItem($invoice)
	{
		$data = Modules::run('database/get', array(
			'table' => 'invoice_product ip',
			'field' => array(
				'ip.*', 's.nama_satuan',
				'p.product as nama_product', 'ps.harga', 'p.id as product_id'
			),
			'join' => array(
				array('product_satuan ps', 'ps.id = ip.product_satuan'),
				array('product p', 'p.id = ps.product'),
				array('satuan s', 's.id = ps.satuan'),
			),
			'where' => "ip.deleted = 0 and ip.invoice = '" . $invoice . "'"
		));

		$result = array();
		$total = 0;
		if (!empty($data)) {
			$temp = array();
			foreach ($data->result_array() as $value) {
				$sub_total = 0;
				$product = $value['product_id'];
				$satuan = array();
				$harga = array();
				$qty = array();
				if (!in_array($product, $temp)) {
					$detail = array();
					foreach ($data->result_array() as $v_child) {
						if ($product == $v_child['product_id']) {
							$sub_total += $v_child['sub_total'];
							array_push($satuan, $v_child['nama_satuan']);
							array_push($qty, $v_child['qty']);
							array_push($harga, 'Rp, ' . number_format($v_child['harga']));
							array_push($detail, $v_child);
						}
					}
					$value['detail'] = $detail;
					$value['sub_total'] = $sub_total;
					$value['satuan_str'] = implode(' + ', $satuan);
					$value['harga_str'] = implode('<br/>', $harga);
					$value['qty_str'] = implode('<br/>', $qty);
					$total += $sub_total;
					array_push($result, $value);
					$temp[] = $product;
				}
			}
		}

		//  echo '<pre>';
		//  print_r($result);
		//  die;

		return array(
			'data' => $result,
			'total' => $total
		);
	}

	public function printFaktur($id)
	{
		$data['sj'] = $this->getDetailDataSurat($id);
		$data['invoice_item'] = $this->getListInvoiceItem($data['sj']['invoice']);
		$data['biaya_item_invoice'] = Modules::run('faktur_pelanggan/getListBiayaItem', $data['sj']['invoice']);
		$data['biaya_item'] = $this->getListBiayaItem($data['sj']['id']);
		$data['self'] = Modules::run('general/getDetailDataGeneral', 1);
		//  echo '<pre>';
		//  print_r($data);die;
		// $mpdf = Modules::run('mpdf/getInitPdf');

		//  $pdf = new mPDF('A4');
		$view = $this->load->view('cetak_html', $data, true);
		echo $view;
		// $mpdf->WriteHTML($view);
		// $mpdf->Output('Nota Surat - ' . date('Y-m-d') . '.pdf', 'I');
	}

	public function getFakturDetail()
	{
		$faktur = $_POST['faktur'];

		$data_item = $this->getListInvoiceItem($faktur);
		$biaya_item_invoice = Modules::run('faktur_pelanggan/getListBiayaItem', $faktur);
		 // echo '<pre>';
		 // print_r($biaya_item_invoice);die;
		$content['invoice_item'] = $data_item['data'];
		$content['biaya_item_invoice'] = $biaya_item_invoice;
		$view_item = $this->load->view('form_product', $content, true);
		echo json_encode(array('view_item' => $view_item));
	}

	public function addBiayaLain()
	{
		$data['index'] = $_POST['index'];
		echo $this->load->view('biaya_item', $data, true);
	}
}
