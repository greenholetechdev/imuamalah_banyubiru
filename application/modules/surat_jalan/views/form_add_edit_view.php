<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Faktur
     </div>
     <div class='col-md-3'>
      <select class="form-control required"
              id="invoice" error="Faktur" onchange="SuratJalan.getFakturDetail(this)">
       <option value="">Pilih Faktur</option>
       <?php if (!empty($list_invoice)) { ?>
        <?php foreach ($list_invoice as $value) { ?>
         <?php $selected = '' ?>
         <?php if (isset($invoice)) { ?>
          <?php $selected = $invoice == $value['id'] ? 'selected' : '' ?>
         <?php } ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['no_faktur'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Tanggal
     </div>
     <div class='col-md-3'>
      <input type='text' readonly="" name='' id='tanggal' class='form-control required' 
             value='<?php echo isset($tanggal) ? $tanggal : '' ?>' error="Tanggal"/>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class="col-md-12">
      <u>Data Produk</u>
     </div>
    </div>
    <hr/>

    <div class="row">
     <div class="col-md-12">
      <div class="form-item">
       <?php echo $this->load->view('form_product'); ?>
      </div>      
     </div>
    </div>
    <br/>
    
    <div class="row">
     <div class="col-md-10 text-right">
      <h4>Total : Rp, <label id="total"><?php echo isset($total) ? number_format($total) : '0' ?></label></h4>
     </div>
    </div>
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="SuratJalan.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="SuratJalan.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
