<div class="row">
 <div class="col-md-10">
  <div class="table-responsive">
   <table class="table table-striped table-bordered table-list-draft" id="tb_product">
    <thead>
     <tr class="bg-primary-light text-white">
      <th>Produk</th>
      <th>Satuan</th>
      <th>Harga</th>
      <th>Jumlah</th>
      <th>Sub Total</th>
     </tr>
    </thead>
    <tbody>
     <?php if (!empty($invoice_item)) { ?>
      <?php $index = 0; ?>
      <?php $temp = ''; ?>
      <?php foreach ($invoice_item as $value) { ?>
       <tr data_id="<?php echo $value['id'] ?>"> 
        <td>
         <?php echo $value['nama_product'] == $temp ? '' : $value['nama_product'] ?>
        </td>
        <td>
         <?php echo $value['satuan_str'] ?>
        </td>
        <td harga="">
         <?php echo $value['harga_str'] ?>
        </td>
        <td>
           <?php echo $value['qty_str'] ?>
        </td>
        <td><?php echo number_format($value['sub_total']) ?></td>
       </tr>
       <?php $index += 1; ?>
       <?php $temp = $value['nama_product']; ?>
      <?php } ?>
	  <?php } ?> 
	  
	  <?php if (!empty($biaya_item_invoice)) { ?>
      <?php foreach ($biaya_item_invoice as $value) { ?>
       <tr data_id="<?php echo $value['id'] ?>" class="biaya_invoice"> 
        <td>
         <?php echo $value['ket_biaya'] ?>
        </td>
        <td harga="" colspan="4">
         <?php echo number_format($value['jumlah']) ?>
        </td>
       </tr>
      <?php } ?>
	  <?php } ?> 
	  
	  <?php if(isset($id)){ ?>
		<?php if (!empty($biaya_item)) { ?>
      <?php foreach ($biaya_item as $value) { ?>
       <tr data_id="<?php echo $value['id'] ?>"> 
        <td>
         <?php echo $value['ket_biaya'] ?>
        </td>
        <td harga="" colspan="4">
         <?php echo number_format($value['jumlah']) ?>
        </td>
       </tr>
      <?php } ?>
	  <?php } ?> 
	  <?php }else{ ?>	 
		<tr data_id="">
          <td colspan="7">
           <label id="add_detail">
	    			<a href="#" onclick="SuratJalan.addBiayaLain(this, event)">Tambah Biaya Lain</a>
           </label>
          </td>
         </tr>
	  <?php } ?>	 
    </tbody>
   </table>
  </div>
 </div>
</div>
