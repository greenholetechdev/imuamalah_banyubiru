<?php

class Tenor_angsuran extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'tenor_angsuran';
 }

 public function getHeaderJSandCSS() {
  $data = array(
  '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
  '<script src="' . base_url() . 'assets/js/controllers/tenor_angsuran.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'ansuran';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Tenor Angsuran";
  $data['title_content'] = 'Data Tenor Angsuran';
  $content = $this->getDataTenorAngsuran();
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getTotalDataTenorAngsuran($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('ta.ansuran', $keyword),
   array('ta.periode_tahun', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
  'table' => $this->getTableName() . ' ta',
  'field' => array('ta.*'),
  'like' => $like,
  'is_or_like' => true,
  'where' => "ta.deleted is null or ta.deleted = 0"
  ));

  return $total;
 }

 public function getDataTenorAngsuran($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
   array('ta.ansuran', $keyword),
   array('ta.periode_tahun', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' ta',
  'field' => array('ta.*'),
  'like' => $like,
  'is_or_like' => true,
  'limit' => $this->limit,
  'offset' => $this->last_no,
  'where' => "ta.deleted is null or ta.deleted = 0"
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
  'data' => $result,
  'total_rows' => $this->getTotalDataTenorAngsuran($keyword)
  );
 }

 public function getDetailDataTenorAngsuran($id) {
  $data = Modules::run('database/get', array(
  'table' => $this->getTableName() . ' kr',
  'where' => "kr.id = '" . $id . "'"
  ));

  return $data->row_array();
 }

 public function add() {
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Tambah Tenor Angsuran";
  $data['title_content'] = 'Tambah Tenor Angsuran';
  echo Modules::run('template', $data);
 }

 public function ubah($id) {
  $data = $this->getDetailDataTenorAngsuran($id);
  $data['view_file'] = 'form_add_edit_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Ubah Tenor Angsuran";
  $data['title_content'] = 'Ubah Tenor Angsuran';
  echo Modules::run('template', $data);
 }

 public function detail($id) {
  $data = $this->getDetailDataTenorAngsuran($id);
  $data['view_file'] = 'detail_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Detail Tenor Angsuran";
  $data['title_content'] = 'Detail Tenor Angsuran';
  echo Modules::run('template', $data);
 }

 public function getPostDataHeader($value) {
  $data['ansuran'] = $value->ansuran;
  $data['periode_tahun'] = $value->periode_tahun;
  return $data;
 }

 public function simpan() {
  $data = json_decode($this->input->post('data'));
  $id = $this->input->post('id');
  $is_valid = false;
  $ansuran = $id;
  $this->db->trans_begin();
  try {
   $post_ansuran = $this->getPostDataHeader($data);
   if ($id == '') {
    $ansuran = Modules::run('database/_insert', $this->getTableName(), $post_ansuran);
   } else {
    //update
    Modules::run('database/_update', $this->getTableName(), $post_ansuran, array('id' => $id));
   }
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'ansuran' => $ansuran));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
  $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data TenorAngsuran";
  $data['title_content'] = 'Data TenorAngsuran';
  $content = $this->getDataTenorAngsuran($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

 public function getDataTenorAngsuranSatuanHarga($produk_satuan) {
  $data = Modules::run('database/get', array(
  'table' => 'produk_satuan_has_harga',
  'where' => array('produk_has_satuan' => $produk_satuan)
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    $value['active'] = "0";
    if ($value['period_end'] == '') {
     $value['active'] = "1";
     $value['period_end'] = '-';
    } else {
     $value['period_end'] = date('d F Y', strtotime($value['period_end']));
    }
    $value['period_start'] = date('d F Y', strtotime($value['period_start']));
    array_push($result, $value);
   }
  }


  return $result;
 }

 public function delete($id) {
  $is_valid = false;
  $this->db->trans_begin();
  try {
   Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
   $this->db->trans_commit();
   $is_valid = true;
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid));
 }

}
