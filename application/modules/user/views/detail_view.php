<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class='row'>
     <div class='col-md-12'>
      <u>Data Pegawai</u>
     </div>
    </div> 
    <hr/>
    <div class="row">
     <div class='col-md-3'>
      Nama Pegawai
     </div>
     <div class='col-md-3'>
      <?php echo $nama_pegawai ?>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3'>
      Hak Akses
     </div>
     <div class='col-md-3'>
      <?php echo $hak_akses ?>
     </div>     
    </div>
    <br/>
    
    <div class="row">
     <div class='col-md-3'>
      Username
     </div>
     <div class='col-md-3'>
      <?php echo $username ?>
     </div>     
    </div>
    <br/>

    <div class='row'>
     <div class='col-md-3'>
      Password
     </div>
     <div class='col-md-3'>
      <?php echo $password ?>
     </div>
    </div>
    <br/>
    
    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-danger-baru" onclick="User.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>
