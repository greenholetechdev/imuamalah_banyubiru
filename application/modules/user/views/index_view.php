<div class="content">
 <div class="animated fadeIn">
  <div class="card">
   <div class="card-header">
    <div class="row">
     <div class="col-md-10">
      <div class="box-card-title middle-left">
       <i class="mdi mdi-clipboard-plus mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
      </div>
     </div>
     <div class="col-sm-2 text-right"></div>
    </div>
   </div>
   <div class="card-body card-block">   
    <div class="row">
     <div class="col-md-3">
      <button class="btn btn-succes-baru" id="" onclick="User.add()">Tambah</button>
     </div>
     <div class="col-md-9">
      <div class="form-inside-icon icon-pos-right">
       <input type="text" id="keyword" class="form-control" placeholder="Pencarian" onkeyup="User.search(this, event)">
       <div class="form-icon">
        <i class="fa fa-search"></i>
       </div>
      </div>
     </div>
    </div>
    <br/>
    <div class='row'>
     <div class='col-md-12'>
      <?php if (isset($keyword)) { ?>
       <?php if ($keyword != '') { ?>
        Cari Data : "<b><?php echo $keyword; ?></b>"
       <?php } ?>
      <?php } ?>
     </div>
    </div>
    <br/>
    <div class="row">
     <div class="col-md-12">
      <table class="table table-striped table-bordered table-list-draft">
       <thead>
        <tr>
         <th>No</th>
         <th>Nama Pegawai</th>
         <th>Username</th>
         <th>Password</th>
         <th>Hak Akses</th>
         <th>Action</th>
        </tr>
       </thead>
       <tbody>
        <?php if (!empty($content)) { ?>
         <?php $no = 1; ?>
         <?php foreach ($content as $value) { ?>
          <tr>
           <td><?php echo $no++ ?></td>
           <td><?php echo $value['pegawai_name'] ?></td>
           <td><?php echo $value['username'] ?></td>
           <td><?php echo $value['password'] ?></td>
           <td><?php echo $value['hak_akses'] ?></td>
           <td class="text-center">
            <?php if ($value['is_active'] != '' && $value['username'] != $this->session->userdata('username')) { ?>
             <button id="" class="btn btn-danger-baru font12" 
                     onclick="User.setUserAktifNonAktif('<?php echo $value['id'] ?>', '<?php echo $value['is_active'] ?>')"><?php echo $value['is_active'] ? 'Non Aktifkan' : 'Aktifkan' ?></button>
                    <?php } ?>
            <button id="" class="btn btn-warning-baru font12" 
                    onclick="User.ubah('<?php echo $value['id'] ?>')">Ubah</button>
            <button id="" class="btn btn-succes-baru font12" 
                    onclick="User.detail('<?php echo $value['id'] ?>')">Detail</button>
           </td>
          </tr>
         <?php } ?>
        <?php } else { ?>
         <tr>
          <td colspan="6" class="text-center">Tidak ada data ditemukan</td>
         </tr>
        <?php } ?>

       </tbody>
      </table>
     </div>          
    </div> 
    <div class="row">
     <div class="col-md-12">
      <div class="pagination">
       <?php echo $pagination['links'] ?>
      </div>
     </div>
    </div>       
   </div>
  </div>
 </div>
</div>
