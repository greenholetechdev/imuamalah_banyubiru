var BankAkun = {
 module: function () {
  return 'bank_akun';
 },

 add: function () {
  window.location.href = url.base_url(BankAkun.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(BankAkun.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(BankAkun.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(BankAkun.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'nama_bank': $('#nama_bank').val(),
   'no_rekening': $('#no_rekening').val(),
   'akun': $('#akun').val(),
  };

  return data;
 },

 simpan: function (id) {
  var data = BankAkun.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(BankAkun.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(BankAkun.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(BankAkun.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(BankAkun.module()) + "detail/" + id;
 },

 setThousandSparator: function () {
  $('#jumlah').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(BankAkun.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(BankAkun.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },
 
 setSelect2: function(){
  $("#product").select2();
//  $('select#product').select2();
 }
};

$(function () {
 BankAkun.setSelect2();
 BankAkun.setThousandSparator();
});