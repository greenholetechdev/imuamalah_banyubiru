var Dashboard = {
 randomData: function () {
  barChartData.datasets.forEach(function (dataset) {
   dataset.data = dataset.data.map(function () {
    return randomScalingFactor();
   });
  });
  window.myBar.update();
 },

 getDataPembelian: function () {
  var data_pembelian = $('#data_penjualan').val();
  var pembelian = data_pembelian.toString().split(',');
  var data_all = [];
  for (var i = 0; i < 31; i++) {
   var value = parseFloat(pembelian[i]).toFixed(2);
   // var value = i * 30;
   data_all.push(value);
  }

  //  console.log(data_all);
  return data_all;
 },

 getDataKredit: function () {
  var data_pembelian = $('#data_kredit').val();
  var pembelian = data_pembelian.toString().split(',');
  var data_all = [];
  for (var i = 0; i < 12; i++) {
   var value = parseFloat(pembelian[i]).toFixed(2);
   // var value = i * 30;
   data_all.push(value);
  }

  //  console.log(data_all);
  return data_all;
 },

 getDataPembelianResep: function () {
  // var data_pembelian = $('#data_pembelian_resep').val();
  //  var pembelian = data_pembelian.toString().split(',');
  var data_all = [];
  for (var i = 0; i < 31; i++) {
   // var value = parseFloat(pembelian[i]).toFixed(2);
   var value = i * 30;
   data_all.push(value);
  }

  console.log(data_all);
  return data_all;
 },

 getDataLabelPembelian: function () {
  var data = [];

  for (i = 1; i < 32; i++) {
   var value = "" + i;
   data.push(value);
  }


  return data;
 },

 getDataLabelKredit: function () {
  var data = [];

  data.push("Januari");
  data.push("Februari");
  data.push("Maret");
  data.push("April");
  data.push("Mei");
  data.push("Juni");
  data.push("Juli");
  data.push("Agustus");
  data.push("September");
  data.push("Oktober");
  data.push("November");
  data.push("Desember");


  return data;
 },

 setGrafikPembelian: function () {
  var ctx = document.getElementById('canvas_pembelian').getContext('2d');
  var barChartData = {
   labels: Dashboard.getDataLabelPembelian(),
   datasets: [{
     label: 'Rumah',
     backgroundColor: window.chartColors.yellow,
     yAxisID: 'rumah',
     data: Dashboard.getDataPembelian()
    }]
  };

  window.myBar = new Chart(ctx, {
   type: 'bar',
   data: barChartData,
   options: {
    responsive: true,
    title: {
     display: true,
     text: 'Grafik Penjualan'
    },
    tooltips: {
     mode: 'index',
     //    intersect: true
    },
    scales: {
     yAxes: [{
       type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
       display: true,
       position: 'left',
       id: 'rumah',
       ticks: {
        stepSize: 100,
        min: 0,
        max: parseInt($('#total_data_penjualan').val())
       }
      }]
    }
   }
  });
 },

 setGrafikPembelianKredit: function () {
  var ctx = document.getElementById('canvas_kredit').getContext('2d');
  var barChartData = {
   labels: Dashboard.getDataLabelKredit(),
   datasets: [{
     label: 'Rumah',
     backgroundColor: window.chartColors.red,
     yAxisID: 'rumah',
     data: Dashboard.getDataKredit()
    }]
  };

  window.myBar = new Chart(ctx, {
   type: 'bar',
   data: barChartData,
   options: {
    responsive: true,
    title: {
     display: true,
     text: 'Grafik Kredit'
    },
    tooltips: {
     mode: 'index',
     //    intersect: true
    },
    scales: {
     yAxes: [{
       type: 'linear', // only linear but allow scale type registration. This allows extensions to exist solely for log scale for instance
       display: true,
       position: 'left',
       id: 'rumah',
       ticks: {
        stepSize: 100,
        min: 0,
        max: parseInt($('#total_data_kredit').val())
       }
      }]
    }
   }
  });
 },

 getDataGrafikPenjualan: function () {
  var data_pj = $('input#data-penjualan').val();
  var data_result = data_pj.toString().split(',');

//  console.log(data_result);
  var tahun = $('input#tahun').val();
  var data = [];
  for (let i = 0; i < data_result.length; i++) {
   var tgl = tahun.toString() + " Q1";
   data.push({y: '2011', item1: 1}, );
  }

  return data;
 },

 setGrafikPenjualan: function () {
  var data_pj = $('input#data-penjualan').val();
  var data_result = data_pj.toString().split(',');
  var tahun = $('input#tahun').val();

  var max_y = $('input#total-data-penjualan').val();

  var bar = new Morris.Bar({
   element: 'line-chart',
   resize: true,
   data: [
    {y: '2019 Jan', a: data_result[0]},
    {y: '2019 Feb', a: data_result[1]},
    {y: '2019 Mar', a: data_result[2]},
    {y: '2019 Apr', a: data_result[3]},
    {y: '2019 Mei', a: data_result[4]},
    {y: '2019 Jun', a: data_result[5]},
    {y: '2019 Jul', a: data_result[6]},
    {y: '2019 Ags', a: data_result[7]},
    {y: '2019 Sep', a: data_result[8]},
    {y: '2019 Okt', a: data_result[9]},
    {y: '2019 Nov', a: data_result[10]},
    {y: '2019 Des', a: data_result[11]}
   ],
   barColors: ['#49a65b'],
   xkey: 'y',
   ymax: max_y,
   ymin: 0,
   ykeys: ['a'],
   labels: ['CPU', 'DISK'],
   hideHover: 'auto'
  });
 },

 setGrafikKas: function () {

  var data_kredit = $('input#data-kas').attr('kredit');
  var data_debit = $('input#data-kas').attr('debit');
  var data_result_kredit = data_kredit.toString().split(',');
  var data_result_debit = data_debit.toString().split(',');
  var tahun = $('input#tahun').val();

  var bar = new Morris.Bar({
   element: 'bar-chart',
   resize: true,
   data: [
    {y: 'Jan', a: data_result_debit[0], b: data_result_kredit[0]},
    {y: 'Feb', a: data_result_debit[1], b: data_result_kredit[1]},
    {y: 'Mar', a: data_result_debit[2], b: data_result_kredit[2]},
    {y: 'Apr', a: data_result_debit[3], b: data_result_kredit[3]},
    {y: 'Mei', a: data_result_debit[4], b: data_result_kredit[4]},
    {y: 'Jun', a: data_result_debit[5], b: data_result_kredit[5]},
    {y: 'Jul', a: data_result_debit[6], b: data_result_kredit[6]},
    {y: 'Ags', a: data_result_debit[7], b: data_result_kredit[7]},
    {y: 'Sep', a: data_result_debit[8], b: data_result_kredit[8]},
    {y: 'Okt', a: data_result_debit[9], b: data_result_kredit[9]},
    {y: 'Nov', a: data_result_debit[9], b: data_result_kredit[10]},
    {y: 'Des', a: data_result_debit[9], b: data_result_kredit[11]},
   ],
   barColors: ['#49a65b', '#dd4b39'],
   xkey: 'y',
   ykeys: ['a', 'b'],
   labels: ['CPU', 'DISK'],
   hideHover: 'auto'
  });
 }
};


$(function () {
 Dashboard.setGrafikPenjualan();
 Dashboard.setGrafikKas();
//  Dashboard.setGrafikPembelian();
//  Dashboard.setGrafikPembelianKredit();
});
