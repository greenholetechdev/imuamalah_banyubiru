var FakturPelanggan = {
 module: function () {
  return 'faktur_pelanggan';
 },

 add: function () {
  window.location.href = url.base_url(FakturPelanggan.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(FakturPelanggan.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(FakturPelanggan.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(FakturPelanggan.module()) + "index";
   }
  }
 },

 getProductItem: function () {
  var tr_data = $('table#tb_product').find('tbody').find('tr');
  var data = [];
  $.each(tr_data, function () {
   var td = $(this).find('td');
   if (td.length > 4) {
    var product_satuan = $(this).find('td:eq(0)').find('select').val();
    var pajak = $(this).find('td:eq(4)').find('select').val();
    var metode = 1;
    var qty = $(this).find('td:eq(1)').find('input').val();
    var sub_total = $.trim($(this).find('td:eq(5)').text());
    var id = $(this).attr('data_id');

    var bank = "";
    if (metode == 2) {
     bank = $(this).closest('tr').next().attr('data_bank');
    }

    data.push({
     'id': id,
     'product_satuan': product_satuan,
     'pajak': pajak,
     'metode': metode,
     'qty': qty,
     'sub_total': sub_total,
     'bank': bank,
     'potongan_item': FakturPelanggan.getPostPotonganItem($(this)),
     'deleted': $(this).hasClass('deleted') ? '1' : '0'
    });
   }
  });

  return data;
 },

 getBiayaItem: function () {
  var tr_data = $('table#tb_product').find('tbody').find('tr');
  var data = [];
  $.each(tr_data, function () {
   var td = $(this).find('td');
   if (td.length > 3) {
    if ($(this).hasClass('biaya')) {
     var biaya = $(this).find('td:eq(1)').find('input').val();
     biaya = biaya == "" ? "0" : biaya;


     data.push({
      'biaya': biaya,
      'keterangan_biaya': $(this).find('td:eq(0)').find('input').val(),
      'deleted': $(this).hasClass('deleted') ? '1' : '0'
     });
    }
   }
  });

  return data;
 },

 getPostPotonganItem: function (elm) {
  var index = $(elm).index();
  var class_pot = "potongan-" + index;

  var data = [];
  $.each($('tr.' + class_pot), function () {
   data.push({
    'potongan': $(this).find('td:eq(0)').attr('data_id'),
    'nilai': $(this).find('label#jenis').attr('nilai'),
   });
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'pembeli': $('#pembeli').val(),
   'tanggal_faktur': $('#tanggal_faktur').val(),
   'tanggal_bayar': $('#tanggal_bayar').val(),
   'potongan': $('#potongan_invoice').val(),
   'pot_faktur': $('#nilai_potongan').val(),
   'jenis_bayar': $('#jenis_bayar').val(),
   'total': $('label#total').text(),
   'product_item': FakturPelanggan.getProductItem(),
   'biaya': FakturPelanggan.getBiayaItem()
  };

  return data;
 },

 simpan: function (id) {
  var data = FakturPelanggan.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(FakturPelanggan.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(FakturPelanggan.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(FakturPelanggan.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(FakturPelanggan.module()) + "detail/" + id;
 },

 setThousandSparator: function () {
  $('#jumlah').divide({
   delimiter: '.',
   divideThousand: true
  });

  $('#nilai_potongan').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(FakturPelanggan.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(FakturPelanggan.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 setSelect2: function () {
  $("#product").select2();
  $("#pembeli").select2();
  $("#metode").select2();
  $("#pajak").select2();

  var tr_product = $('table#tb_product').find('tbody').find('tr');
  $.each(tr_product, function () {
   var td = $(this).find('td');
   if (td.length > 1) {
    var data_id = $(this).attr('data_id');
    if (data_id != '') {
     var index = $(this).index();
     $(this).find('td:eq(0)').find('select').select2();
     $(this).find('td:eq(1)').find('select').select2();
     $(this).find('td:eq(2)').find('select').select2();
    }
   }
  });
 },

 setDate: function () {
  $('input#tanggal_faktur').datepicker({
   format: 'yyyy-mm-dd',
   todayHighlight: true,
   orientation: 'bottom left',
   autoclose: true
  });
  $('input#tanggal_bayar').datepicker({
   format: 'yyyy-mm-dd',
   todayHighlight: true,
   orientation: 'bottom left',
   autoclose: true
  });
 },

 addItem: function (elm, e) {
  e.preventDefault();
  var tr = $(elm).closest('tbody').find('tr:last');
  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    index: tr.index()
   },
   async: false,
   url: url.base_url(FakturPelanggan.module()) + "addItem",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    var tr = $(elm).closest('tbody').find('tr:last');
    var newTr = tr.clone();
    tr.html(resp);
    tr.after(newTr);
   }
  });
 },

 addBiayaLain: function (elm, e) {
  e.preventDefault();
  var tr = $(elm).closest('tbody').find('tr:last');
  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    index: tr.index()
   },
   async: false,
   url: url.base_url(FakturPelanggan.module()) + "addBiayaLain",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    var tr = $(elm).closest('tbody').find('tr:last');
    var newTr = tr.clone();
    tr.html(resp);
    tr.addClass('biaya');
    tr.after(newTr);
   }
  });
 },

 hitungSubTotal: function (elm, jenis) {
  var tr = $(elm).closest('tr');

  var produk = tr.find('td:eq(0)').find('select').val();
  var harga = tr.find('td:eq(0)').find('select').find("option[value='" + produk + "']").attr('harga');

  tr.find('td:eq(2)').find('label#harga_produk').text(harga);
  tr.find('td:eq(2)').find('label#harga_produk').divide({
   delimiter: '.',
   divideThousand: true
  });

  var potongan = 0;
  //  console.log(tr.find('td').length);
  //  if (tr.find('td').length > 3) {
  var class_potongan = "potongan-" + (tr.index());
  //   console.log("kelas_potongan", class_potongan);

  var option_product = tr.find('td:eq(0)').find('select').find('option');
  var harga = 0;
  $.each(option_product, function () {
   if ($(this).is(':selected')) {
    harga = $(this).attr('harga');
   }
  });
//  console.log("harga", harga);
  var option_pajak = tr.find('td:eq(4)').find('select').find('option');
  var persentase = 0;
  $.each(option_pajak, function () {
   if ($(this).is(':selected')) {
    persentase = $(this).attr('persentase');
   }
  });
  console.log("persentase", persentase);
  //  console.log(tr);
  //  return;
  var jumlah = parseFloat(tr.find('td:eq(1)').find('input').val());
  jumlah = isNaN(jumlah) ? 0 : jumlah;
  //  console.log(jumlah);
  //  return;
  console.log("jumlah", jumlah);
  var sub_total_content = tr.find('td:eq(5)');

  var potong_pajak = (jumlah * harga * persentase) / 100;
  var sub_total = (jumlah * harga) - potong_pajak;

  var tr_potongan = $('tr.' + class_potongan);
  //   console.log("kelas", tr_potongan);
  if (tr_potongan.length > 0) {
   $.each(tr_potongan, function () {
    var jenis = $(this).find('label#jenis').attr('jenis');
    if (jenis == "nominal") {
     sub_total -= parseInt($(this).find('label').attr('nilai'));
    } else {
     var nilai_pot = parseInt($(this).find('label').attr('nilai'));
     var nilai_percent = (nilai_pot * sub_total) / 100;
     sub_total -= nilai_percent;
    }
   });
  }

  console.log("subtotal", sub_total);
  if (tr.hasClass('biaya')) {
   var biaya = tr.find('td:eq(1)').find('input').val();
   biaya = biaya == "" ? 0 : parseInt(biaya);
   sub_total += biaya;
  }
  console.log("biaya", biaya);

  sub_total_content.find('label#sub_total').text(sub_total);
  //  }

  sub_total_content.find('label#sub_total').divide({
   delimiter: '.',
   divideThousand: true
  });
  FakturPelanggan.hitungTotal();
 },

 hitungTotal: function () {
  var tb_product = $('table#tb_product').find('tbody').find('tr');

  var potongan = 0;
  var jenis_potongan = $('#potongan_invoice').val();
  //  console.log(tb_product);
  var total = 0;
  $.each(tb_product, function () {
   var td = $(this).find('td');
   if (td.length > 3) {
    if (!$(this).hasClass('deleted')) {
     var sub_total = $(this).find('td:eq(5)').text().toString();
     if (!$(this).hasClass('biaya')) {
      sub_total = sub_total.replace(',', '');
      sub_total = sub_total.replace('.', '');
      sub_total = sub_total.replace('.', '');
      sub_total = sub_total.replace('.', '');
      sub_total = sub_total.replace('.', '');
      sub_total = parseInt(sub_total);
      total += sub_total;
     } else {
      var biaya = $(this).find('td:eq(1)').find('input').val();
      biaya = biaya == "" ? 0 : parseInt(biaya);
      total += biaya;
     }
    }
   }
  });

  if (jenis_potongan == "1") {
   var potongan_input = $('#nilai_potongan').val() == '' ? '0' : $('#nilai_potongan').val();
   potongan = parseInt(potongan_input);
   total -= potongan;
  } else {
   if (jenis_potongan == '2') {
    var nilai_pot = $('#nilai_potongan').val() == '' ? '0' : $('#nilai_potongan').val();

    nilai_pot = parseInt(nilai_pot);
    //    console.log(nilai_pot);
    var nilai_percent = (nilai_pot * total) / 100;
    //    console.log(nilai_percent);
    total -= nilai_percent;
   }
  }
  $('label#total').text(total);
  $('label#total').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 deleteItem: function (elm) {
  var data_id = $(elm).closest('tr').attr('data_id');
  if (data_id != '') {
   var tr = $('table#tb_product').find('tbody').find('tr[data_id="' + data_id + '"]');
   var metode = tr.find('td:eq(2)').find('select').val();
   tr.addClass('hide');
   tr.addClass('deleted');
   if (metode == 2) {
    tr.next().addClass('hide');
    tr.next().addClass('deleted');
   }
  } else {
   $(elm).closest('tr').remove();
  }

  FakturPelanggan.hitungTotal();
 },

 getMetodeBayar: function (elm) {
  var metode = $(elm).val();
  var index = $(elm).closest('tr').index();
  switch (metode) {
   case "2":
    $.ajax({
     type: 'POST',
     data: {
      metode: metode,
      index: index
     },
     dataType: 'html',
     async: false,
     url: url.base_url(FakturPelanggan.module()) + "getMetodeBayar",
     error: function () {
      toastr.error("Gagal");
     },

     beforeSend: function () {

     },

     success: function (resp) {
      bootbox.dialog({
       message: resp
      });
     }
    });
    break;

   default:

    break;
  }
 },

 pilihBank: function (elm) {
  message.closeDialog();
  var index_row = $(elm).attr('index_row');
  var tb_product = $('table#tb_product').find('tbody');
  var tr_product = tb_product.find('tr:eq(' + index_row + ')');

  var nama_bank = $(elm).closest('tr').find('td:eq(0)').text();
  var akun = $(elm).closest('tr').find('td:eq(2)').text();
  var no_rek = $(elm).closest('tr').find('td:eq(1)').text();
  var data_id_bank = $(elm).closest('tr').attr('data_id');
  var tr_bank = '<tr data_bank="' + data_id_bank + '">';
  tr_bank += '<td colspan="7">';
  tr_bank += '[' + nama_bank + '] - [' + akun + '] - [' + no_rek + ']';
  tr_bank += '</td>';
  tr_bank += '</tr>';
  tr_product.after(tr_bank);
 },

 cetak: function (id) {
  window.open(url.base_url(FakturPelanggan.module()) + "printFaktur/" + id);
 },

 bayar: function () {
  window.location.href = url.base_url("payment") + "add";
 },

 approvePrinted: function (id) {
  $.ajax({
   type: 'POST',
   data: {
    invoice: id
   },
   dataType: 'json',
   async: false,
   url: url.base_url(FakturPelanggan.module()) + "approvePrinted",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Approve Printed...");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Diapprove");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Diapprove");
    }
    message.closeLoading();
   }
  });
 },

 getPotongan: function (elm, e) {
  e.preventDefault();
  var tr = $(elm).closest('tr');

  var index = tr.index();
  //  console.log(index);
  //  return;
  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    index: index
   },
   async: false,
   url: url.base_url(FakturPelanggan.module()) + "getPotongan",
   error: function () {
    toastr.error("Gagal");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses("Proses Retrieve Potongan...");
   },

   success: function (resp) {
    message.closeLoading();

    bootbox.dialog({
     message: resp
    });
   }
  });
 },

 submitPot: function (elm, jenis) {
  var potongan = $('#potongan').val();
  var nilai_pot = $('#nilai_pot').val() == "" ? '0' : $('#nilai_pot').val();
  var table = $('#tb_product').find('tbody');
  var index_pot = $('input#index_pot').val();

  message.closeDialog();

  var tr = table.find('tr:eq(' + index_pot + ')');

  var is_has_potongan = $('tr.potongan-' + index_pot);

//  console.log('has potongan', is_has_potongan.length);
  if (validation.run()) {
   if (potongan == 1) {
    //nominal          
    var newTr = null;
    if (is_has_potongan.length > 0) {
     tr = $('tr.potongan-' + index_pot + ':last');
     newTr = $('tr.potongan-' + index_pot + ':last').clone();
//     console.log('newTr bro', newTr);
    } else {
     newTr = tr.clone();
     newTr.addClass('potongan-' + index_pot);
    }
    var td_html = "<td colspan='5' class='potongan' data_id='" + potongan + "'>";
    td_html += "<label id='jenis' jenis='nominal' nilai='" + nilai_pot + "'>Potongan Nominal : </label> <label id='nilai_pot_submit'>" + nilai_pot + '</label>';
    td_html += "</td>";
    td_html += "<td>";
    td_html += "</td>";
    td_html += "<td class='text-center'>";
    td_html += '<i class="mdi mdi-delete mdi-18px" onclick="FakturPelanggan.deleteItem(this)"></i>';
    td_html += "</td>";
    newTr.html(td_html);
    newTr.find('label#nilai_pot_submit').divide({
     delimiter: '.',
     divideThousand: true
    });

    tr.after(newTr);
   } else {
    //persentase

    var newTr = null;
    if (is_has_potongan.length > 0) {
     tr = $('tr.potongan-' + index_pot + ':last');
     newTr = $('tr.potongan-' + index_pot + ':last').clone();
//     console.log('newTr bro', newTr);
    } else {
     newTr = tr.clone();
     newTr.addClass('potongan-' + index_pot);
    }
    var td_html = "<td colspan='5' class='potongan' data_id='" + potongan + "'>";
    td_html += "<label id='jenis' jenis='persentase' nilai='" + nilai_pot + "'>Potongan Persentase (%) : </label> <label id='nilai_pot_submit'>" + nilai_pot + "</label>";
    td_html += "</td>";
    td_html += "<td>";
    td_html += "</td>";
    td_html += "<td class='text-center'>";
    td_html += '<i class="mdi mdi-delete mdi-18px" onclick="FakturPelanggan.deleteItem(this)"></i>';
    td_html += "</td>";
    newTr.html(td_html);

    newTr.find('label#nilai_pot_submit').divide({
     delimiter: '.',
     divideThousand: true
    });
    tr.after(newTr);
   }


   tr = table.find('tr:eq(' + index_pot + ')');
   FakturPelanggan.hitungSubTotal(tr, jenis);
  }
 },

 changeNilai: function (elm) {
  var jenis = $(elm).val();

  $('#nilai_pot').removeClass('text-right');
  switch (jenis) {
   case '1':
    $('#nilai_pot').addClass('text-right');
    break;
   default:
    $('#nilai_pot').removeClass('text-right');
    break;
  }
 }
};

$(function () {
 FakturPelanggan.setDate();
 FakturPelanggan.setSelect2();
 FakturPelanggan.setThousandSparator();
});
