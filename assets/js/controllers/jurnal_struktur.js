var JurnalStruktur = {
 module: function () {
  return 'jurnal_struktur';
 },

 add: function () {
  window.location.href = url.base_url(JurnalStruktur.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(JurnalStruktur.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(JurnalStruktur.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(JurnalStruktur.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'feature': $('#feature').val(),
   'coa': $('#coa').val(),
   'coa_type': $('#coa_type').val()
  };

  return data;
 },

 simpan: function (id) {
  var data = JurnalStruktur.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(JurnalStruktur.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(JurnalStruktur.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(JurnalStruktur.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(JurnalStruktur.module()) + "detail/" + id;
 },

 setThousandSparator: function () {
  $('#jumlah').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(JurnalStruktur.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(JurnalStruktur.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },
 
 setSelect2: function(){
  $('select#feature').select2();
  $('select#coa').select2();
  $('select#coa_type').select2();
 }
};

$(function () {
 JurnalStruktur.setThousandSparator();
 JurnalStruktur.setSelect2();
});