var Kasbon = {
 module: function () {
  return 'kasbon';
 },

 add: function () {
  window.location.href = url.base_url(Kasbon.module()) + "add";
 },
	
	bayar: function () {
  window.location.href = url.base_url("kasbon_payment") + "add";
 },

 back: function () {
  window.location.href = url.base_url(Kasbon.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Kasbon.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Kasbon.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'tanggal_faktur': $('#tanggal_faktur').val(),
   'tanggal_bayar': $('#tanggal_bayar').val(),
   'keterangan': $('#keterangan').val(),
   'total': $('#total').val(),
   'pegawai': $('#pegawai').val()
  };

  return data;
 },

 simpan: function (id) {
  var data = Kasbon.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Kasbon.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Kasbon.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Kasbon.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Kasbon.module()) + "detail/" + id;
 },

 setThousandSparator: function () {
  $('#total').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Kasbon.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Kasbon.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 setDate: function () {
  $('input#tanggal_faktur').datepicker({
   dateFormat: 'yy-mm-dd',
   todayHighlight: true,
  });
  $('input#tanggal_bayar').datepicker({
   dateFormat: 'yy-mm-dd',
   todayHighlight: true,
  });
 },
 
 setSelect2: function () {
  $("#pegawai").select2();
 },
};

$(function () {
 Kasbon.setThousandSparator();
 Kasbon.setDate();
 Kasbon.setSelect2();
});
