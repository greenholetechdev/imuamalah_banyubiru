var SuratJalan = {
	module: function () {
		return 'surat_jalan';
	},

	add: function () {
		window.location.href = url.base_url(SuratJalan.module()) + "add";
	},

	back: function () {
		window.location.href = url.base_url(SuratJalan.module()) + "index";
	},

	search: function (elm, e) {
		if (e.keyCode == 13) {
			var keyWord = $(elm).val();
			if (keyWord != '') {
				window.location.href = url.base_url(SuratJalan.module()) + "search" + '/' + keyWord;
			} else {
				window.location.href = url.base_url(SuratJalan.module()) + "index";
			}
		}
	},

	getPostData: function () {
		var data = {
			'id': $('#id').val(),
			'invoice': $('#invoice').val(),
			'tanggal': $('#tanggal').val(),
			'total': $('#total').text(),
			'biaya_item': SuratJalan.getBiayaItem()
		};

		return data;
	},

	getBiayaItem: function () {
		var tr_data = $('table#tb_product').find('tbody').find('tr');
		var data = [];
		$.each(tr_data, function () {
			var td = $(this).find('td');
			if (td.length > 3) {
				if ($(this).hasClass('biaya')) {
					var biaya = $(this).find('td:eq(1)').find('input').val();
					biaya = biaya == "" ? "0" : biaya;


					data.push({
						'biaya': biaya,
						'keterangan_biaya': $(this).find('td:eq(0)').find('input').val(),
						'deleted': $(this).hasClass('deleted') ? '1' : '0'
					});
				}
			}
		});

		return data;
	},

	simpan: function (id) {
		var data = SuratJalan.getPostData();
		var formData = new FormData();
		formData.append('data', JSON.stringify(data));
		formData.append("id", id);

		if (validation.run()) {
			$.ajax({
				type: 'POST',
				data: formData,
				dataType: 'json',
				processData: false,
				contentType: false,
				async: false,
				url: url.base_url(SuratJalan.module()) + "simpan",
				error: function () {
					toastr.error("Gagal");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Simpan...");
				},

				success: function (resp) {
					if (resp.is_valid) {
						toastr.success("Berhasil Disimpan");
						var reload = function () {
							window.location.href = url.base_url(SuratJalan.module()) + "detail" + '/' + resp.id;
						};

						setTimeout(reload(), 1000);
					} else {
						toastr.error("Gagal Disimpan");
					}
					message.closeLoading();
				}
			});
		}
	},

	ubah: function (id) {
		window.location.href = url.base_url(SuratJalan.module()) + "ubah/" + id;
	},

	detail: function (id) {
		window.location.href = url.base_url(SuratJalan.module()) + "detail/" + id;
	},

	setThousandSparator: function () {
		$('#jumlah').divide({
			delimiter: '.',
			divideThousand: true
		});
	},

	delete: function (id) {
		$.ajax({
			type: 'POST',
			dataType: 'json',
			async: false,
			url: url.base_url(SuratJalan.module()) + "delete/" + id,

			error: function () {
				toastr.error("Gagal Dihapus");
			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Dihapus");
					var reload = function () {
						window.location.href = url.base_url(SuratJalan.module()) + "index";
					};

					setTimeout(reload(), 1000);
				} else {
					toastr.error("Gagal Dihapus");
				}
			}
		});
	},

	setDate: function () {
		$('input#tanggal').datepicker({
			dateFormat: 'yy-mm-dd',
			todayHighlight: true,
		});
	},

	setSelect2: function () {
		$("#invoice").select2();
	},

	cetak: function (id) {
		window.open(url.base_url(SuratJalan.module()) + "printFaktur/" + id);
	},

	getFakturDetail: function (elm) {
		var faktur = $(elm).val();

		$.ajax({
			type: 'POST',
			data: {
				faktur: faktur
			},
			dataType: 'json',
			async: false,
			url: url.base_url(SuratJalan.module()) + "getFakturDetail",
			error: function () {
				toastr.error("Gagal");
				message.closeLoading();
			},

			beforeSend: function () {
				message.loadingProses("Proses Retrieving Data...");
			},

			success: function (resp) {
				message.closeLoading();
				$('div.form-item').html(resp.view_item);
				// $('label#total').text(resp.total);
				SuratJalan.hitungTotal();
			}
		});
	},

	addBiayaLain: function (elm, e) {
		e.preventDefault();
		var tr = $(elm).closest('tbody').find('tr:last');
		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: {
				index: tr.index()
			},
			async: false,
			url: url.base_url(SuratJalan.module()) + "addBiayaLain",
			error: function () {
				toastr.error("Gagal");
			},

			beforeSend: function () {

			},

			success: function (resp) {
				var tr = $(elm).closest('tbody').find('tr:last');
				var newTr = tr.clone();
				tr.html(resp);
				tr.addClass('biaya');
				tr.after(newTr);
			}
		});
	},

	deleteItem: function (elm) {
		$(elm).closest('tr').remove();
		SuratJalan.hitungTotal();
	},

	hitungTotal: function () {
		var tb_product = $('table#tb_product').find('tbody').find('tr');		 
		var total = 0;
		$.each(tb_product, function () {
			var td = $(this).find('td');
			if (td.length > 3) {
				if (!$(this).hasClass('deleted')) {
					if ($(this).hasClass('biaya')) {
						var biaya = $(this).find('td:eq(1)').find('input').val();
						biaya = biaya == "" ? 0 : parseInt(biaya);
						total += biaya;
					} else {
						if (!$(this).hasClass('biaya_invoice')) {
							var sub_total = $(this).find('td:eq(4)').text().toString();
							sub_total = sub_total.replace(',', '');
							sub_total = sub_total.replace(',', '');
							sub_total = sub_total.replace(',', '');
							sub_total = parseInt(sub_total);
							total += sub_total;
						}
					}
				}
			}else{
				if(td.length == 2){
					var biaya = $(this).find('td:eq(1)').text();
					console.log('biaya'+biaya);
					biaya = biaya.replace(',', '');
					biaya = biaya.replace(',', '');
					biaya = biaya.replace(',', '');
					biaya = biaya.replace(',', '');
					biaya = biaya == "" ? 0 : parseInt(biaya);
					total += biaya;
				}
			}
		});
		$('label#total').text(total);
		$('label#total').divide({
			delimiter: '.',
			divideThousand: true
		});
	},
};

$(function () {
	SuratJalan.setDate();
	SuratJalan.setSelect2();
	SuratJalan.setThousandSparator();
});
