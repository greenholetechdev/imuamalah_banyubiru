-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 16, 2019 at 06:21 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `imuamalah`
--

-- --------------------------------------------------------

--
-- Table structure for table `ansuran`
--

CREATE TABLE `ansuran` (
  `id` int(11) NOT NULL,
  `periode_tahun` varchar(50) DEFAULT NULL,
  `ansuran` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ansuran`
--

INSERT INTO `ansuran` (`id`, `periode_tahun`, `ansuran`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, '36', 36, '2019-10-09', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `akun` varchar(255) DEFAULT NULL,
  `nama_bank` varchar(255) DEFAULT NULL,
  `no_rekening` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `akun`, `nama_bank`, `no_rekening`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Tejo Kurniawan', 'Mandiri', '14132434', '2019-10-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `data_notifikasi_tagihan`
--

CREATE TABLE `data_notifikasi_tagihan` (
  `id` int(11) NOT NULL,
  `pembayaran_rumah` int(11) NOT NULL,
  `message` varchar(150) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `document_pengiriman`
--

CREATE TABLE `document_pengiriman` (
  `id` int(11) NOT NULL,
  `no_dokumen` varchar(155) DEFAULT NULL,
  `tgl_pengiriman` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dokumen_pengiriman_status`
--

CREATE TABLE `dokumen_pengiriman_status` (
  `id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'SENT\nBROKE\nRECEIVED',
  `document_pengiriman` int(11) NOT NULL,
  `keterangan` varchar(155) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `general`
--

CREATE TABLE `general` (
  `id` int(11) NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `indetitas`
--

CREATE TABLE `indetitas` (
  `id` int(11) NOT NULL,
  `identitas` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `investor`
--

CREATE TABLE `investor` (
  `id` int(11) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `no_hp` varchar(45) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `alamat` text,
  `keterangan` varchar(150) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `pembeli`, `no_faktur`, `tanggal_faktur`, `tanggal_bayar`, `total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'INV19OCT001', '2019-10-10', '2019-10-31', 127000, '2019-10-10', 1, '2019-10-11 03:34:08', 1, 0),
(2, 1, 'INV19OCT002', '2019-10-10', '2019-10-22', 47500, '2019-10-10', 1, NULL, NULL, 0),
(6, 1, 'INV19OCT003', '2019-10-10', '2019-10-29', 79500, '2019-10-10', 1, '2019-10-11 03:38:47', 1, 0),
(7, 1, 'INV19OCT004', '2019-10-14', '2019-10-15', 50000, '2019-10-14', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_print`
--

CREATE TABLE `invoice_print` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `cetak` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_print`
--

INSERT INTO `invoice_print` (`id`, `user`, `invoice`, `cetak`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 1, NULL, '2019-10-11', 1, NULL, NULL, 0),
(2, 1, 1, NULL, '2019-10-11', 1, NULL, NULL, 0),
(3, 1, 1, NULL, '2019-10-11', 1, NULL, NULL, 0),
(4, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(5, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(6, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(7, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(8, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(9, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(10, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(11, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(12, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(13, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(14, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(15, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(16, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(17, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(18, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(19, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(20, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(21, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(22, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(23, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(24, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(25, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(26, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(27, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(28, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(29, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0),
(30, 1, 2, NULL, '2019-10-11', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_product`
--

CREATE TABLE `invoice_product` (
  `id` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `metode_bayar` int(11) NOT NULL,
  `bank` int(11) DEFAULT '0',
  `pajak` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_product`
--

INSERT INTO `invoice_product` (`id`, `invoice`, `product_satuan`, `metode_bayar`, `bank`, `pajak`, `qty`, `sub_total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 1, 2, 1, 1, 4, 100000, '2019-10-10', 1, '2019-10-11 03:34:08', 1, 0),
(2, 1, 2, 1, NULL, 2, 6, 27000, '2019-10-10', 1, '2019-10-11 03:34:08', 1, 0),
(3, 2, 1, 2, 1, 2, 1, 22500, '2019-10-10', 1, NULL, NULL, 0),
(4, 2, 2, 1, NULL, 1, 5, 25000, '2019-10-10', 1, NULL, NULL, 0),
(9, 6, 1, 2, 1, 1, 2, 50000, '2019-10-10', 1, '2019-10-11 03:37:17', 1, 1),
(10, 6, 2, 1, 0, 2, 1, 4500, '2019-10-10', 1, '2019-10-11 03:38:47', 1, 0),
(11, 6, 1, 1, 0, 1, 3, 75000, '2019-10-11', 1, NULL, NULL, 0),
(12, 7, 1, 1, 0, 1, 2, 50000, '2019-10-14', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_status`
--

CREATE TABLE `invoice_status` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'PAID\nDRAFT\nVALID',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_status`
--

INSERT INTO `invoice_status` (`id`, `user`, `invoice`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 1, 'DRAFT', '2019-10-10', 1, NULL, NULL, 0),
(2, 1, 2, 'DRAFT', '2019-10-10', 1, NULL, NULL, 0),
(3, 1, 6, 'DRAFT', '2019-10-10', 1, NULL, NULL, 0),
(4, 1, 1, 'PAID', '2019-10-11', 1, NULL, NULL, 0),
(5, 1, 7, 'DRAFT', '2019-10-14', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_akad`
--

CREATE TABLE `jenis_akad` (
  `id` int(11) NOT NULL,
  `jenis` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_akad`
--

INSERT INTO `jenis_akad` (`id`, `jenis`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Jenis Akad 1', '2019-10-11', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_notif_pengiriman`
--

CREATE TABLE `jenis_notif_pengiriman` (
  `id` int(11) NOT NULL,
  `jenis` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jenis_pembayaran`
--

CREATE TABLE `jenis_pembayaran` (
  `id` int(11) NOT NULL,
  `jenis` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kas`
--

CREATE TABLE `kas` (
  `id` int(11) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori_akad`
--

CREATE TABLE `kategori_akad` (
  `id` int(11) NOT NULL,
  `kategori` varchar(100) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_akad`
--

INSERT INTO `kategori_akad` (`id`, `kategori`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Kategori Akad 1', '2019-10-11', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kategori_product`
--

CREATE TABLE `kategori_product` (
  `id` int(11) NOT NULL,
  `kategori` varchar(100) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_product`
--

INSERT INTO `kategori_product` (`id`, `kategori`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Kategori A', '2019-10-09', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kerja_sama_internal`
--

CREATE TABLE `kerja_sama_internal` (
  `id` int(11) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `presentase` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `metode_bayar`
--

CREATE TABLE `metode_bayar` (
  `id` int(11) NOT NULL,
  `metode` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `metode_bayar`
--

INSERT INTO `metode_bayar` (`id`, `metode`) VALUES
(1, 'CASH'),
(2, 'TRANSFER');

-- --------------------------------------------------------

--
-- Table structure for table `metode_pembayaran`
--

CREATE TABLE `metode_pembayaran` (
  `id` int(11) NOT NULL,
  `metode` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `metode_pembayaran`
--

INSERT INTO `metode_pembayaran` (`id`, `metode`) VALUES
(1, 'Tagihan'),
(2, 'Vendor'),
(3, 'Lain - lain');

-- --------------------------------------------------------

--
-- Table structure for table `notif_has_jenis_pengiriman`
--

CREATE TABLE `notif_has_jenis_pengiriman` (
  `id` int(11) NOT NULL,
  `notif_jatuh_tempo` int(11) NOT NULL,
  `jenis_notif_pengiriman` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notif_jatuh_tempo`
--

CREATE TABLE `notif_jatuh_tempo` (
  `id` int(11) NOT NULL,
  `nama_notif` varchar(150) DEFAULT NULL,
  `jadwal_notif` int(11) DEFAULT NULL COMMENT 'Jumlah Hari',
  `jenis_notif` varchar(1) DEFAULT NULL COMMENT '+ = Lebih\n- = Kurang',
  `status` int(11) DEFAULT '0' COMMENT '0 : Tidak Aktif\n1 : Aktif',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pajak`
--

CREATE TABLE `pajak` (
  `id` int(11) NOT NULL,
  `jenis` varchar(155) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `persentase` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pajak`
--

INSERT INTO `pajak` (`id`, `jenis`, `keterangan`, `persentase`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Bebas Pajak', 'Bebas tidak kena pajak', 0, NULL, NULL, NULL, NULL, 0),
(2, ' PPH 10 %', 'Kena Pajak 10 Persen', 10, '2019-10-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `partial_payment`
--

CREATE TABLE `partial_payment` (
  `id` int(11) NOT NULL,
  `no_faktur_bayar` varchar(150) DEFAULT NULL,
  `invoice` int(11) NOT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partial_payment`
--

INSERT INTO `partial_payment` (`id`, `no_faktur_bayar`, `invoice`, `tanggal_bayar`, `tanggal_faktur`, `jumlah`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 'PAYSB19OCT001', 2, '2019-10-11', '2019-10-11', 30000, '2019-10-11', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `partial_payment_status`
--

CREATE TABLE `partial_payment_status` (
  `id` int(11) NOT NULL,
  `partial_payment` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'LUNAS\nKREDIT',
  `sisa_hutang` int(11) DEFAULT '0',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `partial_payment_status`
--

INSERT INTO `partial_payment_status` (`id`, `partial_payment`, `status`, `sisa_hutang`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 2, 'PAID OFF', -17500, '2019-10-11', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `no_faktur_bayar` varchar(155) DEFAULT NULL,
  `invoice` int(11) NOT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `sisa` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `no_faktur_bayar`, `invoice`, `tanggal_bayar`, `tanggal_faktur`, `jumlah`, `sisa`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 'PAY19OCT001', 1, '2019-10-31', '2019-10-11', 150000, 23000, '2019-10-11', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payroll`
--

CREATE TABLE `payroll` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `periode` int(11) NOT NULL,
  `pegawai` int(11) NOT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `keterangan` text,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payroll`
--

INSERT INTO `payroll` (`id`, `no_faktur`, `periode`, `pegawai`, `tanggal_faktur`, `tanggal_bayar`, `keterangan`, `total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2, 'PAYSLIP19OCT001', 1, 1, '2019-10-13', '2019-10-25', '', 3250000, '2019-10-13', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payroll_category`
--

CREATE TABLE `payroll_category` (
  `id` int(11) NOT NULL,
  `jenis` varchar(45) DEFAULT NULL COMMENT 'POTONGAN\nTUNJANGAN\nDLL',
  `action` varchar(12) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payroll_category`
--

INSERT INTO `payroll_category` (`id`, `jenis`, `action`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Potongan BPJS', '-', NULL, NULL, '2019-10-13 05:29:56', 1, 0),
(2, 'Potongan Pajak', '-', '2019-10-13', 1, '2019-10-13 05:29:48', 1, 0),
(3, 'Gaji Pokok', '+', '2019-10-13', 1, '2019-10-13 05:28:33', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payroll_item`
--

CREATE TABLE `payroll_item` (
  `id` int(11) NOT NULL,
  `payroll` int(11) NOT NULL,
  `payroll_category` int(11) NOT NULL,
  `keterangan` text,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payroll_item`
--

INSERT INTO `payroll_item` (`id`, `payroll`, `payroll_category`, `keterangan`, `jumlah`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 2, 3, '', 3400000, '2019-10-13', 1, NULL, NULL, 0),
(2, 2, 2, '', 50000, '2019-10-13', 1, NULL, NULL, 0),
(3, 2, 1, '', 100000, '2019-10-13', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payroll_workdays`
--

CREATE TABLE `payroll_workdays` (
  `id` int(11) NOT NULL,
  `payroll` int(11) NOT NULL,
  `keterangan` text,
  `qty_hari` int(11) DEFAULT NULL,
  `qty_jam` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `no_hp` varchar(45) DEFAULT NULL,
  `email` text,
  `alamat` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `nama`, `no_hp`, `email`, `alamat`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Bejo Kurniawan', '6285738233712', 'bejo@gmail.com', 'Surabaya', '2019-10-13', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_has_angsuran`
--

CREATE TABLE `pembayaran_has_angsuran` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `pembayaran_product` int(11) NOT NULL,
  `total_bayar` int(11) DEFAULT NULL,
  `tgl_angsuran` date DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_has_cash`
--

CREATE TABLE `pembayaran_has_cash` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `pembayaran_product` int(11) NOT NULL,
  `tgl_bayar` date DEFAULT NULL,
  `total_bayar` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_has_product`
--

CREATE TABLE `pembayaran_has_product` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `pembayaran_product` int(11) NOT NULL,
  `total_bayar` int(11) DEFAULT NULL,
  `tgl_angsuran` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_lain_lain`
--

CREATE TABLE `pembayaran_lain_lain` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(45) DEFAULT NULL,
  `jenis_pembayaran` int(11) NOT NULL,
  `jenis` varchar(150) DEFAULT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `file` text,
  `tgl_bayar` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_product`
--

CREATE TABLE `pembayaran_product` (
  `id` int(11) NOT NULL,
  `pembeli_has_product` int(11) NOT NULL,
  `status_pembayaran` int(11) NOT NULL,
  `tgl_bayar` datetime DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_tagihan`
--

CREATE TABLE `pembayaran_tagihan` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `tagihan` int(11) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `jenis_pembayaran` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `file` text,
  `tgl_bayar` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_vendor`
--

CREATE TABLE `pembayaran_vendor` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `vendor` int(11) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `jenis_pembayaran` int(11) NOT NULL,
  `file` text,
  `tgl_bayar` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli`
--

CREATE TABLE `pembeli` (
  `id` int(11) NOT NULL,
  `pembeli_kategori` int(11) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `alamat` varchar(150) DEFAULT NULL,
  `email` text,
  `no_hp` varchar(45) DEFAULT NULL,
  `foto` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembeli`
--

INSERT INTO `pembeli` (`id`, `pembeli_kategori`, `nama`, `alamat`, `email`, `no_hp`, `foto`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 2, 'Dodik Rismawan', 'Surabaya', 'dodikitn@gmail.com', '6285738233712', NULL, '2019-10-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `jatuh_tempo` int(11) DEFAULT NULL COMMENT 'Hari',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`id`, `no_invoice`, `jatuh_tempo`, `createddate`, `createdby`, `updateddate`, `updatedby`) VALUES
(1, 'PEM19OCT001', 11, '2019-10-11', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_has_angsuran`
--

CREATE TABLE `pembeli_has_angsuran` (
  `id` int(11) NOT NULL,
  `pembeli_has_product` int(11) NOT NULL,
  `product_has_harga_angsuran` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_has_identitas`
--

CREATE TABLE `pembeli_has_identitas` (
  `id` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `indetitas` int(11) NOT NULL,
  `no_identitas` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_has_persyaratan`
--

CREATE TABLE `pembeli_has_persyaratan` (
  `id` int(11) NOT NULL,
  `pembelian` int(11) NOT NULL,
  `persyaratan_has_berkas` int(11) NOT NULL,
  `berkas` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembeli_has_persyaratan`
--

INSERT INTO `pembeli_has_persyaratan` (`id`, `pembelian`, `persyaratan_has_berkas`, `berkas`, `createddate`, `createdby`, `updateddate`, `updatedby`) VALUES
(1, 1, 1, NULL, '2019-10-11', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_has_product`
--

CREATE TABLE `pembeli_has_product` (
  `id` int(11) NOT NULL,
  `pembelian` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `pembeli` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `status_pembelian` int(11) NOT NULL,
  `tgl_beli` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembeli_has_product`
--

INSERT INTO `pembeli_has_product` (`id`, `pembelian`, `no_invoice`, `pembeli`, `product`, `status_pembelian`, `tgl_beli`, `createddate`, `createdby`, `updateddate`, `updatedby`) VALUES
(1, 1, 'INVPR19OCT001', 1, 1, 1, '2019-10-11', '2019-10-11', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_kategori`
--

CREATE TABLE `pembeli_kategori` (
  `id` int(11) NOT NULL,
  `kategori` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembeli_kategori`
--

INSERT INTO `pembeli_kategori` (`id`, `kategori`) VALUES
(1, 'KONSUMEN I'),
(2, 'KONSUMEN II');

-- --------------------------------------------------------

--
-- Table structure for table `periode`
--

CREATE TABLE `periode` (
  `id` int(11) NOT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updatedate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `periode`
--

INSERT INTO `periode` (`id`, `month`, `year`, `createddate`, `createdby`, `updatedate`, `updatedby`, `deleted`) VALUES
(1, 10, 2019, '2019-10-13', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `persyaratan`
--

CREATE TABLE `persyaratan` (
  `id` int(11) NOT NULL,
  `syarat` varchar(150) DEFAULT NULL,
  `kategori_akad` int(11) NOT NULL,
  `jenis_akad` int(11) NOT NULL,
  `lampirkan_berkas` int(11) DEFAULT NULL COMMENT '0 : Tidak\n1: YA',
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `persyaratan`
--

INSERT INTO `persyaratan` (`id`, `syarat`, `kategori_akad`, `jenis_akad`, `lampirkan_berkas`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Syarat 1', 1, 1, NULL, '-', '2019-10-11', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `persyaratan_has_berkas`
--

CREATE TABLE `persyaratan_has_berkas` (
  `id` int(11) NOT NULL,
  `persyaratan` int(11) NOT NULL,
  `nama_berkas` varchar(150) DEFAULT NULL COMMENT 'Nama Berkas Misal (KTP, SIM, dLL)',
  `status` varchar(150) DEFAULT NULL COMMENT 'Status Tanpa Upload Berkas atau Tidak\nN : Tanpa Berkas\nY : Dengan Berkas',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `persyaratan_has_berkas`
--

INSERT INTO `persyaratan_has_berkas` (`id`, `persyaratan`, `nama_berkas`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`) VALUES
(1, 1, 'Berkas A', 'N', '2019-10-11', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `priveledge`
--

CREATE TABLE `priveledge` (
  `id` int(11) NOT NULL,
  `hak_akses` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `priveledge`
--

INSERT INTO `priveledge` (`id`, `hak_akses`) VALUES
(1, 'superadmin'),
(2, 'company');

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product` varchar(150) DEFAULT NULL,
  `tipe_product` int(11) NOT NULL,
  `kategori_product` int(11) NOT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product`, `tipe_product`, `kategori_product`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Produk A', 1, 1, '-', '2019-10-09', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_distriburst`
--

CREATE TABLE `product_distriburst` (
  `id` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `tanggal_kirim` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_has_foto`
--

CREATE TABLE `product_has_foto` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `foto` longtext,
  `status` varchar(45) DEFAULT NULL COMMENT 'updated : File Diganti dengan file lain',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_has_harga_angsuran`
--

CREATE TABLE `product_has_harga_angsuran` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `ansuran` int(11) NOT NULL,
  `period_start` date DEFAULT NULL,
  `period_end` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `harga_total` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_has_harga_angsuran`
--

INSERT INTO `product_has_harga_angsuran` (`id`, `product`, `harga`, `ansuran`, `period_start`, `period_end`, `createddate`, `createdby`, `updateddate`, `updatedby`, `harga_total`) VALUES
(1, 1, 3000, 1, '2019-10-09', NULL, '2019-10-09', 1, NULL, NULL, 108000);

-- --------------------------------------------------------

--
-- Table structure for table `product_has_harga_jual_pokok`
--

CREATE TABLE `product_has_harga_jual_pokok` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `period_start` date DEFAULT NULL,
  `period_end` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_has_harga_jual_pokok`
--

INSERT INTO `product_has_harga_jual_pokok` (`id`, `product`, `harga`, `diskon`, `period_start`, `period_end`, `createddate`, `createdby`, `updateddate`, `updatedby`) VALUES
(1, 1, 1200, 0, '2019-10-09', NULL, '2019-10-09', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_has_harga_jual_tunai`
--

CREATE TABLE `product_has_harga_jual_tunai` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `period_start` date DEFAULT NULL,
  `period_end` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_has_harga_jual_tunai`
--

INSERT INTO `product_has_harga_jual_tunai` (`id`, `product`, `harga`, `diskon`, `period_start`, `period_end`, `createddate`, `createdby`, `updateddate`, `updatedby`) VALUES
(1, 1, 4000, 0, '2019-10-09', NULL, '2019-10-09', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_kirim`
--

CREATE TABLE `product_kirim` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `document_pengiriman` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_kirim_status`
--

CREATE TABLE `product_kirim_status` (
  `id` int(11) NOT NULL,
  `product_kirim` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'SENT\nRECEIVED\nBROKE',
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_log_stock`
--

CREATE TABLE `product_log_stock` (
  `id` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'OUT',
  `qty` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL COMMENT 'PEMBELIAN\nPENGIRIMAN',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_log_stock`
--

INSERT INTO `product_log_stock` (`id`, `product_satuan`, `status`, `qty`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'OUT', 2, 'Faktur Pelanggan', '2019-10-14', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_satuan`
--

CREATE TABLE `product_satuan` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `satuan` varchar(45) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_satuan`
--

INSERT INTO `product_satuan` (`id`, `product`, `satuan`, `harga`, `level`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 'Dus', 25000, 1, '2019-10-10', 1, NULL, NULL, 0),
(2, 1, 'Pack', 5000, 2, '2019-10-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_stock`
--

CREATE TABLE `product_stock` (
  `id` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `stock` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_stock`
--

INSERT INTO `product_stock` (`id`, `product_satuan`, `stock`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 12, '2019-10-09', 1, NULL, NULL, 0),
(2, 2, 25, '2019-10-09', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_surat_jalan`
--

CREATE TABLE `product_surat_jalan` (
  `id` int(11) NOT NULL,
  `no_sj` varchar(155) DEFAULT NULL,
  `document_pengiriman` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_vendor`
--

CREATE TABLE `product_vendor` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `vendor` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status_pembayaran`
--

CREATE TABLE `status_pembayaran` (
  `id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status_pembelian`
--

CREATE TABLE `status_pembelian` (
  `id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_pembelian`
--

INSERT INTO `status_pembelian` (`id`, `status`) VALUES
(1, 'CASH'),
(2, 'KREDIT');

-- --------------------------------------------------------

--
-- Table structure for table `tagihan`
--

CREATE TABLE `tagihan` (
  `id` int(11) NOT NULL,
  `tagihan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tipe_product`
--

CREATE TABLE `tipe_product` (
  `id` int(11) NOT NULL,
  `tipe` varchar(45) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipe_product`
--

INSERT INTO `tipe_product` (`id`, `tipe`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Tipe A', '2019-10-09', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` text,
  `priveledge` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `priveledge`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'administrator', '6d89013342e5133da1c45e095caa26a6', 1, NULL, NULL, '2019-10-14 15:28:49', 1, 0),
(2, 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', 1, '2019-10-14', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_sms_gateway`
--

CREATE TABLE `user_sms_gateway` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `url` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `id` int(11) NOT NULL,
  `nama_vendor` varchar(150) DEFAULT NULL,
  `vendor_category` int(11) NOT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor_category`
--

CREATE TABLE `vendor_category` (
  `id` int(11) NOT NULL,
  `kategori` varchar(45) DEFAULT NULL COMMENT 'COMPANY\nSUPPLIER'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `zakat_mal`
--

CREATE TABLE `zakat_mal` (
  `id` int(11) NOT NULL,
  `jumlah_laba` int(11) DEFAULT NULL,
  `periode` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ansuran`
--
ALTER TABLE `ansuran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_notifikasi_tagihan`
--
ALTER TABLE `data_notifikasi_tagihan`
  ADD PRIMARY KEY (`id`,`pembayaran_rumah`),
  ADD KEY `fk_data_notifikasi_tagihan_pembayaran_rumah1_idx` (`pembayaran_rumah`);

--
-- Indexes for table `document_pengiriman`
--
ALTER TABLE `document_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dokumen_pengiriman_status`
--
ALTER TABLE `dokumen_pengiriman_status`
  ADD PRIMARY KEY (`id`,`document_pengiriman`),
  ADD KEY `fk_dokumen_pengiriman_status_document_pengiriman1_idx` (`document_pengiriman`);

--
-- Indexes for table `general`
--
ALTER TABLE `general`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `indetitas`
--
ALTER TABLE `indetitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investor`
--
ALTER TABLE `investor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`,`pembeli`),
  ADD KEY `fk_invoice_pembeli1_idx` (`pembeli`);

--
-- Indexes for table `invoice_print`
--
ALTER TABLE `invoice_print`
  ADD PRIMARY KEY (`id`,`user`,`invoice`),
  ADD KEY `fk_invoice_print_invoice1_idx` (`invoice`),
  ADD KEY `fk_invoice_print_user1_idx` (`user`);

--
-- Indexes for table `invoice_product`
--
ALTER TABLE `invoice_product`
  ADD PRIMARY KEY (`id`,`invoice`,`product_satuan`,`metode_bayar`,`pajak`),
  ADD KEY `fk_invoice_product_invoice1_idx` (`invoice`),
  ADD KEY `fk_invoice_product_product_satuan1_idx` (`product_satuan`),
  ADD KEY `fk_invoice_product_pajak1_idx` (`pajak`),
  ADD KEY `fk_invoice_product_metode_bayar1_idx` (`metode_bayar`);

--
-- Indexes for table `invoice_status`
--
ALTER TABLE `invoice_status`
  ADD PRIMARY KEY (`id`,`user`,`invoice`),
  ADD KEY `fk_invoice_status_invoice1_idx` (`invoice`),
  ADD KEY `fk_invoice_status_user1_idx` (`user`);

--
-- Indexes for table `jenis_akad`
--
ALTER TABLE `jenis_akad`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_notif_pengiriman`
--
ALTER TABLE `jenis_notif_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_pembayaran`
--
ALTER TABLE `jenis_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kas`
--
ALTER TABLE `kas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_akad`
--
ALTER TABLE `kategori_akad`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_product`
--
ALTER TABLE `kategori_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kerja_sama_internal`
--
ALTER TABLE `kerja_sama_internal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metode_bayar`
--
ALTER TABLE `metode_bayar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metode_pembayaran`
--
ALTER TABLE `metode_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notif_has_jenis_pengiriman`
--
ALTER TABLE `notif_has_jenis_pengiriman`
  ADD PRIMARY KEY (`id`,`notif_jatuh_tempo`,`jenis_notif_pengiriman`),
  ADD KEY `fk_notif_has_jenis_pengiriman_notif_jatuh_tempo1_idx` (`notif_jatuh_tempo`),
  ADD KEY `fk_notif_has_jenis_pengiriman_jenis_notif_pengiriman1_idx` (`jenis_notif_pengiriman`);

--
-- Indexes for table `notif_jatuh_tempo`
--
ALTER TABLE `notif_jatuh_tempo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pajak`
--
ALTER TABLE `pajak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partial_payment`
--
ALTER TABLE `partial_payment`
  ADD PRIMARY KEY (`id`,`invoice`),
  ADD KEY `fk_partial_payment_invoice1_idx` (`invoice`);

--
-- Indexes for table `partial_payment_status`
--
ALTER TABLE `partial_payment_status`
  ADD PRIMARY KEY (`id`,`partial_payment`),
  ADD KEY `fk_partial_payment_status_partial_payment1_idx` (`partial_payment`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`,`invoice`),
  ADD KEY `fk_payment_invoice1_idx` (`invoice`);

--
-- Indexes for table `payroll`
--
ALTER TABLE `payroll`
  ADD PRIMARY KEY (`id`,`periode`,`pegawai`),
  ADD KEY `fk_payroll_pegawai1_idx` (`pegawai`),
  ADD KEY `fk_payroll_periode1_idx` (`periode`);

--
-- Indexes for table `payroll_category`
--
ALTER TABLE `payroll_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payroll_item`
--
ALTER TABLE `payroll_item`
  ADD PRIMARY KEY (`id`,`payroll`,`payroll_category`),
  ADD KEY `fk_payroll_item_payroll_category1_idx` (`payroll_category`),
  ADD KEY `fk_payroll_item_payroll1_idx` (`payroll`);

--
-- Indexes for table `payroll_workdays`
--
ALTER TABLE `payroll_workdays`
  ADD PRIMARY KEY (`id`,`payroll`),
  ADD KEY `fk_payroll_workdays_payroll1_idx` (`payroll`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembayaran_has_angsuran`
--
ALTER TABLE `pembayaran_has_angsuran`
  ADD PRIMARY KEY (`id`,`pembayaran_product`),
  ADD KEY `fk_pembayaran_has_angsuran_pembayaran_product1_idx` (`pembayaran_product`);

--
-- Indexes for table `pembayaran_has_cash`
--
ALTER TABLE `pembayaran_has_cash`
  ADD PRIMARY KEY (`id`,`pembayaran_product`),
  ADD KEY `fk_pembayaran_has_cash_pembayaran_rumah1_idx` (`pembayaran_product`);

--
-- Indexes for table `pembayaran_has_product`
--
ALTER TABLE `pembayaran_has_product`
  ADD PRIMARY KEY (`id`,`pembayaran_product`),
  ADD KEY `fk_pembayaran_has_angsuran_pembayaran1_idx` (`pembayaran_product`);

--
-- Indexes for table `pembayaran_lain_lain`
--
ALTER TABLE `pembayaran_lain_lain`
  ADD PRIMARY KEY (`id`,`jenis_pembayaran`),
  ADD KEY `fk_pembayaran_lain_lain_jenis_pembayaran1_idx` (`jenis_pembayaran`);

--
-- Indexes for table `pembayaran_product`
--
ALTER TABLE `pembayaran_product`
  ADD PRIMARY KEY (`id`,`pembeli_has_product`,`status_pembayaran`),
  ADD KEY `fk_pembayaran_pembeli_has_rumah1_idx` (`pembeli_has_product`),
  ADD KEY `fk_pembayaran_status_pembayaran1_idx` (`status_pembayaran`);

--
-- Indexes for table `pembayaran_tagihan`
--
ALTER TABLE `pembayaran_tagihan`
  ADD PRIMARY KEY (`id`,`tagihan`,`jenis_pembayaran`),
  ADD KEY `fk_pembayaran_tagihan_tagihan1_idx` (`tagihan`),
  ADD KEY `fk_pembayaran_tagihan_jenis_pembayaran1_idx` (`jenis_pembayaran`);

--
-- Indexes for table `pembayaran_vendor`
--
ALTER TABLE `pembayaran_vendor`
  ADD PRIMARY KEY (`id`,`vendor`,`jenis_pembayaran`),
  ADD KEY `fk_pembayaran_vendor_vendor1_idx` (`vendor`),
  ADD KEY `fk_pembayaran_vendor_jenis_pembayaran1_idx` (`jenis_pembayaran`);

--
-- Indexes for table `pembeli`
--
ALTER TABLE `pembeli`
  ADD PRIMARY KEY (`id`,`pembeli_kategori`),
  ADD KEY `fk_pembeli_pembeli_kategori1_idx` (`pembeli_kategori`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembeli_has_angsuran`
--
ALTER TABLE `pembeli_has_angsuran`
  ADD PRIMARY KEY (`id`,`pembeli_has_product`,`product_has_harga_angsuran`),
  ADD KEY `fk_pembeli_has_angsuran_pembeli_has_rumah1_idx` (`pembeli_has_product`),
  ADD KEY `fk_pembeli_has_angsuran_rumah_has_harga_angsuran1_idx` (`product_has_harga_angsuran`);

--
-- Indexes for table `pembeli_has_identitas`
--
ALTER TABLE `pembeli_has_identitas`
  ADD PRIMARY KEY (`id`,`pembeli`,`indetitas`),
  ADD KEY `fk_kreditur_has_identitas_kreditur1_idx` (`pembeli`),
  ADD KEY `fk_kreditur_has_identitas_indetitas1_idx` (`indetitas`);

--
-- Indexes for table `pembeli_has_persyaratan`
--
ALTER TABLE `pembeli_has_persyaratan`
  ADD PRIMARY KEY (`id`,`pembelian`,`persyaratan_has_berkas`),
  ADD KEY `fk_pembeli_has_persyaratan_persyaratan_has_berkas1_idx` (`persyaratan_has_berkas`),
  ADD KEY `fk_pembeli_has_persyaratan_pembelian1_idx` (`pembelian`);

--
-- Indexes for table `pembeli_has_product`
--
ALTER TABLE `pembeli_has_product`
  ADD PRIMARY KEY (`id`,`pembelian`,`pembeli`,`product`,`status_pembelian`),
  ADD KEY `fk_kreditur_has_rumah_kreditur1_idx` (`pembeli`),
  ADD KEY `fk_kreditur_has_rumah_rumah1_idx` (`product`),
  ADD KEY `fk_pembeli_has_rumah_status_pembelian1_idx` (`status_pembelian`),
  ADD KEY `fk_pembeli_has_rumah_pembelian1_idx` (`pembelian`);

--
-- Indexes for table `pembeli_kategori`
--
ALTER TABLE `pembeli_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `persyaratan`
--
ALTER TABLE `persyaratan`
  ADD PRIMARY KEY (`id`,`kategori_akad`,`jenis_akad`),
  ADD KEY `fk_persyaratan_kategori_akad1_idx` (`kategori_akad`),
  ADD KEY `fk_persyaratan_jenis_akad1_idx` (`jenis_akad`);

--
-- Indexes for table `persyaratan_has_berkas`
--
ALTER TABLE `persyaratan_has_berkas`
  ADD PRIMARY KEY (`id`,`persyaratan`),
  ADD KEY `fk_persyaratan_has_berkas_persyaratan1_idx` (`persyaratan`);

--
-- Indexes for table `priveledge`
--
ALTER TABLE `priveledge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`,`tipe_product`,`kategori_product`),
  ADD KEY `fk_rumah_kategori_rumah1_idx` (`kategori_product`),
  ADD KEY `fk_rumah_tipe_rumah1_idx` (`tipe_product`);

--
-- Indexes for table `product_distriburst`
--
ALTER TABLE `product_distriburst`
  ADD PRIMARY KEY (`id`,`pembeli`,`product`),
  ADD KEY `fk_product_distriburst_product1_idx` (`product`),
  ADD KEY `fk_product_distriburst_pembeli1_idx` (`pembeli`);

--
-- Indexes for table `product_has_foto`
--
ALTER TABLE `product_has_foto`
  ADD PRIMARY KEY (`id`,`product`),
  ADD KEY `fk_rumah_has_foto_rumah1_idx` (`product`);

--
-- Indexes for table `product_has_harga_angsuran`
--
ALTER TABLE `product_has_harga_angsuran`
  ADD PRIMARY KEY (`id`,`product`,`ansuran`),
  ADD KEY `fk_rumah_has_harga_angsuran_rumah1_idx` (`product`),
  ADD KEY `fk_rumah_has_harga_angsuran_ansuran1_idx` (`ansuran`);

--
-- Indexes for table `product_has_harga_jual_pokok`
--
ALTER TABLE `product_has_harga_jual_pokok`
  ADD PRIMARY KEY (`id`,`product`),
  ADD KEY `fk_rumah_has_harga_jual_rumah1_idx` (`product`);

--
-- Indexes for table `product_has_harga_jual_tunai`
--
ALTER TABLE `product_has_harga_jual_tunai`
  ADD PRIMARY KEY (`id`,`product`),
  ADD KEY `fk_rumah_has_harga_jual_kredit_rumah1_idx` (`product`);

--
-- Indexes for table `product_kirim`
--
ALTER TABLE `product_kirim`
  ADD PRIMARY KEY (`id`,`product`,`document_pengiriman`),
  ADD KEY `fk_product_kirim_document_pengiriman1_idx` (`document_pengiriman`),
  ADD KEY `fk_product_kirim_product1_idx` (`product`);

--
-- Indexes for table `product_kirim_status`
--
ALTER TABLE `product_kirim_status`
  ADD PRIMARY KEY (`id`,`product_kirim`),
  ADD KEY `fk_product_kirim_status_product_kirim1_idx` (`product_kirim`);

--
-- Indexes for table `product_log_stock`
--
ALTER TABLE `product_log_stock`
  ADD PRIMARY KEY (`id`,`product_satuan`),
  ADD KEY `fk_product_log_stock_product_satuan1_idx` (`product_satuan`);

--
-- Indexes for table `product_satuan`
--
ALTER TABLE `product_satuan`
  ADD PRIMARY KEY (`id`,`product`),
  ADD KEY `fk_product_satuan_product1_idx` (`product`);

--
-- Indexes for table `product_stock`
--
ALTER TABLE `product_stock`
  ADD PRIMARY KEY (`id`,`product_satuan`),
  ADD KEY `fk_product_stock_product_satuan1_idx` (`product_satuan`);

--
-- Indexes for table `product_surat_jalan`
--
ALTER TABLE `product_surat_jalan`
  ADD PRIMARY KEY (`id`,`document_pengiriman`),
  ADD KEY `fk_product_surat_jalan_document_pengiriman1_idx` (`document_pengiriman`);

--
-- Indexes for table `product_vendor`
--
ALTER TABLE `product_vendor`
  ADD PRIMARY KEY (`id`,`product`,`vendor`),
  ADD KEY `fk_product_vendor_product1_idx` (`product`),
  ADD KEY `fk_product_vendor_vendor1_idx` (`vendor`);

--
-- Indexes for table `status_pembayaran`
--
ALTER TABLE `status_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_pembelian`
--
ALTER TABLE `status_pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tagihan`
--
ALTER TABLE `tagihan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipe_product`
--
ALTER TABLE `tipe_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`,`priveledge`),
  ADD KEY `fk_user_priveledge_idx` (`priveledge`);

--
-- Indexes for table `user_sms_gateway`
--
ALTER TABLE `user_sms_gateway`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`id`,`vendor_category`),
  ADD KEY `fk_vendor_vendor_category1_idx` (`vendor_category`);

--
-- Indexes for table `vendor_category`
--
ALTER TABLE `vendor_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `zakat_mal`
--
ALTER TABLE `zakat_mal`
  ADD PRIMARY KEY (`id`,`periode`),
  ADD KEY `fk_zakat_mal_periode1_idx` (`periode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ansuran`
--
ALTER TABLE `ansuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `data_notifikasi_tagihan`
--
ALTER TABLE `data_notifikasi_tagihan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `document_pengiriman`
--
ALTER TABLE `document_pengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dokumen_pengiriman_status`
--
ALTER TABLE `dokumen_pengiriman_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `general`
--
ALTER TABLE `general`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `indetitas`
--
ALTER TABLE `indetitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `investor`
--
ALTER TABLE `investor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `invoice_print`
--
ALTER TABLE `invoice_print`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `invoice_product`
--
ALTER TABLE `invoice_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `invoice_status`
--
ALTER TABLE `invoice_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `jenis_akad`
--
ALTER TABLE `jenis_akad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jenis_notif_pengiriman`
--
ALTER TABLE `jenis_notif_pengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jenis_pembayaran`
--
ALTER TABLE `jenis_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kas`
--
ALTER TABLE `kas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kategori_akad`
--
ALTER TABLE `kategori_akad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kategori_product`
--
ALTER TABLE `kategori_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kerja_sama_internal`
--
ALTER TABLE `kerja_sama_internal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `metode_bayar`
--
ALTER TABLE `metode_bayar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `metode_pembayaran`
--
ALTER TABLE `metode_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `notif_has_jenis_pengiriman`
--
ALTER TABLE `notif_has_jenis_pengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notif_jatuh_tempo`
--
ALTER TABLE `notif_jatuh_tempo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pajak`
--
ALTER TABLE `pajak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `partial_payment`
--
ALTER TABLE `partial_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `partial_payment_status`
--
ALTER TABLE `partial_payment_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payroll`
--
ALTER TABLE `payroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payroll_category`
--
ALTER TABLE `payroll_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payroll_item`
--
ALTER TABLE `payroll_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payroll_workdays`
--
ALTER TABLE `payroll_workdays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pembayaran_has_angsuran`
--
ALTER TABLE `pembayaran_has_angsuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_has_cash`
--
ALTER TABLE `pembayaran_has_cash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_has_product`
--
ALTER TABLE `pembayaran_has_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_lain_lain`
--
ALTER TABLE `pembayaran_lain_lain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_product`
--
ALTER TABLE `pembayaran_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_tagihan`
--
ALTER TABLE `pembayaran_tagihan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_vendor`
--
ALTER TABLE `pembayaran_vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembeli`
--
ALTER TABLE `pembeli`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pembeli_has_angsuran`
--
ALTER TABLE `pembeli_has_angsuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembeli_has_identitas`
--
ALTER TABLE `pembeli_has_identitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembeli_has_persyaratan`
--
ALTER TABLE `pembeli_has_persyaratan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pembeli_has_product`
--
ALTER TABLE `pembeli_has_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pembeli_kategori`
--
ALTER TABLE `pembeli_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `periode`
--
ALTER TABLE `periode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `persyaratan`
--
ALTER TABLE `persyaratan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `persyaratan_has_berkas`
--
ALTER TABLE `persyaratan_has_berkas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `priveledge`
--
ALTER TABLE `priveledge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_distriburst`
--
ALTER TABLE `product_distriburst`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_has_foto`
--
ALTER TABLE `product_has_foto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_has_harga_angsuran`
--
ALTER TABLE `product_has_harga_angsuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_has_harga_jual_pokok`
--
ALTER TABLE `product_has_harga_jual_pokok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_has_harga_jual_tunai`
--
ALTER TABLE `product_has_harga_jual_tunai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_kirim`
--
ALTER TABLE `product_kirim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_kirim_status`
--
ALTER TABLE `product_kirim_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_log_stock`
--
ALTER TABLE `product_log_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `product_satuan`
--
ALTER TABLE `product_satuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_stock`
--
ALTER TABLE `product_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_surat_jalan`
--
ALTER TABLE `product_surat_jalan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_vendor`
--
ALTER TABLE `product_vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status_pembayaran`
--
ALTER TABLE `status_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status_pembelian`
--
ALTER TABLE `status_pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tagihan`
--
ALTER TABLE `tagihan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tipe_product`
--
ALTER TABLE `tipe_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_sms_gateway`
--
ALTER TABLE `user_sms_gateway`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendor_category`
--
ALTER TABLE `vendor_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `zakat_mal`
--
ALTER TABLE `zakat_mal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `data_notifikasi_tagihan`
--
ALTER TABLE `data_notifikasi_tagihan`
  ADD CONSTRAINT `fk_data_notifikasi_tagihan_pembayaran_rumah1` FOREIGN KEY (`pembayaran_rumah`) REFERENCES `pembayaran_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `dokumen_pengiriman_status`
--
ALTER TABLE `dokumen_pengiriman_status`
  ADD CONSTRAINT `fk_dokumen_pengiriman_status_document_pengiriman1` FOREIGN KEY (`document_pengiriman`) REFERENCES `document_pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `fk_invoice_pembeli1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_print`
--
ALTER TABLE `invoice_print`
  ADD CONSTRAINT `fk_invoice_print_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_print_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_product`
--
ALTER TABLE `invoice_product`
  ADD CONSTRAINT `fk_invoice_product_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_product_metode_bayar1` FOREIGN KEY (`metode_bayar`) REFERENCES `metode_bayar` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_product_pajak1` FOREIGN KEY (`pajak`) REFERENCES `pajak` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_product_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_status`
--
ALTER TABLE `invoice_status`
  ADD CONSTRAINT `fk_invoice_status_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_status_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `notif_has_jenis_pengiriman`
--
ALTER TABLE `notif_has_jenis_pengiriman`
  ADD CONSTRAINT `fk_notif_has_jenis_pengiriman_jenis_notif_pengiriman1` FOREIGN KEY (`jenis_notif_pengiriman`) REFERENCES `jenis_notif_pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_notif_has_jenis_pengiriman_notif_jatuh_tempo1` FOREIGN KEY (`notif_jatuh_tempo`) REFERENCES `notif_jatuh_tempo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `partial_payment`
--
ALTER TABLE `partial_payment`
  ADD CONSTRAINT `fk_partial_payment_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `partial_payment_status`
--
ALTER TABLE `partial_payment_status`
  ADD CONSTRAINT `fk_partial_payment_status_partial_payment1` FOREIGN KEY (`partial_payment`) REFERENCES `partial_payment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `fk_payment_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payroll`
--
ALTER TABLE `payroll`
  ADD CONSTRAINT `fk_payroll_pegawai1` FOREIGN KEY (`pegawai`) REFERENCES `pegawai` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_payroll_periode1` FOREIGN KEY (`periode`) REFERENCES `periode` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payroll_item`
--
ALTER TABLE `payroll_item`
  ADD CONSTRAINT `fk_payroll_item_payroll1` FOREIGN KEY (`payroll`) REFERENCES `payroll` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_payroll_item_payroll_category1` FOREIGN KEY (`payroll_category`) REFERENCES `payroll_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payroll_workdays`
--
ALTER TABLE `payroll_workdays`
  ADD CONSTRAINT `fk_payroll_workdays_payroll1` FOREIGN KEY (`payroll`) REFERENCES `payroll` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_has_angsuran`
--
ALTER TABLE `pembayaran_has_angsuran`
  ADD CONSTRAINT `fk_pembayaran_has_angsuran_pembayaran_product1` FOREIGN KEY (`pembayaran_product`) REFERENCES `pembayaran_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_has_cash`
--
ALTER TABLE `pembayaran_has_cash`
  ADD CONSTRAINT `fk_pembayaran_has_cash_pembayaran_rumah1` FOREIGN KEY (`pembayaran_product`) REFERENCES `pembayaran_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_has_product`
--
ALTER TABLE `pembayaran_has_product`
  ADD CONSTRAINT `fk_pembayaran_has_angsuran_pembayaran1` FOREIGN KEY (`pembayaran_product`) REFERENCES `pembayaran_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_lain_lain`
--
ALTER TABLE `pembayaran_lain_lain`
  ADD CONSTRAINT `fk_pembayaran_lain_lain_jenis_pembayaran1` FOREIGN KEY (`jenis_pembayaran`) REFERENCES `jenis_pembayaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_product`
--
ALTER TABLE `pembayaran_product`
  ADD CONSTRAINT `fk_pembayaran_pembeli_has_rumah1` FOREIGN KEY (`pembeli_has_product`) REFERENCES `pembeli_has_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembayaran_status_pembayaran1` FOREIGN KEY (`status_pembayaran`) REFERENCES `status_pembayaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_tagihan`
--
ALTER TABLE `pembayaran_tagihan`
  ADD CONSTRAINT `fk_pembayaran_tagihan_jenis_pembayaran1` FOREIGN KEY (`jenis_pembayaran`) REFERENCES `jenis_pembayaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembayaran_tagihan_tagihan1` FOREIGN KEY (`tagihan`) REFERENCES `tagihan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_vendor`
--
ALTER TABLE `pembayaran_vendor`
  ADD CONSTRAINT `fk_pembayaran_vendor_jenis_pembayaran1` FOREIGN KEY (`jenis_pembayaran`) REFERENCES `jenis_pembayaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembayaran_vendor_vendor1` FOREIGN KEY (`vendor`) REFERENCES `vendor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli`
--
ALTER TABLE `pembeli`
  ADD CONSTRAINT `fk_pembeli_pembeli_kategori1` FOREIGN KEY (`pembeli_kategori`) REFERENCES `pembeli_kategori` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli_has_angsuran`
--
ALTER TABLE `pembeli_has_angsuran`
  ADD CONSTRAINT `fk_pembeli_has_angsuran_pembeli_has_rumah1` FOREIGN KEY (`pembeli_has_product`) REFERENCES `pembeli_has_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembeli_has_angsuran_rumah_has_harga_angsuran1` FOREIGN KEY (`product_has_harga_angsuran`) REFERENCES `product_has_harga_angsuran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli_has_identitas`
--
ALTER TABLE `pembeli_has_identitas`
  ADD CONSTRAINT `fk_kreditur_has_identitas_indetitas1` FOREIGN KEY (`indetitas`) REFERENCES `indetitas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kreditur_has_identitas_kreditur1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli_has_persyaratan`
--
ALTER TABLE `pembeli_has_persyaratan`
  ADD CONSTRAINT `fk_pembeli_has_persyaratan_pembelian1` FOREIGN KEY (`pembelian`) REFERENCES `pembelian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembeli_has_persyaratan_persyaratan_has_berkas1` FOREIGN KEY (`persyaratan_has_berkas`) REFERENCES `persyaratan_has_berkas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli_has_product`
--
ALTER TABLE `pembeli_has_product`
  ADD CONSTRAINT `fk_kreditur_has_rumah_kreditur1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kreditur_has_rumah_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembeli_has_rumah_pembelian1` FOREIGN KEY (`pembelian`) REFERENCES `pembelian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembeli_has_rumah_status_pembelian1` FOREIGN KEY (`status_pembelian`) REFERENCES `status_pembelian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `persyaratan`
--
ALTER TABLE `persyaratan`
  ADD CONSTRAINT `fk_persyaratan_jenis_akad1` FOREIGN KEY (`jenis_akad`) REFERENCES `jenis_akad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_persyaratan_kategori_akad1` FOREIGN KEY (`kategori_akad`) REFERENCES `kategori_akad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `persyaratan_has_berkas`
--
ALTER TABLE `persyaratan_has_berkas`
  ADD CONSTRAINT `fk_persyaratan_has_berkas_persyaratan1` FOREIGN KEY (`persyaratan`) REFERENCES `persyaratan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_rumah_kategori_rumah1` FOREIGN KEY (`kategori_product`) REFERENCES `kategori_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rumah_tipe_rumah1` FOREIGN KEY (`tipe_product`) REFERENCES `tipe_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_distriburst`
--
ALTER TABLE `product_distriburst`
  ADD CONSTRAINT `fk_product_distriburst_pembeli1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_distriburst_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_has_foto`
--
ALTER TABLE `product_has_foto`
  ADD CONSTRAINT `fk_rumah_has_foto_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_has_harga_angsuran`
--
ALTER TABLE `product_has_harga_angsuran`
  ADD CONSTRAINT `fk_rumah_has_harga_angsuran_ansuran1` FOREIGN KEY (`ansuran`) REFERENCES `ansuran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rumah_has_harga_angsuran_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_has_harga_jual_pokok`
--
ALTER TABLE `product_has_harga_jual_pokok`
  ADD CONSTRAINT `fk_rumah_has_harga_jual_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_has_harga_jual_tunai`
--
ALTER TABLE `product_has_harga_jual_tunai`
  ADD CONSTRAINT `fk_rumah_has_harga_jual_kredit_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_kirim`
--
ALTER TABLE `product_kirim`
  ADD CONSTRAINT `fk_product_kirim_document_pengiriman1` FOREIGN KEY (`document_pengiriman`) REFERENCES `document_pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_kirim_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_kirim_status`
--
ALTER TABLE `product_kirim_status`
  ADD CONSTRAINT `fk_product_kirim_status_product_kirim1` FOREIGN KEY (`product_kirim`) REFERENCES `product_kirim` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_log_stock`
--
ALTER TABLE `product_log_stock`
  ADD CONSTRAINT `fk_product_log_stock_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_satuan`
--
ALTER TABLE `product_satuan`
  ADD CONSTRAINT `fk_product_satuan_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_stock`
--
ALTER TABLE `product_stock`
  ADD CONSTRAINT `fk_product_stock_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_surat_jalan`
--
ALTER TABLE `product_surat_jalan`
  ADD CONSTRAINT `fk_product_surat_jalan_document_pengiriman1` FOREIGN KEY (`document_pengiriman`) REFERENCES `document_pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_vendor`
--
ALTER TABLE `product_vendor`
  ADD CONSTRAINT `fk_product_vendor_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_vendor_vendor1` FOREIGN KEY (`vendor`) REFERENCES `vendor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_priveledge` FOREIGN KEY (`priveledge`) REFERENCES `priveledge` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `vendor`
--
ALTER TABLE `vendor`
  ADD CONSTRAINT `fk_vendor_vendor_category1` FOREIGN KEY (`vendor_category`) REFERENCES `vendor_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `zakat_mal`
--
ALTER TABLE `zakat_mal`
  ADD CONSTRAINT `fk_zakat_mal_periode1` FOREIGN KEY (`periode`) REFERENCES `periode` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
