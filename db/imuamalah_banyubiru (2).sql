-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 05, 2019 at 05:29 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `imuamalah_banyubiru`
--

-- --------------------------------------------------------

--
-- Table structure for table `ansuran`
--

CREATE TABLE `ansuran` (
  `id` int(11) NOT NULL,
  `periode_tahun` varchar(50) DEFAULT NULL,
  `ansuran` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `akun` varchar(255) DEFAULT NULL,
  `nama_bank` varchar(255) DEFAULT NULL,
  `no_rekening` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `akun`, `nama_bank`, `no_rekening`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Tejo Kurniawan', 'Mandiri', '14132434', '2019-10-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `coa`
--

CREATE TABLE `coa` (
  `id` int(11) NOT NULL,
  `code` varchar(100) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coa`
--

INSERT INTO `coa` (`id`, `code`, `parent`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, '01', NULL, 'Income / Pemasukan', '2019-10-19', 1, '2019-10-22 02:55:00', 1, 0),
(2, '02', NULL, 'Cost of Revenue / Pengeluaran', '2019-10-22', 1, NULL, NULL, 0),
(3, '03', NULL, 'Expenses / Beban Biaya', '2019-10-22', 1, NULL, NULL, 0),
(4, '04', NULL, 'Payable / Utang usaha', '2019-10-22', 1, '2019-10-22 08:17:41', NULL, 0),
(5, '05', NULL, 'Equity / Ekuitas', '2019-10-22', 1, NULL, NULL, 0),
(6, '06', NULL, 'Aset Tidak Lancar / Aset Tetap', '2019-10-22', 1, NULL, NULL, 0),
(7, '07', NULL, 'Aset Lancar', '2019-10-22', 1, '2019-10-22 03:04:44', 1, 0),
(8, '08', NULL, 'Bank & Kas', '2019-10-22', 1, NULL, NULL, 0),
(9, '09', NULL, 'Liabilitas', '2019-10-22', 1, NULL, NULL, 0),
(10, '10', NULL, 'Receivable / Piutang usaha', '2019-10-22', NULL, '2019-10-22 08:18:05', NULL, 0),
(11, '11', NULL, 'Kasbon', '2019-10-24', 1, '2019-10-24 04:39:31', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `coa_type`
--

CREATE TABLE `coa_type` (
  `id` int(11) NOT NULL,
  `jenis` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coa_type`
--

INSERT INTO `coa_type` (`id`, `jenis`) VALUES
(1, 'KREDIT'),
(2, 'DEBIT');

-- --------------------------------------------------------

--
-- Table structure for table `data_notifikasi_tagihan`
--

CREATE TABLE `data_notifikasi_tagihan` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `message` varchar(150) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `jenis` varchar(100) DEFAULT NULL COMMENT 'FAKTUR\nRETUR\nPENGADAAN',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `document_pengiriman`
--

CREATE TABLE `document_pengiriman` (
  `id` int(11) NOT NULL,
  `no_dokumen` varchar(155) DEFAULT NULL,
  `tgl_pengiriman` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dokumen_pengiriman_status`
--

CREATE TABLE `dokumen_pengiriman_status` (
  `id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'SENT\nBROKE\nRECEIVED',
  `document_pengiriman` int(11) NOT NULL,
  `keterangan` varchar(155) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feature`
--

CREATE TABLE `feature` (
  `id` int(11) NOT NULL,
  `nama` varchar(155) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feature`
--

INSERT INTO `feature` (`id`, `nama`) VALUES
(1, 'Dashboard'),
(2, 'Pelanggan'),
(3, 'Produk'),
(4, 'Surat Jalan'),
(5, 'Retur Penjualan'),
(6, 'Rute Salesman'),
(7, 'Faktur Bersyarat'),
(8, 'Faktur'),
(9, 'Syarat Pembayaran'),
(10, 'Metode Pembayaran'),
(11, 'Vendor'),
(12, 'Pengadaan'),
(13, 'Retur Pembelian'),
(14, 'Tagihan'),
(15, 'Biaya Umum'),
(16, 'Gudang'),
(17, 'Rak'),
(18, 'Stok'),
(19, 'Bagan Akun'),
(20, 'Bank Akun'),
(21, 'Kas Bank'),
(22, 'Mata Uang'),
(23, 'Gaji'),
(24, 'Kategori Gaji'),
(25, 'Periode'),
(26, 'Kasbon'),
(27, 'Karyawan'),
(28, 'Kontrak'),
(29, 'E-Faktur'),
(30, 'Zakat Usaha'),
(31, 'Pembayaran'),
(32, 'Kasbon Bayar'),
(33, 'Reimburse'),
(34, 'Order'),
(35, 'Piutang Non Usaha');

-- --------------------------------------------------------

--
-- Table structure for table `general`
--

CREATE TABLE `general` (
  `id` int(11) NOT NULL,
  `title` varchar(150) DEFAULT NULL,
  `logo` text,
  `alamat` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general`
--

INSERT INTO `general` (`id`, `title`, `logo`, `alamat`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Perusahaan', 'blank.png', 'Surabaya', NULL, NULL, '2019-10-24 09:22:21', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `gudang`
--

CREATE TABLE `gudang` (
  `id` int(11) NOT NULL,
  `nama_gudang` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gudang`
--

INSERT INTO `gudang` (`id`, `nama_gudang`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'UMUM', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hari`
--

CREATE TABLE `hari` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hari`
--

INSERT INTO `hari` (`id`, `nama`) VALUES
(1, 'Senin'),
(2, 'Selasa'),
(3, 'Rabu'),
(4, 'Kamis'),
(5, 'Jumat'),
(6, 'Sabtu'),
(7, 'Minggu');

-- --------------------------------------------------------

--
-- Table structure for table `indetitas`
--

CREATE TABLE `indetitas` (
  `id` int(11) NOT NULL,
  `identitas` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `investor`
--

CREATE TABLE `investor` (
  `id` int(11) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `no_hp` varchar(45) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `alamat` text,
  `keterangan` varchar(150) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `jenis_pembayaran` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `potongan` int(11) NOT NULL,
  `ref` int(11) DEFAULT NULL COMMENT 'oder table',
  `pot_faktur` int(11) DEFAULT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`id`, `jenis_pembayaran`, `pembeli`, `potongan`, `ref`, `pot_faktur`, `no_faktur`, `tanggal_faktur`, `tanggal_bayar`, `total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 1, 1, NULL, NULL, 'INV19NOV001', '2019-11-18', '2019-11-18', 412000, '2019-11-18', 1, NULL, NULL, 0),
(2, 2, 1, 1, NULL, NULL, 'INV19NOV002', '2019-11-26', '2019-11-26', 1600000, '2019-11-26', 1, NULL, NULL, 0),
(5, 1, 1, 1, NULL, 0, 'INV19NOV003', '2019-11-28', '2019-11-29', 300000, '2019-11-28', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_biaya`
--

CREATE TABLE `invoice_biaya` (
  `id` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `ket_biaya` varchar(155) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updatedate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_biaya`
--

INSERT INTO `invoice_biaya` (`id`, `invoice`, `ket_biaya`, `jumlah`, `createddate`, `createdby`, `updatedate`, `updatedby`, `deleted`) VALUES
(1, 12, 'Biaya Lain', 3000, '2019-11-14', 1, NULL, NULL, 0),
(2, 1, 'Biaya Transport', 12000, '2019-11-18', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_pot_product`
--

CREATE TABLE `invoice_pot_product` (
  `id` int(11) NOT NULL,
  `invoice_product` int(11) NOT NULL,
  `nilai` int(11) DEFAULT NULL,
  `potongan` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_pot_product`
--

INSERT INTO `invoice_pot_product` (`id`, `invoice_product`, `nilai`, `potongan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 3, 100000, 1, '2019-11-28', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_print`
--

CREATE TABLE `invoice_print` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `cetak` int(11) DEFAULT NULL,
  `approve` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_print`
--

INSERT INTO `invoice_print` (`id`, `user`, `invoice`, `cetak`, `approve`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 2, NULL, NULL, '2019-11-26', 1, NULL, NULL, 0),
(2, 1, 1, NULL, NULL, '2019-11-26', 1, NULL, NULL, 0),
(3, 1, 1, NULL, 'ASK', '2019-11-26', 1, NULL, NULL, 0),
(4, 1, 1, NULL, 'APPROVED', '2019-11-26', 1, NULL, NULL, 0),
(5, 1, 1, NULL, NULL, '2019-11-26', 1, NULL, NULL, 0),
(6, 1, 1, NULL, NULL, '2019-11-26', 1, NULL, NULL, 0),
(7, 1, 1, NULL, NULL, '2019-11-26', 1, NULL, NULL, 0),
(8, 1, 1, NULL, NULL, '2019-11-26', 1, NULL, NULL, 0),
(9, 1, 1, NULL, 'ASK', '2019-11-27', 1, NULL, NULL, 0),
(10, 1, 1, NULL, 'APPROVED', '2019-11-27', 1, NULL, NULL, 0),
(11, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(12, 1, 1, NULL, 'ASK', '2019-11-27', 1, NULL, NULL, 0),
(13, 1, 1, NULL, 'APPROVED', '2019-11-27', 1, NULL, NULL, 0),
(14, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(15, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(16, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(17, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(18, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(19, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(20, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(21, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(22, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(23, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(24, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(25, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(26, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(27, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(28, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(29, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(30, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(31, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(32, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(33, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(34, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(35, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(36, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(37, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(38, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(39, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(40, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(41, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(42, 1, 1, NULL, NULL, '2019-11-27', 1, NULL, NULL, 0),
(43, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(44, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(45, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(46, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(47, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(48, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(49, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(50, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(51, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(52, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(53, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(54, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(55, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(56, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(57, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(58, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(59, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(60, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(61, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(62, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(63, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(64, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(65, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(66, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(67, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(68, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(69, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(70, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0),
(71, 1, 5, NULL, NULL, '2019-11-28', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_product`
--

CREATE TABLE `invoice_product` (
  `id` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `metode_bayar` int(11) NOT NULL,
  `bank` int(11) DEFAULT '0',
  `pajak` int(11) NOT NULL,
  `qty` double DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_product`
--

INSERT INTO `invoice_product` (`id`, `invoice`, `product_satuan`, `metode_bayar`, `bank`, `pajak`, `qty`, `sub_total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 5371, 1, 0, 1, 1, 400000, '2019-11-18', 1, NULL, NULL, 0),
(2, 2, 5371, 1, 0, 1, 4.5, 1600000, '2019-11-26', 1, NULL, NULL, 0),
(3, 5, 5371, 1, 0, 1, 1, 300, '2019-11-28', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_sisa`
--

CREATE TABLE `invoice_sisa` (
  `id` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_sisa`
--

INSERT INTO `invoice_sisa` (`id`, `invoice`, `jumlah`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 412000, '2019-11-18', 1, NULL, NULL, 0),
(2, 2, 1600000, '2019-11-26', 1, NULL, NULL, 0),
(3, 5, 300000, '2019-11-28', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_status`
--

CREATE TABLE `invoice_status` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'PAID\nDRAFT\nVALID',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoice_status`
--

INSERT INTO `invoice_status` (`id`, `user`, `invoice`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 1, 'DRAFT', '2019-11-18', 1, NULL, NULL, 0),
(2, 1, 2, 'DRAFT', '2019-11-26', 1, NULL, NULL, 0),
(3, 1, 5, 'DRAFT', '2019-11-28', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_akad`
--

CREATE TABLE `jenis_akad` (
  `id` int(11) NOT NULL,
  `jenis` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_akad`
--

INSERT INTO `jenis_akad` (`id`, `jenis`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Jenis Akad 1', '2019-10-11', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_notif_pengiriman`
--

CREATE TABLE `jenis_notif_pengiriman` (
  `id` int(11) NOT NULL,
  `jenis` varchar(100) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_notif_pengiriman`
--

INSERT INTO `jenis_notif_pengiriman` (`id`, `jenis`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`) VALUES
(1, 'Email', 1, NULL, NULL, NULL, NULL),
(2, 'Sms', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_pembayaran`
--

CREATE TABLE `jenis_pembayaran` (
  `id` int(11) NOT NULL,
  `jenis` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_pembayaran`
--

INSERT INTO `jenis_pembayaran` (`id`, `jenis`) VALUES
(1, 'TUNAI'),
(2, 'KREDIT'),
(4, 'TRANSFER');

-- --------------------------------------------------------

--
-- Table structure for table `jurnal`
--

CREATE TABLE `jurnal` (
  `id` int(11) NOT NULL,
  `no_jurnal` varchar(155) DEFAULT NULL,
  `ref` varchar(155) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurnal`
--

INSERT INTO `jurnal` (`id`, `no_jurnal`, `ref`, `tanggal`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(4, 'JURNAL19OCT001', NULL, '2019-10-22', '2019-10-22', 1, NULL, NULL, 0),
(5, 'JURNAL19OCT002', NULL, '2019-10-22', '2019-10-22', 1, NULL, NULL, 0),
(6, 'JURNAL19OCT003', NULL, '2019-10-23', '2019-10-23', 1, NULL, NULL, 0),
(7, 'JURNAL19OCT004', NULL, '2019-10-23', '2019-10-23', 1, NULL, NULL, 0),
(9, 'JURNAL19OCT005', NULL, '2019-10-24', '2019-10-24', 1, NULL, NULL, 0),
(10, 'JURNAL19OCT006', NULL, '2019-10-24', '2019-10-24', 1, NULL, NULL, 0),
(11, 'JURNAL19OCT007', NULL, '2019-10-25', '2019-10-25', 1, NULL, NULL, 0),
(12, 'JURNAL19OCT008', NULL, '2019-10-26', '2019-10-26', 1, NULL, NULL, 0),
(13, 'JURNAL19OCT009', NULL, '2019-10-29', '2019-10-29', 1, NULL, NULL, 0),
(14, 'JURNAL19OCT010', NULL, '2019-10-30', '2019-10-30', 1, NULL, NULL, 0),
(15, 'JURNAL19OCT011', NULL, '2019-10-30', '2019-10-30', NULL, NULL, NULL, 0),
(16, 'JURNAL19OCT012', NULL, '2019-10-30', '2019-10-30', NULL, NULL, NULL, 0),
(17, 'JURNAL19NOV001', NULL, '2019-11-01', '2019-11-01', NULL, NULL, NULL, 0),
(18, 'JURNAL19NOV002', NULL, '2019-11-01', '2019-11-01', NULL, NULL, NULL, 0),
(19, 'JURNAL19NOV003', NULL, '2019-11-01', '2019-11-01', NULL, NULL, NULL, 0),
(20, 'JURNAL19NOV004', NULL, '2019-11-01', '2019-11-01', NULL, NULL, NULL, 0),
(21, 'JURNAL19NOV005', NULL, '2019-11-01', '2019-11-01', NULL, NULL, NULL, 0),
(22, 'JURNAL19NOV006', 'INV19NOV002', '2019-11-02', '2019-11-02', 1, NULL, NULL, 0),
(23, 'JURNAL19NOV007', 'INVPEMTAG19NOV004', '2019-11-06', '2019-11-06', NULL, NULL, NULL, 0),
(24, 'JURNAL19NOV008', 'INVPEMTAG19NOV005', '2019-11-06', '2019-11-06', NULL, NULL, NULL, 0),
(25, 'JURNAL19NOV009', 'INV19NOV003', '2019-11-06', '2019-11-06', 1, NULL, NULL, 0),
(26, 'JURNAL19NOV010', 'INV19NOV004', '2019-11-06', '2019-11-06', 1, NULL, NULL, 0),
(27, 'JURNAL19NOV011', 'INV19NOV001', '2019-11-09', '2019-11-09', 1, NULL, NULL, 0),
(28, 'JURNAL19NOV012', 'INV19NOV002', '2019-11-09', '2019-11-09', 1, NULL, NULL, 0),
(29, 'JURNAL19NOV013', 'PROC19NOV001', '2019-11-09', '2019-11-09', 1, NULL, NULL, 0),
(30, 'JURNAL19NOV014', 'INV19NOV001', '2019-11-10', '2019-11-10', 1, NULL, NULL, 0),
(31, 'JURNAL19NOV015', 'INV19NOV002', '2019-11-10', '2019-11-10', 1, NULL, NULL, 0),
(32, 'JURNAL19NOV016', 'INV19NOV001', '2019-11-10', '2019-11-10', 1, NULL, NULL, 0),
(33, 'JURNAL19NOV017', 'INV19NOV001', '2019-11-13', '2019-11-13', 1, NULL, NULL, 0),
(34, 'JURNAL19NOV018', 'INV19NOV002', '2019-11-13', '2019-11-13', 1, NULL, NULL, 0),
(35, 'JURNAL19NOV019', 'PAY19NOV001', '2019-11-13', '2019-11-13', 1, NULL, NULL, 0),
(36, 'JURNAL19NOV020', 'RECUST19NOV001', '2019-11-13', '2019-11-13', 1, NULL, NULL, 0),
(37, 'JURNAL19NOV021', 'INV19NOV001', '2019-11-13', '2019-11-13', 1, NULL, NULL, 0),
(38, 'JURNAL19NOV022', 'INV19NOV001', '2019-11-14', '2019-11-14', 1, NULL, NULL, 0),
(39, 'JURNAL19NOV023', 'INV19NOV002', '2019-11-14', '2019-11-14', 1, NULL, NULL, 0),
(40, 'JURNAL19NOV024', 'INV19NOV003', '2019-11-14', '2019-11-14', 1, NULL, NULL, 0),
(41, 'JURNAL19NOV025', 'INV19NOV004', '2019-11-14', '2019-11-14', 1, NULL, NULL, 0),
(42, 'JURNAL19NOV026', 'INVPEMPIUT19NOV001', '2019-11-14', '2019-11-14', 1, NULL, NULL, 0),
(43, 'JURNAL19NOV027', 'INVPEMPIUT19NOV002', '2019-11-14', '2019-11-14', 1, NULL, NULL, 0),
(44, 'JURNAL19NOV028', 'INVPEMPIUT19NOV003', '2019-11-14', '2019-11-14', 1, NULL, NULL, 0),
(45, 'JURNAL19NOV029', 'PAYSLIP19NOV001', '2019-11-14', '2019-11-14', 1, NULL, NULL, 0),
(46, 'JURNAL19NOV030', 'INV19NOV001', '2019-11-18', '2019-11-18', 1, NULL, NULL, 0),
(47, 'JURNAL19NOV031', 'INV19NOV002', '2019-11-26', '2019-11-26', 1, NULL, NULL, 0),
(48, 'JURNAL19NOV032', 'PAYSLIP19NOV001', '2019-11-26', '2019-11-26', 1, NULL, NULL, 0),
(49, 'JURNAL19NOV033', 'PAYSLIP19NOV002', '2019-11-26', '2019-11-26', 1, NULL, NULL, 0),
(50, 'JURNAL19NOV034', 'PAYSLIP19NOV003', '2019-11-26', '2019-11-26', 1, NULL, NULL, 0),
(51, 'JURNAL19NOV035', 'INV19NOV003', '2019-11-28', '2019-11-28', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_detail`
--

CREATE TABLE `jurnal_detail` (
  `id` int(11) NOT NULL,
  `jurnal` int(11) NOT NULL,
  `jurnal_struktur` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurnal_detail`
--

INSERT INTO `jurnal_detail` (`id`, `jurnal`, `jurnal_struktur`, `jumlah`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 27, 1, 2380000, '2019-11-09', 1, NULL, NULL, 0),
(2, 27, 2, 2380000, '2019-11-09', 1, NULL, NULL, 0),
(3, 28, 1, 56000, '2019-11-09', 1, NULL, NULL, 0),
(4, 28, 2, 56000, '2019-11-09', 1, NULL, NULL, 0),
(5, 29, 11, 1950000, '2019-11-09', 1, NULL, NULL, 0),
(6, 29, 12, 1950000, '2019-11-09', 1, NULL, NULL, 0),
(7, 30, 1, 212000, '2019-11-10', 1, NULL, NULL, 0),
(8, 30, 2, 212000, '2019-11-10', 1, NULL, NULL, 0),
(9, 31, 1, 46600, '2019-11-10', 1, NULL, NULL, 0),
(10, 31, 2, 46600, '2019-11-10', 1, NULL, NULL, 0),
(11, 32, 1, 53866, '2019-11-10', 1, NULL, NULL, 0),
(12, 32, 2, 53866, '2019-11-10', 1, NULL, NULL, 0),
(13, 33, 1, 6798000, '2019-11-13', 1, NULL, NULL, 0),
(14, 33, 2, 6798000, '2019-11-13', 1, NULL, NULL, 0),
(15, 34, 1, 6798000, '2019-11-13', 1, NULL, NULL, 0),
(16, 34, 2, 6798000, '2019-11-13', 1, NULL, NULL, 0),
(17, 35, 3, 6798000, '2019-11-13', 1, NULL, NULL, 0),
(18, 35, 4, 6798000, '2019-11-13', 1, NULL, NULL, 0),
(19, 36, 15, 679800, '2019-11-13', 1, NULL, NULL, 0),
(20, 36, 16, 679800, '2019-11-13', 1, NULL, NULL, 0),
(21, 37, 1, 55650, '2019-11-13', 1, NULL, NULL, 0),
(22, 37, 2, 55650, '2019-11-13', 1, NULL, NULL, 0),
(23, 38, 1, 400000, '2019-11-14', 1, NULL, NULL, 0),
(24, 38, 2, 400000, '2019-11-14', 1, NULL, NULL, 0),
(25, 39, 1, 412000, '2019-11-14', 1, NULL, NULL, 0),
(26, 39, 2, 412000, '2019-11-14', 1, NULL, NULL, 0),
(27, 40, 1, 403000, '2019-11-14', 1, NULL, NULL, 0),
(28, 40, 2, 403000, '2019-11-14', 1, NULL, NULL, 0),
(29, 41, 1, 403000, '2019-11-14', 1, NULL, NULL, 0),
(30, 41, 2, 403000, '2019-11-14', 1, NULL, NULL, 0),
(31, 42, 21, 300000, '2019-11-14', 1, NULL, NULL, 0),
(32, 42, 22, 300000, '2019-11-14', 1, NULL, NULL, 0),
(33, 43, 21, 300000, '2019-11-14', 1, NULL, NULL, 0),
(34, 43, 22, 300000, '2019-11-14', 1, NULL, NULL, 0),
(35, 44, 21, 300000, '2019-11-14', 1, NULL, NULL, 0),
(36, 44, 22, 300000, '2019-11-14', 1, NULL, NULL, 0),
(37, 45, 5, 1200000, '2019-11-14', 1, NULL, NULL, 0),
(38, 45, 6, 1200000, '2019-11-14', 1, NULL, NULL, 0),
(39, 46, 1, 412000, '2019-11-18', 1, NULL, NULL, 0),
(40, 46, 2, 412000, '2019-11-18', 1, NULL, NULL, 0),
(41, 47, 1, 1600000, '2019-11-26', 1, NULL, NULL, 0),
(42, 47, 2, 1600000, '2019-11-26', 1, NULL, NULL, 0),
(43, 48, 5, 1190000, '2019-11-26', 1, NULL, NULL, 0),
(44, 48, 6, 1190000, '2019-11-26', 1, NULL, NULL, 0),
(45, 49, 5, 1190000, '2019-11-26', 1, NULL, NULL, 0),
(46, 49, 6, 1190000, '2019-11-26', 1, NULL, NULL, 0),
(47, 50, 5, 1500000, '2019-11-26', 1, NULL, NULL, 0),
(48, 50, 6, 1500000, '2019-11-26', 1, NULL, NULL, 0),
(49, 51, 1, 300000, '2019-11-28', 1, NULL, NULL, 0),
(50, 51, 2, 300000, '2019-11-28', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jurnal_struktur`
--

CREATE TABLE `jurnal_struktur` (
  `id` int(11) NOT NULL,
  `feature` int(11) NOT NULL,
  `coa` int(11) NOT NULL,
  `coa_type` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurnal_struktur`
--

INSERT INTO `jurnal_struktur` (`id`, `feature`, `coa`, `coa_type`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 8, 10, 2, '2019-10-22', NULL, '2019-10-22 08:18:37', NULL, 0),
(2, 8, 1, 1, '2019-10-22', NULL, NULL, NULL, 0),
(3, 31, 10, 1, '2019-10-22', 1, '2019-10-22 16:24:37', 1, 0),
(4, 31, 8, 2, '2019-10-22', 1, NULL, NULL, 0),
(5, 23, 3, 2, '2019-10-23', 1, NULL, NULL, 0),
(6, 23, 8, 1, '2019-10-23', 1, NULL, NULL, 0),
(7, 26, 8, 1, '2019-10-24', 1, NULL, NULL, 0),
(8, 26, 10, 2, '2019-10-24', 1, NULL, NULL, 0),
(9, 32, 8, 2, '2019-10-24', 1, NULL, NULL, 0),
(10, 32, 10, 1, '2019-10-24', 1, NULL, NULL, 0),
(11, 12, 2, 2, '2019-10-25', 1, NULL, NULL, 0),
(12, 12, 8, 1, '2019-10-25', 1, NULL, NULL, 0),
(13, 13, 2, 1, '2019-10-26', 1, NULL, NULL, 0),
(14, 13, 8, 2, '2019-10-26', 1, NULL, NULL, 0),
(15, 5, 8, 1, '2019-10-29', 1, NULL, NULL, 0),
(16, 5, 10, 2, '2019-10-29', 1, NULL, NULL, 0),
(17, 33, 8, 1, '2019-10-30', 1, NULL, NULL, 0),
(18, 33, 2, 2, '2019-10-30', 1, NULL, NULL, 0),
(19, 15, 2, 2, '2019-11-01', 1, NULL, NULL, 0),
(20, 15, 8, 1, '2019-11-01', 1, NULL, NULL, 0),
(21, 35, 3, 2, '2019-11-14', 1, NULL, NULL, 0),
(22, 35, 8, 1, '2019-11-14', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kas`
--

CREATE TABLE `kas` (
  `id` int(11) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon`
--

CREATE TABLE `kasbon` (
  `id` int(11) NOT NULL,
  `no_kasbon` varchar(155) DEFAULT NULL,
  `pegawai` int(11) NOT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon_paid`
--

CREATE TABLE `kasbon_paid` (
  `id` int(11) NOT NULL,
  `payroll` int(11) NOT NULL,
  `kasbon` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon_payment`
--

CREATE TABLE `kasbon_payment` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon_payment_item`
--

CREATE TABLE `kasbon_payment_item` (
  `id` int(11) NOT NULL,
  `kasbon_payment` int(11) NOT NULL,
  `kasbon` int(11) NOT NULL,
  `jumlah_bayar` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon_sisa`
--

CREATE TABLE `kasbon_sisa` (
  `id` int(11) NOT NULL,
  `kasbon` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kasbon_status`
--

CREATE TABLE `kasbon_status` (
  `id` int(11) NOT NULL,
  `kasbon` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'DRAFT\nPAID',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori_akad`
--

CREATE TABLE `kategori_akad` (
  `id` int(11) NOT NULL,
  `kategori` varchar(100) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kategori_product`
--

CREATE TABLE `kategori_product` (
  `id` int(11) NOT NULL,
  `kategori` varchar(100) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori_product`
--

INSERT INTO `kategori_product` (`id`, `kategori`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'BARANG', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kerja_sama_internal`
--

CREATE TABLE `kerja_sama_internal` (
  `id` int(11) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `presentase` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `log_user_position`
--

CREATE TABLE `log_user_position` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `lat` double DEFAULT NULL,
  `lng` double DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_user_position`
--

INSERT INTO `log_user_position` (`id`, `user`, `lat`, `lng`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 3, -7.250445, 112.768845, NULL, NULL, NULL, NULL, 0),
(2, 3, -6.2, 106.816666, NULL, NULL, NULL, NULL, 0),
(3, 3, -7.161367, 113.482498, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `metode_bayar`
--

CREATE TABLE `metode_bayar` (
  `id` int(11) NOT NULL,
  `metode` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `metode_bayar`
--

INSERT INTO `metode_bayar` (`id`, `metode`) VALUES
(1, 'CASH'),
(2, 'TRANSFER');

-- --------------------------------------------------------

--
-- Table structure for table `metode_pembayaran`
--

CREATE TABLE `metode_pembayaran` (
  `id` int(11) NOT NULL,
  `metode` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `metode_pembayaran`
--

INSERT INTO `metode_pembayaran` (`id`, `metode`) VALUES
(1, 'Tagihan'),
(2, 'Vendor'),
(3, 'Lain - lain'),
(4, 'Piutang Non Usaha');

-- --------------------------------------------------------

--
-- Table structure for table `minggu`
--

CREATE TABLE `minggu` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `minggu`
--

INSERT INTO `minggu` (`id`, `nama`) VALUES
(1, 'Minggu 1'),
(2, 'Minggu 2'),
(3, 'Minggu 3'),
(4, 'Minggu 4');

-- --------------------------------------------------------

--
-- Table structure for table `notif_has_jenis_pengiriman`
--

CREATE TABLE `notif_has_jenis_pengiriman` (
  `id` int(11) NOT NULL,
  `notif_jatuh_tempo` int(11) NOT NULL,
  `jenis_notif_pengiriman` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notif_has_jenis_pengiriman`
--

INSERT INTO `notif_has_jenis_pengiriman` (`id`, `notif_jatuh_tempo`, `jenis_notif_pengiriman`, `createddate`, `createdby`, `updateddate`, `updatedby`) VALUES
(1, 1, 1, '2019-10-24', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `notif_jatuh_tempo`
--

CREATE TABLE `notif_jatuh_tempo` (
  `id` int(11) NOT NULL,
  `nama_notif` varchar(150) DEFAULT NULL,
  `jadwal_notif` int(11) DEFAULT NULL COMMENT 'Jumlah Hari',
  `jenis_notif` varchar(1) DEFAULT NULL COMMENT '+ = Lebih\n- = Kurang',
  `status` int(11) DEFAULT '0' COMMENT '0 : Tidak Aktif\n1 : Aktif',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notif_jatuh_tempo`
--

INSERT INTO `notif_jatuh_tempo` (`id`, `nama_notif`, `jadwal_notif`, `jenis_notif`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Notif Jatuh Tempo', 12, '+', 0, '2019-10-24', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `no_order` varchar(155) DEFAULT NULL,
  `potongan` int(11) NOT NULL,
  `pot_faktur` int(11) DEFAULT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_pot_product`
--

CREATE TABLE `order_pot_product` (
  `id` int(11) NOT NULL,
  `order_product` int(11) NOT NULL,
  `nilai` int(11) DEFAULT NULL,
  `potongan` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_product`
--

CREATE TABLE `order_product` (
  `id` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pajak`
--

CREATE TABLE `pajak` (
  `id` int(11) NOT NULL,
  `jenis` varchar(155) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `persentase` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pajak`
--

INSERT INTO `pajak` (`id`, `jenis`, `keterangan`, `persentase`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Bebas Pajak', 'Bebas tidak kena pajak', 0, NULL, NULL, NULL, NULL, 0),
(2, ' PPH 10 %', 'Kena Pajak 10 Persen', 10, '2019-10-10', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `partial_payment`
--

CREATE TABLE `partial_payment` (
  `id` int(11) NOT NULL,
  `no_faktur_bayar` varchar(150) DEFAULT NULL,
  `invoice` int(11) NOT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `partial_payment_status`
--

CREATE TABLE `partial_payment_status` (
  `id` int(11) NOT NULL,
  `partial_payment` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'LUNAS\nKREDIT',
  `sisa_hutang` int(11) DEFAULT '0',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `no_faktur_bayar` varchar(155) DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `no_faktur_bayar`, `tanggal_bayar`, `tanggal_faktur`, `jumlah`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'PAY19NOV001', '2019-11-13', '2019-11-13', 6798000, '2019-11-13', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payment_item`
--

CREATE TABLE `payment_item` (
  `id` int(11) NOT NULL,
  `payment` int(11) NOT NULL,
  `invoice` int(11) NOT NULL,
  `jumlah_bayar` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_item`
--

INSERT INTO `payment_item` (`id`, `payment`, `invoice`, `jumlah_bayar`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 37, 6798000, '2019-11-13', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payroll`
--

CREATE TABLE `payroll` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `periode` int(11) NOT NULL,
  `pegawai` int(11) NOT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `tanggal_bayar` date DEFAULT NULL,
  `keterangan` text,
  `total` int(11) DEFAULT NULL,
  `jumlah_hari_masuk` int(11) DEFAULT NULL,
  `jumlah_gaji_perhari` int(11) DEFAULT NULL,
  `jumlah_pengangkutan` int(11) DEFAULT NULL,
  `jumlah_gaji_pengangkutan` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payroll`
--

INSERT INTO `payroll` (`id`, `no_faktur`, `periode`, `pegawai`, `tanggal_faktur`, `tanggal_bayar`, `keterangan`, `total`, `jumlah_hari_masuk`, `jumlah_gaji_perhari`, `jumlah_pengangkutan`, `jumlah_gaji_pengangkutan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'PAYSLIP19NOV001', 3, 1, '2019-11-26', '2019-11-26', '-', 1190000, 20, 50000, 2, NULL, '2019-11-26', 1, NULL, NULL, 0),
(2, 'PAYSLIP19NOV002', 3, 1, '2019-11-26', '2019-11-26', '-', 1190000, 20, 50000, 2, NULL, '2019-11-26', 1, NULL, NULL, 0),
(3, 'PAYSLIP19NOV003', 3, 1, '2019-11-26', '2019-11-27', '-', 1500000, 20, 50000, 12, NULL, '2019-11-26', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payroll_category`
--

CREATE TABLE `payroll_category` (
  `id` int(11) NOT NULL,
  `jenis` varchar(45) DEFAULT NULL COMMENT 'POTONGAN\nTUNJANGAN\nDLL',
  `action` varchar(12) DEFAULT NULL,
  `angkutan` int(11) NOT NULL DEFAULT '0',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payroll_category`
--

INSERT INTO `payroll_category` (`id`, `jenis`, `action`, `angkutan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Potongan BPJS', '-', 0, NULL, NULL, '2019-10-13 05:29:56', 1, 0),
(2, 'Potongan Pajak', '-', 0, '2019-10-13', 1, '2019-10-13 05:29:48', 1, 0),
(3, 'Gaji Pokok', '+', 0, '2019-10-13', 1, '2019-10-13 05:28:33', 1, 0),
(4, 'Potongan Angsuran', '-', 0, '2019-11-15', 1, NULL, NULL, 0),
(5, 'Angkutan B', '+', 1, '2019-11-26', 1, '2019-11-26 14:55:42', 1, 0),
(6, 'Angkutan A', '+', 1, '2019-11-26', 1, '2019-11-26 04:48:42', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payroll_item`
--

CREATE TABLE `payroll_item` (
  `id` int(11) NOT NULL,
  `payroll` int(11) NOT NULL,
  `payroll_category` int(11) NOT NULL,
  `keterangan` text,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payroll_item`
--

INSERT INTO `payroll_item` (`id`, `payroll`, `payroll_category`, `keterangan`, `jumlah`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 1, 6, '-', 50000, '2019-11-26', 1, NULL, NULL, 0),
(2, 1, 5, '-', 45000, '2019-11-26', 1, NULL, NULL, 0),
(3, 2, 6, '-', 50000, '2019-11-26', 1, NULL, NULL, 0),
(4, 2, 5, '-', 45000, '2019-11-26', 1, NULL, NULL, 0),
(5, 3, 5, '-', 50000, '2019-11-26', 1, NULL, NULL, 0),
(6, 3, 4, '', 100000, '2019-11-26', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `payroll_workdays`
--

CREATE TABLE `payroll_workdays` (
  `id` int(11) NOT NULL,
  `payroll` int(11) NOT NULL,
  `keterangan` text,
  `qty_hari` int(11) DEFAULT NULL,
  `qty_jam` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `no_hp` varchar(45) DEFAULT NULL,
  `email` text,
  `alamat` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id`, `nama`, `no_hp`, `email`, `alamat`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Dodik Rismawan', '6285738233712', 'dodikitn@gmail.com', '-', '2019-11-14', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai_kontrak`
--

CREATE TABLE `pegawai_kontrak` (
  `id` int(11) NOT NULL,
  `pegawai` int(11) NOT NULL,
  `tanggal_awal` date DEFAULT NULL,
  `tanggal_akhir` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_has_angsuran`
--

CREATE TABLE `pembayaran_has_angsuran` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `pembayaran_product` int(11) NOT NULL,
  `total_bayar` int(11) DEFAULT NULL,
  `tgl_angsuran` date DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_has_cash`
--

CREATE TABLE `pembayaran_has_cash` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `pembayaran_product` int(11) NOT NULL,
  `tgl_bayar` date DEFAULT NULL,
  `total_bayar` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_has_product`
--

CREATE TABLE `pembayaran_has_product` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `pembayaran_product` int(11) NOT NULL,
  `total_bayar` int(11) DEFAULT NULL,
  `tgl_angsuran` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_lain_lain`
--

CREATE TABLE `pembayaran_lain_lain` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(45) DEFAULT NULL,
  `jenis_pembayaran` int(11) NOT NULL,
  `jenis` varchar(150) DEFAULT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `file` text,
  `tgl_bayar` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_piutang_non_usaha`
--

CREATE TABLE `pembayaran_piutang_non_usaha` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(255) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `jenis_pembayaran` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `tgl_bayar` date DEFAULT NULL,
  `file` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembayaran_piutang_non_usaha`
--

INSERT INTO `pembayaran_piutang_non_usaha` (`id`, `no_invoice`, `keterangan`, `jenis_pembayaran`, `total`, `tgl_bayar`, `file`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'INVPEMPIUT19NOV001', 'pembayaran -', 1, 1200000, '0000-00-00', NULL, '2019-11-19', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_product`
--

CREATE TABLE `pembayaran_product` (
  `id` int(11) NOT NULL,
  `pembeli_has_product` int(11) NOT NULL,
  `status_pembayaran` int(11) NOT NULL,
  `tgl_bayar` datetime DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_tagihan`
--

CREATE TABLE `pembayaran_tagihan` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `tagihan` int(11) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `jenis_pembayaran` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `file` text,
  `tgl_bayar` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembayaran_vendor`
--

CREATE TABLE `pembayaran_vendor` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `vendor` int(11) NOT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `jenis_pembayaran` int(11) NOT NULL,
  `file` text,
  `tgl_bayar` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli`
--

CREATE TABLE `pembeli` (
  `id` int(11) NOT NULL,
  `pembeli_kategori` int(11) NOT NULL,
  `nama` varchar(150) DEFAULT NULL,
  `alamat` varchar(150) DEFAULT NULL,
  `email` text,
  `no_hp` varchar(45) DEFAULT NULL,
  `foto` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembeli`
--

INSERT INTO `pembeli` (`id`, `pembeli_kategori`, `nama`, `alamat`, `email`, `no_hp`, `foto`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 2, 'Dodik Rismawan', '-', '-', '6285738233712', NULL, '2019-11-18', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `id` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `jatuh_tempo` int(11) DEFAULT NULL COMMENT 'Hari',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_has_angsuran`
--

CREATE TABLE `pembeli_has_angsuran` (
  `id` int(11) NOT NULL,
  `pembeli_has_product` int(11) NOT NULL,
  `product_has_harga_angsuran` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_has_identitas`
--

CREATE TABLE `pembeli_has_identitas` (
  `id` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `indetitas` int(11) NOT NULL,
  `no_identitas` varchar(150) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_has_persyaratan`
--

CREATE TABLE `pembeli_has_persyaratan` (
  `id` int(11) NOT NULL,
  `pembelian` int(11) NOT NULL,
  `persyaratan_has_berkas` int(11) NOT NULL,
  `berkas` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_has_product`
--

CREATE TABLE `pembeli_has_product` (
  `id` int(11) NOT NULL,
  `pembelian` int(11) NOT NULL,
  `no_invoice` varchar(100) DEFAULT NULL,
  `pembeli` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `status_pembelian` int(11) NOT NULL,
  `tgl_beli` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pembeli_kategori`
--

CREATE TABLE `pembeli_kategori` (
  `id` int(11) NOT NULL,
  `kategori` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembeli_kategori`
--

INSERT INTO `pembeli_kategori` (`id`, `kategori`) VALUES
(1, 'KONSUMEN I'),
(2, 'KONSUMEN II');

-- --------------------------------------------------------

--
-- Table structure for table `periode`
--

CREATE TABLE `periode` (
  `id` int(11) NOT NULL,
  `month` int(11) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updatedate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `periode`
--

INSERT INTO `periode` (`id`, `month`, `year`, `createddate`, `createdby`, `updatedate`, `updatedby`, `deleted`) VALUES
(1, 10, 2019, '2019-10-13', 1, NULL, NULL, 0),
(2, 12, 2019, '2019-11-15', 1, NULL, NULL, 0),
(3, 11, 2019, '2019-11-15', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `persyaratan`
--

CREATE TABLE `persyaratan` (
  `id` int(11) NOT NULL,
  `syarat` varchar(150) DEFAULT NULL,
  `kategori_akad` int(11) NOT NULL,
  `jenis_akad` int(11) NOT NULL,
  `lampirkan_berkas` int(11) DEFAULT NULL COMMENT '0 : Tidak\n1: YA',
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `persyaratan_has_berkas`
--

CREATE TABLE `persyaratan_has_berkas` (
  `id` int(11) NOT NULL,
  `persyaratan` int(11) NOT NULL,
  `nama_berkas` varchar(150) DEFAULT NULL COMMENT 'Nama Berkas Misal (KTP, SIM, dLL)',
  `status` varchar(150) DEFAULT NULL COMMENT 'Status Tanpa Upload Berkas atau Tidak\nN : Tanpa Berkas\nY : Dengan Berkas',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `potongan`
--

CREATE TABLE `potongan` (
  `id` int(11) NOT NULL,
  `potongan` varchar(45) DEFAULT NULL COMMENT 'Nominal\nPersentase'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `potongan`
--

INSERT INTO `potongan` (`id`, `potongan`) VALUES
(1, 'Nominal'),
(2, 'Persentase'),
(3, 'Tidak ada potongan');

-- --------------------------------------------------------

--
-- Table structure for table `priveledge`
--

CREATE TABLE `priveledge` (
  `id` int(11) NOT NULL,
  `hak_akses` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `priveledge`
--

INSERT INTO `priveledge` (`id`, `hak_akses`) VALUES
(1, 'superadmin'),
(2, 'company'),
(3, 'sales'),
(4, 'operational'),
(5, 'manajemen hrd'),
(6, 'manajemen accounting'),
(7, 'direktur');

-- --------------------------------------------------------

--
-- Table structure for table `procurement`
--

CREATE TABLE `procurement` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(150) DEFAULT NULL,
  `vendor` int(11) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `procurement_item`
--

CREATE TABLE `procurement_item` (
  `id` int(11) NOT NULL,
  `procurement` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `satuan` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `procurement_retur`
--

CREATE TABLE `procurement_retur` (
  `id` int(11) NOT NULL,
  `procurement` int(11) NOT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `procurement_retur_item`
--

CREATE TABLE `procurement_retur_item` (
  `id` int(11) NOT NULL,
  `procurement_item` int(11) NOT NULL,
  `procurement_retur` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `procurement_status`
--

CREATE TABLE `procurement_status` (
  `id` int(11) NOT NULL,
  `procurement` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'DRAFT\nPAID',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `product` varchar(150) DEFAULT NULL,
  `kode_product` varchar(50) DEFAULT NULL,
  `tipe_product` int(11) NOT NULL,
  `kategori_product` int(11) NOT NULL,
  `keterangan` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `product`, `kode_product`, `tipe_product`, `kategori_product`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(2840, 'PASIR 05', 'PS', 1, 1, '-', '2019-11-14', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_distriburst`
--

CREATE TABLE `product_distriburst` (
  `id` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `tanggal_kirim` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_has_foto`
--

CREATE TABLE `product_has_foto` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `foto` longtext,
  `status` varchar(45) DEFAULT NULL COMMENT 'updated : File Diganti dengan file lain',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_has_harga_angsuran`
--

CREATE TABLE `product_has_harga_angsuran` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `ansuran` int(11) NOT NULL,
  `period_start` date DEFAULT NULL,
  `period_end` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `harga_total` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_has_harga_jual_pokok`
--

CREATE TABLE `product_has_harga_jual_pokok` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `period_start` date DEFAULT NULL,
  `period_end` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_has_harga_jual_tunai`
--

CREATE TABLE `product_has_harga_jual_tunai` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `harga` int(11) DEFAULT NULL,
  `diskon` int(11) DEFAULT NULL,
  `period_start` date DEFAULT NULL,
  `period_end` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_kirim`
--

CREATE TABLE `product_kirim` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `document_pengiriman` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_kirim_status`
--

CREATE TABLE `product_kirim_status` (
  `id` int(11) NOT NULL,
  `product_kirim` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'SENT\nRECEIVED\nBROKE',
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_log_stock`
--

CREATE TABLE `product_log_stock` (
  `id` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL COMMENT 'OUT',
  `qty` int(11) DEFAULT NULL,
  `stok_kategori` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL COMMENT 'PEMBELIAN\nPENGIRIMAN',
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_log_stock`
--

INSERT INTO `product_log_stock` (`id`, `product_satuan`, `status`, `qty`, `stok_kategori`, `keterangan`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 5371, 'OUT', 1, 1, 'Faktur Pelanggan', '2019-11-18', 1, NULL, NULL, 0),
(2, 5371, 'OUT', 5, 1, 'Faktur Pelanggan', '2019-11-26', 1, NULL, NULL, 0),
(3, 5371, 'OUT', 1, 1, 'Faktur Pelanggan', '2019-11-28', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_satuan`
--

CREATE TABLE `product_satuan` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `satuan` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL COMMENT 'Isi Dari Satuan',
  `harga` int(11) DEFAULT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_satuan`
--

INSERT INTO `product_satuan` (`id`, `product`, `satuan`, `qty`, `harga`, `harga_beli`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(5371, 2840, 906, 1, 400000, 120000, '2019-11-14', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_stock`
--

CREATE TABLE `product_stock` (
  `id` int(11) NOT NULL,
  `product_satuan` int(11) NOT NULL,
  `stock` int(11) DEFAULT NULL,
  `gudang` int(11) DEFAULT NULL,
  `rak` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product_stock`
--

INSERT INTO `product_stock` (`id`, `product_satuan`, `stock`, `gudang`, `rak`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(6613, 5371, 100, 1, 1, '2019-11-14', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_surat_jalan`
--

CREATE TABLE `product_surat_jalan` (
  `id` int(11) NOT NULL,
  `no_sj` varchar(155) DEFAULT NULL,
  `document_pengiriman` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `product_vendor`
--

CREATE TABLE `product_vendor` (
  `id` int(11) NOT NULL,
  `product` int(11) NOT NULL,
  `vendor` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rak`
--

CREATE TABLE `rak` (
  `id` int(11) NOT NULL,
  `nama_rak` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rak`
--

INSERT INTO `rak` (`id`, `nama_rak`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'UTAMA', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `reimburse`
--

CREATE TABLE `reimburse` (
  `id` int(11) NOT NULL,
  `no_faktur` varchar(155) DEFAULT NULL,
  `pegawai` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `return_penjualan`
--

CREATE TABLE `return_penjualan` (
  `id` int(11) NOT NULL,
  `no_retur` varchar(255) DEFAULT NULL,
  `invoice` int(11) NOT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `return_penjualan_item`
--

CREATE TABLE `return_penjualan_item` (
  `id` int(11) NOT NULL,
  `return_penjualan` int(11) NOT NULL,
  `invoice_product` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `stok_kategori` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `retur_order`
--

CREATE TABLE `retur_order` (
  `id` int(11) NOT NULL,
  `no_retur` varchar(155) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `retur_order_item`
--

CREATE TABLE `retur_order_item` (
  `id` int(11) NOT NULL,
  `retur_order` int(11) NOT NULL,
  `order_product` int(11) NOT NULL,
  `qty` int(11) DEFAULT NULL,
  `sub_total` int(11) DEFAULT NULL,
  `stok_kategori` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_rute`
--

CREATE TABLE `sales_rute` (
  `id` int(11) NOT NULL,
  `no_rute` varchar(155) DEFAULT NULL,
  `minggu` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_rute_customer`
--

CREATE TABLE `sales_rute_customer` (
  `id` int(11) NOT NULL,
  `sales_rute` int(11) NOT NULL,
  `pembeli` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_rute_hari`
--

CREATE TABLE `sales_rute_hari` (
  `id` int(11) NOT NULL,
  `sales_rute` int(11) NOT NULL,
  `hari` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sales_rute_user`
--

CREATE TABLE `sales_rute_user` (
  `id` int(11) NOT NULL,
  `sales_rute` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `satuan`
--

CREATE TABLE `satuan` (
  `id` int(11) NOT NULL,
  `nama_satuan` varchar(155) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `satuan`
--

INSERT INTO `satuan` (`id`, `nama_satuan`, `parent`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(906, 'M3', NULL, '2019-11-14', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `status_pembayaran`
--

CREATE TABLE `status_pembayaran` (
  `id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `status_pembelian`
--

CREATE TABLE `status_pembelian` (
  `id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_pembelian`
--

INSERT INTO `status_pembelian` (`id`, `status`) VALUES
(1, 'CASH'),
(2, 'KREDIT');

-- --------------------------------------------------------

--
-- Table structure for table `stok_kategori`
--

CREATE TABLE `stok_kategori` (
  `id` int(11) NOT NULL,
  `kategori` varchar(155) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stok_kategori`
--

INSERT INTO `stok_kategori` (`id`, `kategori`) VALUES
(1, 'GOOD STOCK'),
(2, 'BAD STOCK');

-- --------------------------------------------------------

--
-- Table structure for table `surat_jalan`
--

CREATE TABLE `surat_jalan` (
  `id` int(11) NOT NULL,
  `no_surat_jalan` varchar(155) DEFAULT NULL,
  `invoice` int(11) NOT NULL,
  `tanggal_faktur` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `surat_jalan_biaya`
--

CREATE TABLE `surat_jalan_biaya` (
  `id` int(11) NOT NULL,
  `surat_jalan` int(11) NOT NULL,
  `ket_biaya` varchar(255) DEFAULT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tagihan`
--

CREATE TABLE `tagihan` (
  `id` int(11) NOT NULL,
  `tagihan` varchar(255) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `threshold_mas`
--

CREATE TABLE `threshold_mas` (
  `id` int(11) NOT NULL,
  `jumlah` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `period_start` date DEFAULT NULL,
  `period_end` date DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `threshold_mas`
--

INSERT INTO `threshold_mas` (`id`, `jumlah`, `harga`, `period_start`, `period_end`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 85, 695000, '2019-08-31', '2020-08-19', '2019-11-06', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tipe_product`
--

CREATE TABLE `tipe_product` (
  `id` int(11) NOT NULL,
  `tipe` varchar(45) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipe_product`
--

INSERT INTO `tipe_product` (`id`, `tipe`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'BARANG', NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` text,
  `pegawai` int(11) DEFAULT NULL,
  `priveledge` int(11) NOT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `pegawai`, `priveledge`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'administrator', '6d89013342e5133da1c45e095caa26a6', NULL, 1, NULL, NULL, '2019-10-14 15:28:49', 1, 0),
(2, 'demo', 'fe01ce2a7fbac8fafaed7c982a04e229', NULL, 1, '2019-10-14', 1, NULL, NULL, 0),
(3, 'bejo', 'd4c01b1d3471a1b41ad485918d2298cb', 1, 3, '2019-10-24', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_sms_gateway`
--

CREATE TABLE `user_sms_gateway` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `url` text,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `vendor`
--

CREATE TABLE `vendor` (
  `id` int(11) NOT NULL,
  `nama_vendor` varchar(150) DEFAULT NULL,
  `vendor_category` int(11) NOT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `keterangan` varchar(150) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor`
--

INSERT INTO `vendor` (`id`, `nama_vendor`, `vendor_category`, `no_hp`, `keterangan`, `alamat`, `email`, `status`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(1, 'Vendor', 2, '085748233712', '-', 'Desa Bangsri 2 Rt2 Rw 4 Kecamatan Nglegok Kabupaten Blitar\nDesa Bangsri 2 Rt2 Rw 4 Kecamatan Nglegok Kabupaten Blitar', 'dodikitn@gmail.com', 1, '2019-11-09', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `vendor_category`
--

CREATE TABLE `vendor_category` (
  `id` int(11) NOT NULL,
  `kategori` varchar(45) DEFAULT NULL COMMENT 'COMPANY\nSUPPLIER'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vendor_category`
--

INSERT INTO `vendor_category` (`id`, `kategori`) VALUES
(1, 'COMPANY'),
(2, 'SUPPLIER');

-- --------------------------------------------------------

--
-- Table structure for table `zakat_mal`
--

CREATE TABLE `zakat_mal` (
  `id` int(11) NOT NULL,
  `threshold_mas` int(11) NOT NULL,
  `tot_kas_bank` int(15) NOT NULL,
  `tot_inventori` int(15) NOT NULL,
  `tot_hutang` int(15) NOT NULL,
  `tot_bruto` int(15) NOT NULL,
  `tot_net` int(15) NOT NULL,
  `zakat_threshold` int(15) NOT NULL,
  `total` int(11) DEFAULT NULL,
  `createddate` date DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `updateddate` datetime DEFAULT NULL,
  `updatedby` int(11) DEFAULT NULL,
  `deleted` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `zakat_mal`
--

INSERT INTO `zakat_mal` (`id`, `threshold_mas`, `tot_kas_bank`, `tot_inventori`, `tot_hutang`, `tot_bruto`, `tot_net`, `zakat_threshold`, `total`, `createddate`, `createdby`, `updateddate`, `updatedby`, `deleted`) VALUES
(0, 1, 0, 23800000, 0, 23800000, 23800000, 59075000, 0, '2019-11-10', 1, NULL, NULL, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ansuran`
--
ALTER TABLE `ansuran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coa`
--
ALTER TABLE `coa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coa_type`
--
ALTER TABLE `coa_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_notifikasi_tagihan`
--
ALTER TABLE `data_notifikasi_tagihan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_pengiriman`
--
ALTER TABLE `document_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dokumen_pengiriman_status`
--
ALTER TABLE `dokumen_pengiriman_status`
  ADD PRIMARY KEY (`id`,`document_pengiriman`),
  ADD KEY `fk_dokumen_pengiriman_status_document_pengiriman1_idx` (`document_pengiriman`);

--
-- Indexes for table `feature`
--
ALTER TABLE `feature`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general`
--
ALTER TABLE `general`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gudang`
--
ALTER TABLE `gudang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hari`
--
ALTER TABLE `hari`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `indetitas`
--
ALTER TABLE `indetitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `investor`
--
ALTER TABLE `investor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`,`jenis_pembayaran`,`pembeli`,`potongan`),
  ADD KEY `fk_invoice_pembeli1_idx` (`pembeli`),
  ADD KEY `fk_invoice_potongan1_idx` (`potongan`),
  ADD KEY `fk_invoice_jenis_pembayaran1_idx` (`jenis_pembayaran`);

--
-- Indexes for table `invoice_biaya`
--
ALTER TABLE `invoice_biaya`
  ADD PRIMARY KEY (`id`,`invoice`),
  ADD KEY `fk_invoice_biaya_invoice1_idx` (`invoice`);

--
-- Indexes for table `invoice_pot_product`
--
ALTER TABLE `invoice_pot_product`
  ADD PRIMARY KEY (`id`,`invoice_product`,`potongan`),
  ADD KEY `fk_invoice_pot_product_potongan1_idx` (`potongan`),
  ADD KEY `fk_invoice_pot_product_invoice_product1_idx` (`invoice_product`);

--
-- Indexes for table `invoice_print`
--
ALTER TABLE `invoice_print`
  ADD PRIMARY KEY (`id`,`user`,`invoice`),
  ADD KEY `fk_invoice_print_invoice1_idx` (`invoice`),
  ADD KEY `fk_invoice_print_user1_idx` (`user`);

--
-- Indexes for table `invoice_product`
--
ALTER TABLE `invoice_product`
  ADD PRIMARY KEY (`id`,`invoice`,`product_satuan`,`metode_bayar`,`pajak`),
  ADD KEY `fk_invoice_product_invoice1_idx` (`invoice`),
  ADD KEY `fk_invoice_product_product_satuan1_idx` (`product_satuan`),
  ADD KEY `fk_invoice_product_pajak1_idx` (`pajak`),
  ADD KEY `fk_invoice_product_metode_bayar1_idx` (`metode_bayar`);

--
-- Indexes for table `invoice_sisa`
--
ALTER TABLE `invoice_sisa`
  ADD PRIMARY KEY (`id`,`invoice`),
  ADD KEY `fk_invoice_sisa_invoice1_idx` (`invoice`);

--
-- Indexes for table `invoice_status`
--
ALTER TABLE `invoice_status`
  ADD PRIMARY KEY (`id`,`user`,`invoice`),
  ADD KEY `fk_invoice_status_invoice1_idx` (`invoice`),
  ADD KEY `fk_invoice_status_user1_idx` (`user`);

--
-- Indexes for table `jenis_akad`
--
ALTER TABLE `jenis_akad`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_notif_pengiriman`
--
ALTER TABLE `jenis_notif_pengiriman`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_pembayaran`
--
ALTER TABLE `jenis_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurnal`
--
ALTER TABLE `jurnal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jurnal_detail`
--
ALTER TABLE `jurnal_detail`
  ADD PRIMARY KEY (`id`,`jurnal`,`jurnal_struktur`),
  ADD KEY `fk_jurnal_detail_jurnal1_idx` (`jurnal`),
  ADD KEY `fk_jurnal_detail_jurnal_struktur1_idx` (`jurnal_struktur`);

--
-- Indexes for table `jurnal_struktur`
--
ALTER TABLE `jurnal_struktur`
  ADD PRIMARY KEY (`id`,`feature`,`coa`,`coa_type`),
  ADD KEY `fk_jurnal_struktur_coa_type1_idx` (`coa_type`),
  ADD KEY `fk_jurnal_struktur_coa1_idx` (`coa`),
  ADD KEY `fk_jurnal_struktur_feature1_idx` (`feature`);

--
-- Indexes for table `kas`
--
ALTER TABLE `kas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kasbon`
--
ALTER TABLE `kasbon`
  ADD PRIMARY KEY (`id`,`pegawai`),
  ADD KEY `fk_kasbon_pegawai1_idx` (`pegawai`);

--
-- Indexes for table `kasbon_paid`
--
ALTER TABLE `kasbon_paid`
  ADD PRIMARY KEY (`id`,`payroll`,`kasbon`),
  ADD KEY `fk_kasbon_paid_kasbon1_idx` (`kasbon`),
  ADD KEY `fk_kasbon_paid_payroll1_idx` (`payroll`);

--
-- Indexes for table `kasbon_payment`
--
ALTER TABLE `kasbon_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kasbon_payment_item`
--
ALTER TABLE `kasbon_payment_item`
  ADD PRIMARY KEY (`id`,`kasbon_payment`,`kasbon`),
  ADD KEY `fk_kasbon_payment_item_kasbon_payment1_idx` (`kasbon_payment`),
  ADD KEY `fk_kasbon_payment_item_kasbon1_idx` (`kasbon`);

--
-- Indexes for table `kasbon_sisa`
--
ALTER TABLE `kasbon_sisa`
  ADD PRIMARY KEY (`id`,`kasbon`),
  ADD KEY `fk_kasbon_sisa_kasbon1_idx` (`kasbon`);

--
-- Indexes for table `kasbon_status`
--
ALTER TABLE `kasbon_status`
  ADD PRIMARY KEY (`id`,`kasbon`),
  ADD KEY `fk_kasbon_status_kasbon1_idx` (`kasbon`);

--
-- Indexes for table `kategori_akad`
--
ALTER TABLE `kategori_akad`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_product`
--
ALTER TABLE `kategori_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kerja_sama_internal`
--
ALTER TABLE `kerja_sama_internal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_user_position`
--
ALTER TABLE `log_user_position`
  ADD PRIMARY KEY (`id`,`user`),
  ADD KEY `fk_log_user_position_user1_idx` (`user`);

--
-- Indexes for table `metode_bayar`
--
ALTER TABLE `metode_bayar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metode_pembayaran`
--
ALTER TABLE `metode_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `minggu`
--
ALTER TABLE `minggu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notif_has_jenis_pengiriman`
--
ALTER TABLE `notif_has_jenis_pengiriman`
  ADD PRIMARY KEY (`id`,`notif_jatuh_tempo`,`jenis_notif_pengiriman`),
  ADD KEY `fk_notif_has_jenis_pengiriman_notif_jatuh_tempo1_idx` (`notif_jatuh_tempo`),
  ADD KEY `fk_notif_has_jenis_pengiriman_jenis_notif_pengiriman1_idx` (`jenis_notif_pengiriman`);

--
-- Indexes for table `notif_jatuh_tempo`
--
ALTER TABLE `notif_jatuh_tempo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`,`pembeli`,`potongan`),
  ADD KEY `fk_order_pembeli1_idx` (`pembeli`),
  ADD KEY `fk_order_potongan1_idx` (`potongan`);

--
-- Indexes for table `order_pot_product`
--
ALTER TABLE `order_pot_product`
  ADD PRIMARY KEY (`id`,`order_product`,`potongan`),
  ADD KEY `fk_order_pot_product_order_product1_idx` (`order_product`),
  ADD KEY `fk_order_pot_product_potongan1_idx` (`potongan`);

--
-- Indexes for table `order_product`
--
ALTER TABLE `order_product`
  ADD PRIMARY KEY (`id`,`product_satuan`,`order`),
  ADD KEY `fk_order_product_order1_idx` (`order`),
  ADD KEY `fk_order_product_product_satuan1_idx` (`product_satuan`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`id`,`order`),
  ADD KEY `fk_order_status_order1_idx` (`order`);

--
-- Indexes for table `pajak`
--
ALTER TABLE `pajak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partial_payment`
--
ALTER TABLE `partial_payment`
  ADD PRIMARY KEY (`id`,`invoice`),
  ADD KEY `fk_partial_payment_invoice1_idx` (`invoice`);

--
-- Indexes for table `partial_payment_status`
--
ALTER TABLE `partial_payment_status`
  ADD PRIMARY KEY (`id`,`partial_payment`),
  ADD KEY `fk_partial_payment_status_partial_payment1_idx` (`partial_payment`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_item`
--
ALTER TABLE `payment_item`
  ADD PRIMARY KEY (`id`,`payment`,`invoice`),
  ADD KEY `fk_payment_item_payment1_idx` (`payment`),
  ADD KEY `fk_payment_item_invoice1_idx` (`invoice`);

--
-- Indexes for table `payroll`
--
ALTER TABLE `payroll`
  ADD PRIMARY KEY (`id`,`periode`,`pegawai`),
  ADD KEY `fk_payroll_pegawai1_idx` (`pegawai`),
  ADD KEY `fk_payroll_periode1_idx` (`periode`);

--
-- Indexes for table `payroll_category`
--
ALTER TABLE `payroll_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payroll_item`
--
ALTER TABLE `payroll_item`
  ADD PRIMARY KEY (`id`,`payroll`,`payroll_category`),
  ADD KEY `fk_payroll_item_payroll_category1_idx` (`payroll_category`),
  ADD KEY `fk_payroll_item_payroll1_idx` (`payroll`);

--
-- Indexes for table `payroll_workdays`
--
ALTER TABLE `payroll_workdays`
  ADD PRIMARY KEY (`id`,`payroll`),
  ADD KEY `fk_payroll_workdays_payroll1_idx` (`payroll`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pegawai_kontrak`
--
ALTER TABLE `pegawai_kontrak`
  ADD PRIMARY KEY (`id`,`pegawai`),
  ADD KEY `fk_pegawai_kontrak_pegawai1_idx` (`pegawai`);

--
-- Indexes for table `pembayaran_has_angsuran`
--
ALTER TABLE `pembayaran_has_angsuran`
  ADD PRIMARY KEY (`id`,`pembayaran_product`),
  ADD KEY `fk_pembayaran_has_angsuran_pembayaran_product1_idx` (`pembayaran_product`);

--
-- Indexes for table `pembayaran_has_cash`
--
ALTER TABLE `pembayaran_has_cash`
  ADD PRIMARY KEY (`id`,`pembayaran_product`),
  ADD KEY `fk_pembayaran_has_cash_pembayaran_rumah1_idx` (`pembayaran_product`);

--
-- Indexes for table `pembayaran_has_product`
--
ALTER TABLE `pembayaran_has_product`
  ADD PRIMARY KEY (`id`,`pembayaran_product`),
  ADD KEY `fk_pembayaran_has_angsuran_pembayaran1_idx` (`pembayaran_product`);

--
-- Indexes for table `pembayaran_lain_lain`
--
ALTER TABLE `pembayaran_lain_lain`
  ADD PRIMARY KEY (`id`,`jenis_pembayaran`),
  ADD KEY `fk_pembayaran_lain_lain_jenis_pembayaran1_idx` (`jenis_pembayaran`);

--
-- Indexes for table `pembayaran_piutang_non_usaha`
--
ALTER TABLE `pembayaran_piutang_non_usaha`
  ADD PRIMARY KEY (`id`,`jenis_pembayaran`),
  ADD KEY `fk_pembayaran_piutang_non_usaha_jenis_pembayaran1_idx` (`jenis_pembayaran`);

--
-- Indexes for table `pembayaran_product`
--
ALTER TABLE `pembayaran_product`
  ADD PRIMARY KEY (`id`,`pembeli_has_product`,`status_pembayaran`),
  ADD KEY `fk_pembayaran_pembeli_has_rumah1_idx` (`pembeli_has_product`),
  ADD KEY `fk_pembayaran_status_pembayaran1_idx` (`status_pembayaran`);

--
-- Indexes for table `pembayaran_tagihan`
--
ALTER TABLE `pembayaran_tagihan`
  ADD PRIMARY KEY (`id`,`tagihan`,`jenis_pembayaran`),
  ADD KEY `fk_pembayaran_tagihan_tagihan1_idx` (`tagihan`),
  ADD KEY `fk_pembayaran_tagihan_jenis_pembayaran1_idx` (`jenis_pembayaran`);

--
-- Indexes for table `pembayaran_vendor`
--
ALTER TABLE `pembayaran_vendor`
  ADD PRIMARY KEY (`id`,`vendor`,`jenis_pembayaran`),
  ADD KEY `fk_pembayaran_vendor_vendor1_idx` (`vendor`),
  ADD KEY `fk_pembayaran_vendor_jenis_pembayaran1_idx` (`jenis_pembayaran`);

--
-- Indexes for table `pembeli`
--
ALTER TABLE `pembeli`
  ADD PRIMARY KEY (`id`,`pembeli_kategori`),
  ADD KEY `fk_pembeli_pembeli_kategori1_idx` (`pembeli_kategori`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembeli_has_angsuran`
--
ALTER TABLE `pembeli_has_angsuran`
  ADD PRIMARY KEY (`id`,`pembeli_has_product`,`product_has_harga_angsuran`),
  ADD KEY `fk_pembeli_has_angsuran_pembeli_has_rumah1_idx` (`pembeli_has_product`),
  ADD KEY `fk_pembeli_has_angsuran_rumah_has_harga_angsuran1_idx` (`product_has_harga_angsuran`);

--
-- Indexes for table `pembeli_has_identitas`
--
ALTER TABLE `pembeli_has_identitas`
  ADD PRIMARY KEY (`id`,`pembeli`,`indetitas`),
  ADD KEY `fk_kreditur_has_identitas_kreditur1_idx` (`pembeli`),
  ADD KEY `fk_kreditur_has_identitas_indetitas1_idx` (`indetitas`);

--
-- Indexes for table `pembeli_has_persyaratan`
--
ALTER TABLE `pembeli_has_persyaratan`
  ADD PRIMARY KEY (`id`,`pembelian`,`persyaratan_has_berkas`),
  ADD KEY `fk_pembeli_has_persyaratan_persyaratan_has_berkas1_idx` (`persyaratan_has_berkas`),
  ADD KEY `fk_pembeli_has_persyaratan_pembelian1_idx` (`pembelian`);

--
-- Indexes for table `pembeli_has_product`
--
ALTER TABLE `pembeli_has_product`
  ADD PRIMARY KEY (`id`,`pembelian`,`pembeli`,`product`,`status_pembelian`),
  ADD KEY `fk_kreditur_has_rumah_kreditur1_idx` (`pembeli`),
  ADD KEY `fk_kreditur_has_rumah_rumah1_idx` (`product`),
  ADD KEY `fk_pembeli_has_rumah_status_pembelian1_idx` (`status_pembelian`),
  ADD KEY `fk_pembeli_has_rumah_pembelian1_idx` (`pembelian`);

--
-- Indexes for table `pembeli_kategori`
--
ALTER TABLE `pembeli_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `persyaratan`
--
ALTER TABLE `persyaratan`
  ADD PRIMARY KEY (`id`,`kategori_akad`,`jenis_akad`),
  ADD KEY `fk_persyaratan_kategori_akad1_idx` (`kategori_akad`),
  ADD KEY `fk_persyaratan_jenis_akad1_idx` (`jenis_akad`);

--
-- Indexes for table `persyaratan_has_berkas`
--
ALTER TABLE `persyaratan_has_berkas`
  ADD PRIMARY KEY (`id`,`persyaratan`),
  ADD KEY `fk_persyaratan_has_berkas_persyaratan1_idx` (`persyaratan`);

--
-- Indexes for table `potongan`
--
ALTER TABLE `potongan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `priveledge`
--
ALTER TABLE `priveledge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procurement`
--
ALTER TABLE `procurement`
  ADD PRIMARY KEY (`id`,`vendor`),
  ADD KEY `fk_procurement_vendor1_idx` (`vendor`);

--
-- Indexes for table `procurement_item`
--
ALTER TABLE `procurement_item`
  ADD PRIMARY KEY (`id`,`procurement`,`product`,`satuan`),
  ADD KEY `fk_procurement_item_product1_idx` (`product`),
  ADD KEY `fk_procurement_item_satuan1_idx` (`satuan`),
  ADD KEY `fk_procurement_item_procurement1_idx` (`procurement`);

--
-- Indexes for table `procurement_retur`
--
ALTER TABLE `procurement_retur`
  ADD PRIMARY KEY (`id`,`procurement`),
  ADD KEY `fk_procurement_retur_procurement1_idx` (`procurement`);

--
-- Indexes for table `procurement_retur_item`
--
ALTER TABLE `procurement_retur_item`
  ADD PRIMARY KEY (`id`,`procurement_item`,`procurement_retur`),
  ADD KEY `fk_procurement_return_item_procurement_retur1_idx` (`procurement_retur`),
  ADD KEY `fk_procurement_return_item_procurement_item1_idx` (`procurement_item`);

--
-- Indexes for table `procurement_status`
--
ALTER TABLE `procurement_status`
  ADD PRIMARY KEY (`id`,`procurement`),
  ADD KEY `fk_procurement_status_procurement1_idx` (`procurement`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`,`tipe_product`,`kategori_product`),
  ADD KEY `fk_rumah_kategori_rumah1_idx` (`kategori_product`),
  ADD KEY `fk_rumah_tipe_rumah1_idx` (`tipe_product`);

--
-- Indexes for table `product_distriburst`
--
ALTER TABLE `product_distriburst`
  ADD PRIMARY KEY (`id`,`pembeli`,`product`),
  ADD KEY `fk_product_distriburst_product1_idx` (`product`),
  ADD KEY `fk_product_distriburst_pembeli1_idx` (`pembeli`);

--
-- Indexes for table `product_has_foto`
--
ALTER TABLE `product_has_foto`
  ADD PRIMARY KEY (`id`,`product`),
  ADD KEY `fk_rumah_has_foto_rumah1_idx` (`product`);

--
-- Indexes for table `product_has_harga_angsuran`
--
ALTER TABLE `product_has_harga_angsuran`
  ADD PRIMARY KEY (`id`,`product`,`ansuran`),
  ADD KEY `fk_rumah_has_harga_angsuran_rumah1_idx` (`product`),
  ADD KEY `fk_rumah_has_harga_angsuran_ansuran1_idx` (`ansuran`);

--
-- Indexes for table `product_has_harga_jual_pokok`
--
ALTER TABLE `product_has_harga_jual_pokok`
  ADD PRIMARY KEY (`id`,`product`),
  ADD KEY `fk_rumah_has_harga_jual_rumah1_idx` (`product`);

--
-- Indexes for table `product_has_harga_jual_tunai`
--
ALTER TABLE `product_has_harga_jual_tunai`
  ADD PRIMARY KEY (`id`,`product`),
  ADD KEY `fk_rumah_has_harga_jual_kredit_rumah1_idx` (`product`);

--
-- Indexes for table `product_kirim`
--
ALTER TABLE `product_kirim`
  ADD PRIMARY KEY (`id`,`product`,`document_pengiriman`),
  ADD KEY `fk_product_kirim_document_pengiriman1_idx` (`document_pengiriman`),
  ADD KEY `fk_product_kirim_product1_idx` (`product`);

--
-- Indexes for table `product_kirim_status`
--
ALTER TABLE `product_kirim_status`
  ADD PRIMARY KEY (`id`,`product_kirim`),
  ADD KEY `fk_product_kirim_status_product_kirim1_idx` (`product_kirim`);

--
-- Indexes for table `product_log_stock`
--
ALTER TABLE `product_log_stock`
  ADD PRIMARY KEY (`id`,`product_satuan`),
  ADD KEY `fk_product_log_stock_product_satuan1_idx` (`product_satuan`);

--
-- Indexes for table `product_satuan`
--
ALTER TABLE `product_satuan`
  ADD PRIMARY KEY (`id`,`product`,`satuan`),
  ADD KEY `fk_product_satuan_product1_idx` (`product`),
  ADD KEY `fk_product_satuan_satuan1_idx` (`satuan`);

--
-- Indexes for table `product_stock`
--
ALTER TABLE `product_stock`
  ADD PRIMARY KEY (`id`,`product_satuan`),
  ADD KEY `fk_product_stock_product_satuan1_idx` (`product_satuan`);

--
-- Indexes for table `product_surat_jalan`
--
ALTER TABLE `product_surat_jalan`
  ADD PRIMARY KEY (`id`,`document_pengiriman`),
  ADD KEY `fk_product_surat_jalan_document_pengiriman1_idx` (`document_pengiriman`);

--
-- Indexes for table `product_vendor`
--
ALTER TABLE `product_vendor`
  ADD PRIMARY KEY (`id`,`product`,`vendor`),
  ADD KEY `fk_product_vendor_product1_idx` (`product`),
  ADD KEY `fk_product_vendor_vendor1_idx` (`vendor`);

--
-- Indexes for table `rak`
--
ALTER TABLE `rak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reimburse`
--
ALTER TABLE `reimburse`
  ADD PRIMARY KEY (`id`,`pegawai`),
  ADD KEY `fk_reimburse_pegawai1_idx` (`pegawai`);

--
-- Indexes for table `return_penjualan`
--
ALTER TABLE `return_penjualan`
  ADD PRIMARY KEY (`id`,`invoice`),
  ADD KEY `fk_return_penjualan_invoice1_idx` (`invoice`);

--
-- Indexes for table `return_penjualan_item`
--
ALTER TABLE `return_penjualan_item`
  ADD PRIMARY KEY (`id`,`return_penjualan`,`invoice_product`),
  ADD KEY `fk_return_penjualan_item_return_penjualan1_idx` (`return_penjualan`),
  ADD KEY `fk_return_penjualan_item_invoice_product1_idx` (`invoice_product`);

--
-- Indexes for table `retur_order`
--
ALTER TABLE `retur_order`
  ADD PRIMARY KEY (`id`,`order`),
  ADD KEY `fk_retur_order_order1_idx` (`order`);

--
-- Indexes for table `retur_order_item`
--
ALTER TABLE `retur_order_item`
  ADD PRIMARY KEY (`id`,`retur_order`,`order_product`),
  ADD KEY `fk_retur_order_item_order_product1_idx` (`order_product`),
  ADD KEY `fk_retur_order_item_retur_order1_idx` (`retur_order`);

--
-- Indexes for table `sales_rute`
--
ALTER TABLE `sales_rute`
  ADD PRIMARY KEY (`id`,`minggu`),
  ADD KEY `fk_sales_rute_minggu1_idx` (`minggu`);

--
-- Indexes for table `sales_rute_customer`
--
ALTER TABLE `sales_rute_customer`
  ADD PRIMARY KEY (`id`,`sales_rute`,`pembeli`),
  ADD KEY `fk_sales_rute_customer_pembeli1_idx` (`pembeli`),
  ADD KEY `fk_sales_rute_customer_sales_rute1_idx` (`sales_rute`);

--
-- Indexes for table `sales_rute_hari`
--
ALTER TABLE `sales_rute_hari`
  ADD PRIMARY KEY (`id`,`sales_rute`,`hari`),
  ADD KEY `fk_sales_rute_hari_sales_rute1_idx` (`sales_rute`),
  ADD KEY `fk_sales_rute_hari_hari1_idx` (`hari`);

--
-- Indexes for table `sales_rute_user`
--
ALTER TABLE `sales_rute_user`
  ADD PRIMARY KEY (`id`,`sales_rute`,`user`),
  ADD KEY `fk_sales_rute_user_user1_idx` (`user`),
  ADD KEY `fk_sales_rute_user_sales_rute1_idx` (`sales_rute`);

--
-- Indexes for table `satuan`
--
ALTER TABLE `satuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_pembayaran`
--
ALTER TABLE `status_pembayaran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_pembelian`
--
ALTER TABLE `status_pembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stok_kategori`
--
ALTER TABLE `stok_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surat_jalan`
--
ALTER TABLE `surat_jalan`
  ADD PRIMARY KEY (`id`,`invoice`),
  ADD KEY `fk_surat_jalan_invoice1_idx` (`invoice`);

--
-- Indexes for table `surat_jalan_biaya`
--
ALTER TABLE `surat_jalan_biaya`
  ADD PRIMARY KEY (`id`,`surat_jalan`),
  ADD KEY `fk_surat_jalan_biaya_surat_jalan1_idx` (`surat_jalan`);

--
-- Indexes for table `tagihan`
--
ALTER TABLE `tagihan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `threshold_mas`
--
ALTER TABLE `threshold_mas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tipe_product`
--
ALTER TABLE `tipe_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`,`priveledge`),
  ADD KEY `fk_user_priveledge_idx` (`priveledge`);

--
-- Indexes for table `user_sms_gateway`
--
ALTER TABLE `user_sms_gateway`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendor`
--
ALTER TABLE `vendor`
  ADD PRIMARY KEY (`id`,`vendor_category`),
  ADD KEY `fk_vendor_vendor_category1_idx` (`vendor_category`);

--
-- Indexes for table `vendor_category`
--
ALTER TABLE `vendor_category`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ansuran`
--
ALTER TABLE `ansuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `coa`
--
ALTER TABLE `coa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `coa_type`
--
ALTER TABLE `coa_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_notifikasi_tagihan`
--
ALTER TABLE `data_notifikasi_tagihan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `document_pengiriman`
--
ALTER TABLE `document_pengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `dokumen_pengiriman_status`
--
ALTER TABLE `dokumen_pengiriman_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feature`
--
ALTER TABLE `feature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `general`
--
ALTER TABLE `general`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gudang`
--
ALTER TABLE `gudang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `hari`
--
ALTER TABLE `hari`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `indetitas`
--
ALTER TABLE `indetitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `investor`
--
ALTER TABLE `investor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `invoice_biaya`
--
ALTER TABLE `invoice_biaya`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `invoice_pot_product`
--
ALTER TABLE `invoice_pot_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `invoice_print`
--
ALTER TABLE `invoice_print`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `invoice_product`
--
ALTER TABLE `invoice_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `invoice_sisa`
--
ALTER TABLE `invoice_sisa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `invoice_status`
--
ALTER TABLE `invoice_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `jenis_akad`
--
ALTER TABLE `jenis_akad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `jenis_notif_pengiriman`
--
ALTER TABLE `jenis_notif_pengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `jenis_pembayaran`
--
ALTER TABLE `jenis_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jurnal`
--
ALTER TABLE `jurnal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `jurnal_detail`
--
ALTER TABLE `jurnal_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `jurnal_struktur`
--
ALTER TABLE `jurnal_struktur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `kas`
--
ALTER TABLE `kas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kasbon`
--
ALTER TABLE `kasbon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kasbon_paid`
--
ALTER TABLE `kasbon_paid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kasbon_payment`
--
ALTER TABLE `kasbon_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kasbon_payment_item`
--
ALTER TABLE `kasbon_payment_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kasbon_sisa`
--
ALTER TABLE `kasbon_sisa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kasbon_status`
--
ALTER TABLE `kasbon_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kategori_akad`
--
ALTER TABLE `kategori_akad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kategori_product`
--
ALTER TABLE `kategori_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kerja_sama_internal`
--
ALTER TABLE `kerja_sama_internal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `log_user_position`
--
ALTER TABLE `log_user_position`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `metode_bayar`
--
ALTER TABLE `metode_bayar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `metode_pembayaran`
--
ALTER TABLE `metode_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `minggu`
--
ALTER TABLE `minggu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `notif_has_jenis_pengiriman`
--
ALTER TABLE `notif_has_jenis_pengiriman`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `notif_jatuh_tempo`
--
ALTER TABLE `notif_jatuh_tempo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_pot_product`
--
ALTER TABLE `order_pot_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_product`
--
ALTER TABLE `order_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_status`
--
ALTER TABLE `order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pajak`
--
ALTER TABLE `pajak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `partial_payment`
--
ALTER TABLE `partial_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `partial_payment_status`
--
ALTER TABLE `partial_payment_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payment_item`
--
ALTER TABLE `payment_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payroll`
--
ALTER TABLE `payroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `payroll_category`
--
ALTER TABLE `payroll_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `payroll_item`
--
ALTER TABLE `payroll_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `payroll_workdays`
--
ALTER TABLE `payroll_workdays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pegawai_kontrak`
--
ALTER TABLE `pegawai_kontrak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_has_angsuran`
--
ALTER TABLE `pembayaran_has_angsuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_has_cash`
--
ALTER TABLE `pembayaran_has_cash`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_has_product`
--
ALTER TABLE `pembayaran_has_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_lain_lain`
--
ALTER TABLE `pembayaran_lain_lain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_piutang_non_usaha`
--
ALTER TABLE `pembayaran_piutang_non_usaha`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pembayaran_product`
--
ALTER TABLE `pembayaran_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_tagihan`
--
ALTER TABLE `pembayaran_tagihan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembayaran_vendor`
--
ALTER TABLE `pembayaran_vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembeli`
--
ALTER TABLE `pembeli`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembeli_has_angsuran`
--
ALTER TABLE `pembeli_has_angsuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembeli_has_identitas`
--
ALTER TABLE `pembeli_has_identitas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembeli_has_persyaratan`
--
ALTER TABLE `pembeli_has_persyaratan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembeli_has_product`
--
ALTER TABLE `pembeli_has_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pembeli_kategori`
--
ALTER TABLE `pembeli_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `periode`
--
ALTER TABLE `periode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `persyaratan`
--
ALTER TABLE `persyaratan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `persyaratan_has_berkas`
--
ALTER TABLE `persyaratan_has_berkas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `potongan`
--
ALTER TABLE `potongan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `priveledge`
--
ALTER TABLE `priveledge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `procurement`
--
ALTER TABLE `procurement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `procurement_item`
--
ALTER TABLE `procurement_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `procurement_retur`
--
ALTER TABLE `procurement_retur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `procurement_retur_item`
--
ALTER TABLE `procurement_retur_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `procurement_status`
--
ALTER TABLE `procurement_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2841;

--
-- AUTO_INCREMENT for table `product_distriburst`
--
ALTER TABLE `product_distriburst`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_has_foto`
--
ALTER TABLE `product_has_foto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_has_harga_angsuran`
--
ALTER TABLE `product_has_harga_angsuran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_has_harga_jual_pokok`
--
ALTER TABLE `product_has_harga_jual_pokok`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_has_harga_jual_tunai`
--
ALTER TABLE `product_has_harga_jual_tunai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_kirim`
--
ALTER TABLE `product_kirim`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_kirim_status`
--
ALTER TABLE `product_kirim_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_log_stock`
--
ALTER TABLE `product_log_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `product_satuan`
--
ALTER TABLE `product_satuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5372;

--
-- AUTO_INCREMENT for table `product_stock`
--
ALTER TABLE `product_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6614;

--
-- AUTO_INCREMENT for table `product_surat_jalan`
--
ALTER TABLE `product_surat_jalan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_vendor`
--
ALTER TABLE `product_vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rak`
--
ALTER TABLE `rak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `reimburse`
--
ALTER TABLE `reimburse`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `return_penjualan`
--
ALTER TABLE `return_penjualan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `return_penjualan_item`
--
ALTER TABLE `return_penjualan_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `retur_order`
--
ALTER TABLE `retur_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `retur_order_item`
--
ALTER TABLE `retur_order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_rute`
--
ALTER TABLE `sales_rute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_rute_customer`
--
ALTER TABLE `sales_rute_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_rute_hari`
--
ALTER TABLE `sales_rute_hari`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sales_rute_user`
--
ALTER TABLE `sales_rute_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `satuan`
--
ALTER TABLE `satuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=907;

--
-- AUTO_INCREMENT for table `status_pembayaran`
--
ALTER TABLE `status_pembayaran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `status_pembelian`
--
ALTER TABLE `status_pembelian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `stok_kategori`
--
ALTER TABLE `stok_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `surat_jalan`
--
ALTER TABLE `surat_jalan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `surat_jalan_biaya`
--
ALTER TABLE `surat_jalan_biaya`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tagihan`
--
ALTER TABLE `tagihan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `threshold_mas`
--
ALTER TABLE `threshold_mas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tipe_product`
--
ALTER TABLE `tipe_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user_sms_gateway`
--
ALTER TABLE `user_sms_gateway`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `vendor`
--
ALTER TABLE `vendor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `vendor_category`
--
ALTER TABLE `vendor_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dokumen_pengiriman_status`
--
ALTER TABLE `dokumen_pengiriman_status`
  ADD CONSTRAINT `fk_dokumen_pengiriman_status_document_pengiriman1` FOREIGN KEY (`document_pengiriman`) REFERENCES `document_pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `fk_invoice_jenis_pembayaran1` FOREIGN KEY (`jenis_pembayaran`) REFERENCES `jenis_pembayaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_pembeli1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_potongan1` FOREIGN KEY (`potongan`) REFERENCES `potongan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_pot_product`
--
ALTER TABLE `invoice_pot_product`
  ADD CONSTRAINT `fk_invoice_pot_product_invoice_product1` FOREIGN KEY (`invoice_product`) REFERENCES `invoice_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_pot_product_potongan1` FOREIGN KEY (`potongan`) REFERENCES `potongan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_print`
--
ALTER TABLE `invoice_print`
  ADD CONSTRAINT `fk_invoice_print_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_print_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_product`
--
ALTER TABLE `invoice_product`
  ADD CONSTRAINT `fk_invoice_product_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_product_metode_bayar1` FOREIGN KEY (`metode_bayar`) REFERENCES `metode_bayar` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_product_pajak1` FOREIGN KEY (`pajak`) REFERENCES `pajak` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_product_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_sisa`
--
ALTER TABLE `invoice_sisa`
  ADD CONSTRAINT `fk_invoice_sisa_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `invoice_status`
--
ALTER TABLE `invoice_status`
  ADD CONSTRAINT `fk_invoice_status_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_invoice_status_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `jurnal_detail`
--
ALTER TABLE `jurnal_detail`
  ADD CONSTRAINT `fk_jurnal_detail_jurnal1` FOREIGN KEY (`jurnal`) REFERENCES `jurnal` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_jurnal_detail_jurnal_struktur1` FOREIGN KEY (`jurnal_struktur`) REFERENCES `jurnal_struktur` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `jurnal_struktur`
--
ALTER TABLE `jurnal_struktur`
  ADD CONSTRAINT `fk_jurnal_struktur_coa1` FOREIGN KEY (`coa`) REFERENCES `coa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_jurnal_struktur_coa_type1` FOREIGN KEY (`coa_type`) REFERENCES `coa_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_jurnal_struktur_feature1` FOREIGN KEY (`feature`) REFERENCES `feature` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kasbon`
--
ALTER TABLE `kasbon`
  ADD CONSTRAINT `fk_kasbon_pegawai1` FOREIGN KEY (`pegawai`) REFERENCES `pegawai` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kasbon_paid`
--
ALTER TABLE `kasbon_paid`
  ADD CONSTRAINT `fk_kasbon_paid_kasbon1` FOREIGN KEY (`kasbon`) REFERENCES `kasbon` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kasbon_paid_payroll1` FOREIGN KEY (`payroll`) REFERENCES `payroll` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kasbon_payment_item`
--
ALTER TABLE `kasbon_payment_item`
  ADD CONSTRAINT `fk_kasbon_payment_item_kasbon1` FOREIGN KEY (`kasbon`) REFERENCES `kasbon` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kasbon_payment_item_kasbon_payment1` FOREIGN KEY (`kasbon_payment`) REFERENCES `kasbon_payment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kasbon_sisa`
--
ALTER TABLE `kasbon_sisa`
  ADD CONSTRAINT `fk_kasbon_sisa_kasbon1` FOREIGN KEY (`kasbon`) REFERENCES `kasbon` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `kasbon_status`
--
ALTER TABLE `kasbon_status`
  ADD CONSTRAINT `fk_kasbon_status_kasbon1` FOREIGN KEY (`kasbon`) REFERENCES `kasbon` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `log_user_position`
--
ALTER TABLE `log_user_position`
  ADD CONSTRAINT `fk_log_user_position_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `notif_has_jenis_pengiriman`
--
ALTER TABLE `notif_has_jenis_pengiriman`
  ADD CONSTRAINT `fk_notif_has_jenis_pengiriman_jenis_notif_pengiriman1` FOREIGN KEY (`jenis_notif_pengiriman`) REFERENCES `jenis_notif_pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_notif_has_jenis_pengiriman_notif_jatuh_tempo1` FOREIGN KEY (`notif_jatuh_tempo`) REFERENCES `notif_jatuh_tempo` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `fk_order_pembeli1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_potongan1` FOREIGN KEY (`potongan`) REFERENCES `potongan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_pot_product`
--
ALTER TABLE `order_pot_product`
  ADD CONSTRAINT `fk_order_pot_product_order_product1` FOREIGN KEY (`order_product`) REFERENCES `order_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_pot_product_potongan1` FOREIGN KEY (`potongan`) REFERENCES `potongan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_product`
--
ALTER TABLE `order_product`
  ADD CONSTRAINT `fk_order_product_order1` FOREIGN KEY (`order`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_product_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order_status`
--
ALTER TABLE `order_status`
  ADD CONSTRAINT `fk_order_status_order1` FOREIGN KEY (`order`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `partial_payment`
--
ALTER TABLE `partial_payment`
  ADD CONSTRAINT `fk_partial_payment_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `partial_payment_status`
--
ALTER TABLE `partial_payment_status`
  ADD CONSTRAINT `fk_partial_payment_status_partial_payment1` FOREIGN KEY (`partial_payment`) REFERENCES `partial_payment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payment_item`
--
ALTER TABLE `payment_item`
  ADD CONSTRAINT `fk_payment_item_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_payment_item_payment1` FOREIGN KEY (`payment`) REFERENCES `payment` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payroll_item`
--
ALTER TABLE `payroll_item`
  ADD CONSTRAINT `fk_payroll_item_payroll1` FOREIGN KEY (`payroll`) REFERENCES `payroll` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_payroll_item_payroll_category1` FOREIGN KEY (`payroll_category`) REFERENCES `payroll_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payroll_workdays`
--
ALTER TABLE `payroll_workdays`
  ADD CONSTRAINT `fk_payroll_workdays_payroll1` FOREIGN KEY (`payroll`) REFERENCES `payroll` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pegawai_kontrak`
--
ALTER TABLE `pegawai_kontrak`
  ADD CONSTRAINT `fk_pegawai_kontrak_pegawai1` FOREIGN KEY (`pegawai`) REFERENCES `pegawai` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_has_angsuran`
--
ALTER TABLE `pembayaran_has_angsuran`
  ADD CONSTRAINT `fk_pembayaran_has_angsuran_pembayaran_product1` FOREIGN KEY (`pembayaran_product`) REFERENCES `pembayaran_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_has_cash`
--
ALTER TABLE `pembayaran_has_cash`
  ADD CONSTRAINT `fk_pembayaran_has_cash_pembayaran_rumah1` FOREIGN KEY (`pembayaran_product`) REFERENCES `pembayaran_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_has_product`
--
ALTER TABLE `pembayaran_has_product`
  ADD CONSTRAINT `fk_pembayaran_has_angsuran_pembayaran1` FOREIGN KEY (`pembayaran_product`) REFERENCES `pembayaran_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_lain_lain`
--
ALTER TABLE `pembayaran_lain_lain`
  ADD CONSTRAINT `fk_pembayaran_lain_lain_jenis_pembayaran1` FOREIGN KEY (`jenis_pembayaran`) REFERENCES `jenis_pembayaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_product`
--
ALTER TABLE `pembayaran_product`
  ADD CONSTRAINT `fk_pembayaran_pembeli_has_rumah1` FOREIGN KEY (`pembeli_has_product`) REFERENCES `pembeli_has_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembayaran_status_pembayaran1` FOREIGN KEY (`status_pembayaran`) REFERENCES `status_pembayaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_tagihan`
--
ALTER TABLE `pembayaran_tagihan`
  ADD CONSTRAINT `fk_pembayaran_tagihan_jenis_pembayaran1` FOREIGN KEY (`jenis_pembayaran`) REFERENCES `jenis_pembayaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembayaran_tagihan_tagihan1` FOREIGN KEY (`tagihan`) REFERENCES `tagihan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembayaran_vendor`
--
ALTER TABLE `pembayaran_vendor`
  ADD CONSTRAINT `fk_pembayaran_vendor_jenis_pembayaran1` FOREIGN KEY (`jenis_pembayaran`) REFERENCES `jenis_pembayaran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembayaran_vendor_vendor1` FOREIGN KEY (`vendor`) REFERENCES `vendor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli`
--
ALTER TABLE `pembeli`
  ADD CONSTRAINT `fk_pembeli_pembeli_kategori1` FOREIGN KEY (`pembeli_kategori`) REFERENCES `pembeli_kategori` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli_has_angsuran`
--
ALTER TABLE `pembeli_has_angsuran`
  ADD CONSTRAINT `fk_pembeli_has_angsuran_pembeli_has_rumah1` FOREIGN KEY (`pembeli_has_product`) REFERENCES `pembeli_has_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembeli_has_angsuran_rumah_has_harga_angsuran1` FOREIGN KEY (`product_has_harga_angsuran`) REFERENCES `product_has_harga_angsuran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli_has_identitas`
--
ALTER TABLE `pembeli_has_identitas`
  ADD CONSTRAINT `fk_kreditur_has_identitas_indetitas1` FOREIGN KEY (`indetitas`) REFERENCES `indetitas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kreditur_has_identitas_kreditur1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli_has_persyaratan`
--
ALTER TABLE `pembeli_has_persyaratan`
  ADD CONSTRAINT `fk_pembeli_has_persyaratan_pembelian1` FOREIGN KEY (`pembelian`) REFERENCES `pembelian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembeli_has_persyaratan_persyaratan_has_berkas1` FOREIGN KEY (`persyaratan_has_berkas`) REFERENCES `persyaratan_has_berkas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pembeli_has_product`
--
ALTER TABLE `pembeli_has_product`
  ADD CONSTRAINT `fk_kreditur_has_rumah_kreditur1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_kreditur_has_rumah_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembeli_has_rumah_pembelian1` FOREIGN KEY (`pembelian`) REFERENCES `pembelian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pembeli_has_rumah_status_pembelian1` FOREIGN KEY (`status_pembelian`) REFERENCES `status_pembelian` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `persyaratan`
--
ALTER TABLE `persyaratan`
  ADD CONSTRAINT `fk_persyaratan_jenis_akad1` FOREIGN KEY (`jenis_akad`) REFERENCES `jenis_akad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_persyaratan_kategori_akad1` FOREIGN KEY (`kategori_akad`) REFERENCES `kategori_akad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `persyaratan_has_berkas`
--
ALTER TABLE `persyaratan_has_berkas`
  ADD CONSTRAINT `fk_persyaratan_has_berkas_persyaratan1` FOREIGN KEY (`persyaratan`) REFERENCES `persyaratan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `procurement`
--
ALTER TABLE `procurement`
  ADD CONSTRAINT `fk_procurement_vendor1` FOREIGN KEY (`vendor`) REFERENCES `vendor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `procurement_item`
--
ALTER TABLE `procurement_item`
  ADD CONSTRAINT `fk_procurement_item_procurement1` FOREIGN KEY (`procurement`) REFERENCES `procurement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_procurement_item_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_procurement_item_satuan1` FOREIGN KEY (`satuan`) REFERENCES `satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `procurement_retur`
--
ALTER TABLE `procurement_retur`
  ADD CONSTRAINT `fk_procurement_retur_procurement1` FOREIGN KEY (`procurement`) REFERENCES `procurement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `procurement_retur_item`
--
ALTER TABLE `procurement_retur_item`
  ADD CONSTRAINT `fk_procurement_return_item_procurement_item1` FOREIGN KEY (`procurement_item`) REFERENCES `procurement_item` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_procurement_return_item_procurement_retur1` FOREIGN KEY (`procurement_retur`) REFERENCES `procurement_retur` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `procurement_status`
--
ALTER TABLE `procurement_status`
  ADD CONSTRAINT `fk_procurement_status_procurement1` FOREIGN KEY (`procurement`) REFERENCES `procurement` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_rumah_kategori_rumah1` FOREIGN KEY (`kategori_product`) REFERENCES `kategori_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rumah_tipe_rumah1` FOREIGN KEY (`tipe_product`) REFERENCES `tipe_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_distriburst`
--
ALTER TABLE `product_distriburst`
  ADD CONSTRAINT `fk_product_distriburst_pembeli1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_distriburst_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_has_foto`
--
ALTER TABLE `product_has_foto`
  ADD CONSTRAINT `fk_rumah_has_foto_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_has_harga_angsuran`
--
ALTER TABLE `product_has_harga_angsuran`
  ADD CONSTRAINT `fk_rumah_has_harga_angsuran_ansuran1` FOREIGN KEY (`ansuran`) REFERENCES `ansuran` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_rumah_has_harga_angsuran_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_has_harga_jual_pokok`
--
ALTER TABLE `product_has_harga_jual_pokok`
  ADD CONSTRAINT `fk_rumah_has_harga_jual_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_has_harga_jual_tunai`
--
ALTER TABLE `product_has_harga_jual_tunai`
  ADD CONSTRAINT `fk_rumah_has_harga_jual_kredit_rumah1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_kirim`
--
ALTER TABLE `product_kirim`
  ADD CONSTRAINT `fk_product_kirim_document_pengiriman1` FOREIGN KEY (`document_pengiriman`) REFERENCES `document_pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_kirim_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_kirim_status`
--
ALTER TABLE `product_kirim_status`
  ADD CONSTRAINT `fk_product_kirim_status_product_kirim1` FOREIGN KEY (`product_kirim`) REFERENCES `product_kirim` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_log_stock`
--
ALTER TABLE `product_log_stock`
  ADD CONSTRAINT `fk_product_log_stock_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_satuan`
--
ALTER TABLE `product_satuan`
  ADD CONSTRAINT `fk_product_satuan_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_satuan_satuan1` FOREIGN KEY (`satuan`) REFERENCES `satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_stock`
--
ALTER TABLE `product_stock`
  ADD CONSTRAINT `fk_product_stock_product_satuan1` FOREIGN KEY (`product_satuan`) REFERENCES `product_satuan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_surat_jalan`
--
ALTER TABLE `product_surat_jalan`
  ADD CONSTRAINT `fk_product_surat_jalan_document_pengiriman1` FOREIGN KEY (`document_pengiriman`) REFERENCES `document_pengiriman` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `product_vendor`
--
ALTER TABLE `product_vendor`
  ADD CONSTRAINT `fk_product_vendor_product1` FOREIGN KEY (`product`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_vendor_vendor1` FOREIGN KEY (`vendor`) REFERENCES `vendor` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `reimburse`
--
ALTER TABLE `reimburse`
  ADD CONSTRAINT `fk_reimburse_pegawai1` FOREIGN KEY (`pegawai`) REFERENCES `pegawai` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `return_penjualan`
--
ALTER TABLE `return_penjualan`
  ADD CONSTRAINT `fk_return_penjualan_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `return_penjualan_item`
--
ALTER TABLE `return_penjualan_item`
  ADD CONSTRAINT `fk_return_penjualan_item_invoice_product1` FOREIGN KEY (`invoice_product`) REFERENCES `invoice_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_return_penjualan_item_return_penjualan1` FOREIGN KEY (`return_penjualan`) REFERENCES `return_penjualan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `retur_order`
--
ALTER TABLE `retur_order`
  ADD CONSTRAINT `fk_retur_order_order1` FOREIGN KEY (`order`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `retur_order_item`
--
ALTER TABLE `retur_order_item`
  ADD CONSTRAINT `fk_retur_order_item_order_product1` FOREIGN KEY (`order_product`) REFERENCES `order_product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_retur_order_item_retur_order1` FOREIGN KEY (`retur_order`) REFERENCES `retur_order` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sales_rute`
--
ALTER TABLE `sales_rute`
  ADD CONSTRAINT `fk_sales_rute_minggu1` FOREIGN KEY (`minggu`) REFERENCES `minggu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sales_rute_customer`
--
ALTER TABLE `sales_rute_customer`
  ADD CONSTRAINT `fk_sales_rute_customer_pembeli1` FOREIGN KEY (`pembeli`) REFERENCES `pembeli` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sales_rute_customer_sales_rute1` FOREIGN KEY (`sales_rute`) REFERENCES `sales_rute` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sales_rute_hari`
--
ALTER TABLE `sales_rute_hari`
  ADD CONSTRAINT `fk_sales_rute_hari_hari1` FOREIGN KEY (`hari`) REFERENCES `hari` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sales_rute_hari_sales_rute1` FOREIGN KEY (`sales_rute`) REFERENCES `sales_rute` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `sales_rute_user`
--
ALTER TABLE `sales_rute_user`
  ADD CONSTRAINT `fk_sales_rute_user_sales_rute1` FOREIGN KEY (`sales_rute`) REFERENCES `sales_rute` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sales_rute_user_user1` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `surat_jalan`
--
ALTER TABLE `surat_jalan`
  ADD CONSTRAINT `fk_surat_jalan_invoice1` FOREIGN KEY (`invoice`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_user_priveledge` FOREIGN KEY (`priveledge`) REFERENCES `priveledge` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `vendor`
--
ALTER TABLE `vendor`
  ADD CONSTRAINT `fk_vendor_vendor_category1` FOREIGN KEY (`vendor_category`) REFERENCES `vendor_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
